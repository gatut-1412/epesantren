<?php
    session_start();
    include_once("config/config.php");
    if(empty($_SESSION['student_nis'])){
    header('location:main.php');
    }
    if((time() - $_SESSION['auto_logout_time'])>900){
    header('location:logout1.php');
    }
?>

<?php
    $student_full_name=$_SESSION["student_full_name"];
    $class_name=$_SESSION["class_name"];
    $majors_name=$_SESSION["majors_name"];
    $student_id=$_SESSION["student_id"];
    $student_nis=$_SESSION["student_nis"];
    $foldersekolah=$_SESSION["folder"];
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=0">
    <title>Pembayaran</title>

	<!-- Google font file. If you want you can change. -->
	<link href="https://fonts.googleapis.com/css?family=Nunito:400,600,700,900" rel="stylesheet">

	<!-- Fontawesome font file css -->
	<link rel="stylesheet" type="text/css" href="css/font-awesome.min.css">

	<!-- Template global css file. Requared all pages -->
	<link rel="stylesheet" type="text/css" href="css/global.style.css">
</head>

<body>
	
	<div class="wrapper">
        <?php 
		    include("menu/nav-menu.php");
		?>
		<div class="wrapper-inline">
			<!-- Header area start -->
			<header> <!-- extra class no-background -->
				<a class="go-back-link" href="main.php"><i class="fa fa-arrow-left"></i></a>
				<h1 class="page-title">Izin Santri<span></h1>
				<div class="navi-menu-button">
					<em></em>
					<em></em>
					<em></em>
				</div>
			</header>
			<!-- Header area end -->
			<!-- Page content start -->
			<main class="fix-top-menu">

                <div class="container">
                    <form action="<?php echo $_SERVER["PHP_SELF"];?>" method="get">
                        <div class="form-row no-padding">
                            <label for="sel1">Tahun Ajaran:</label>
                            <select class="form-element" id = "period" name="period">
                            <?php                   
                                    $sql="select * from period";
                                    $hasil=mysqli_query($mysqli,$sql);
                                    $no=0;
                                    while ($data = mysqli_fetch_array($hasil)) {

                                    $ket="";
                                        if (isset($_GET['period'])) {
                                        $period = trim($_GET['period']);

                                        if ($period==$data['period_id'])
                                        {
                                        $ket="selected";
                                        }
                                    }
                            ?>
                            <option <?php echo $ket; ?> value="<?php echo $data['period_id'];?>"><?php echo $data['period_start'];?>/<?php echo $data['period_end'];?></option>
                            <?php
                                }
                            ?>
                                
                            </select>
                                <div class="form-divider"></div>
                                <div class="col-sm-4">
                                    <button type="submit" class="button circle green"><i class="fa fa-search"> </i> Cari Pembayaran</button>
                                </div>
                        </div>
                    </form>
                </div>

                <?php if (isset($_GET['period'])) {
                        $tahun      = $_GET['period'];
                        $siswaID    = $_SESSION["student_id"];

                ?>
                <?php }  ?>

                <!-- TAB CONTROL START -->
                    <div class="tab-item">

                        <div class="tab-menu fix-width">
                            <a class="menu-item active" href="javascript:void(0);" data-content="contentPost"><i class="fa fa-bars"></i> Izin Keluar</a>
                            <a class="menu-item" href="javascript:void(0);" data-content="contentFavorites"><i class="fa fa-comments"></i> Izin Pulang</a>
                        </div>

                            <div class="tab-content">

                                <div class="content-item active" id="contentPost">

                                    <div class="form-divider"></div>
                                    <div class="form-label-divider"><span>Izin Keluar</span></div>
                                    <div class="form-divider"></div>

                                    <!--expendable list item -->

                                    <div class="table-responsive">          
                                        <table id="example" class="table table-striped table-bordered">
                                            <thead class="thead-dark">
                                                <tr>
                                                    <th>No</th>
                                                    <th>Tanggal</th>
                                                    <th>Jam</th>
                                                    <th>Keperluan</th>
                                                </tr>
                                            </thead>
                                                <?php 
                                                    $bebas = "SELECT `izin`.`izin_date`, `izin`.`izin_time`, `izin`.`izin_keperluan`, `student_full_name`, `student_nis`, `period_start`, `period_end`,`user_email`,`user_password`,`user_full_name`
                                                    FROM `izin` 
                                                    LEFT JOIN `period` ON `izin`.izin_period_id = `period`.period_id 
                                                    LEFT JOIN `student` ON `student`.student_id = `izin`.izin_student_id
                                                    LEFT JOIN `users` ON `users`.user_id = `izin`.izin_user_id
                                                    WHERE izin_student_id = '$siswaID' AND izin_period_id = '$tahun'
                                                    ORDER BY `izin_id` DESC";

                                                    $cicil=mysqli_query($mysqli,$bebas);
                                                    $j =1;
                                                    while($row = mysqli_fetch_array($cicil)) {
                                                ?>

                                                <tbody>
                                                    <tr>
                                                        <td><?php echo $j ?></td>
                                                        <td><?php echo date ("d-F-Y", strtotime ($row['izin_date']))?></td>
                                                        <td><?php echo $row['izin_time']?></td>
                                                        <td><?php echo $row['izin_keperluan'] ?></td>
                                                    </tr> 
                                                </tbody>
                                                <?php
                                                    $j++;
                                                    }
                                                ?> 
                                        </table>
                                    </div>

                                    <!--expendable list item -->

                                </div>   

                                <div class="content-item" id="contentFavorites">
                                                
                                    <div class="form-divider"></div>
                                    <div class="form-label-divider"><span>Izin Pulang</span></div>
                                    <div class="form-divider"></div>

                                    <div class="table-responsive">          
                                        <table id="example" class="table table-striped table-bordered">
                                            <thead class="thead-dark">
                                                <tr>
                                                    <th>No</th>
                                                    <th>Tanggal</th>
                                                    <th>Jumlah Hari</th>
                                                    <th>Keperluan</th>
                                                </tr>
                                            </thead>

                                            <?php 
                                                $pulang = "SELECT `pulang`.`pulang_date`, `pulang`.`pulang_days`, `pulang`.`pulang_note`, `student_full_name`, `student_nis`, `period_start`, `period_end`,`user_email`,`user_password`,`user_full_name` 
                                                FROM `pulang` 
                                                LEFT JOIN `period` ON `pulang`.pulang_period_id = `period`.period_id 
                                                LEFT JOIN `student` ON `student`.student_id = `pulang`.pulang_student_id 
                                                LEFT JOIN `users` ON `users`.user_id = `pulang`.pulang_user_id
                                                WHERE pulang_student_id = '$siswaID' AND pulang_period_id = '$tahun'
                                                ORDER BY `pulang_id` DESC";

                                                $muleh = mysqli_query($mysqli,$pulang);
                                                $i =1;
                                                while($row = mysqli_fetch_array($muleh)) {
                                            ?>
                                                <tbody>
                                                    <tr>
                                                        <td><?php echo $i ?></td>
                                                        <td><?php echo date ("d-F-Y", strtotime ($row['pulang_date']))?></td>
                                                        <td><?php echo $row['pulang_days']?></td>
                                                        <td><?php echo $row['pulang_note'] ?></td>
                                                    </tr> 
                                                </tbody>
                                            <?php
                                                $i++;
                                                }
                                            ?>
                                        </table>
                                    </div>

                                </div>

                            </div>

                    </div>
                <!-- TAB CONTROL END -->

                <?php 
                    include("footer/footer.php");
                ?>

			</main>
			<!-- Page content end -->
		</div>
    </div>


    <!--Page loader DOM Elements. Requared all pages-->
    <div class="sweet-loader">
        <div class="box">
            <div class="circle1"></div>
            <div class="circle2"></div>
            <div class="circle3"></div>
        </div>
    </div>  

	<!-- JQuery library file. requared all pages -->
	<script src="js/jquery-3.2.1.min.js"></script>
    <script src="jquery-ui/jquery-ui.js" type="text/javascript"></script>
    <script src="jquery-ui/jquery-ui.min.js" type="text/javascript"></script>

	<!-- Template global script file. requared all pages -->
    <script src="js/global.script.js"></script>

    <!-- Turbo slider plugin file. requared only wizard pages -->
    <script src="plugins/turbo-slider/turbo.min.js"></script>
    <script src="js/turbo-ini.js"></script>

    <!-- Load file library jQuery -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

    <!-- Load file library Popper JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>

    <!-- Load file library JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>

    <script>
    $(function() {
        $(document).ready(function() {
        $('#example').DataTable();
        });
    });
    </script>

    <!-- <script type="text/javascript">
        $(function(){
            $("#nis").autocomplete({
                source:'autocompletefilter.php',
                minLength:3,
                select:function(event,data){
                    $('input[name=nama]').val(data.item.student_full_name);
                    $('input[name=sekolahaddress]').val(data.item.alamat_sekolah);
                    $('input[name=alamat]').val(data.item.alamat);
                }
            });
        });
    </script> -->
    
    <script type="text/javascript">
    $(document).ready(function() {
        $('[data-toggle="toggle"]').change(function(){
            $(this).parents().next('.hide').toggle();
        });
    });
    </script>

	
</body>

</html>

