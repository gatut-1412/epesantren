<?php
session_start();
if(!isset($_SESSION["student_id"])){
	header("Location:login.php");
}
include_once("config/config.php");
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=0">

	<!-- Google font file. If you want you can change. -->
	<link href="https://fonts.googleapis.com/css?family=Nunito:400,600,700,900" rel="stylesheet">

	<!-- Fontawesome font file css -->
	<link rel="stylesheet" type="text/css" href="css/font-awesome.min.css">

	<!-- Template global css file. Requared all pages -->
	<link rel="stylesheet" type="text/css" href="css/global.style.css">
	<title><?php foreach ($siswa as $row):?> <?php echo ($f['r'] == $row['student_nis']) ? $row['student_full_name'] : '' ?><?php endforeach; ?></title>
</head>

<style type="text/css">
@page {
	margin-top: 0.5cm;
	/*margin-bottom: 0.1em;*/
	margin-left: 1cm;
	margin-right: 1cm;
	margin-bottom: 0.1cm;
}
.name-school{
	font-size: 15pt;
	font-weight: bold;
	padding-bottom: -15px;
}

.unit{
    font-weight: bold;
	font-size: 8pt;
	margin-bottom: -10px;
}

.alamat{
	font-size: 8pt;
	margin-bottom: -10px;
}
.detail{
	font-size: 10pt;
	font-weight: bold;
	padding-top: -15px;
	padding-bottom: -12px;
}
body {
	font-family: sans-serif;
}
table {
	font-family: verdana,arial,sans-serif;
	font-size:11px;
	color:#333333;
	border-width: none;
	/*border-color: #666666;*/
	border-collapse: collapse;
	width: 100%;
}

th {
	padding-bottom: 8px;
	padding-top: 8px;
	border-color: #666666;
	background-color: #dedede;
	/*border-bottom: solid;*/
	text-align: left;
}

td {
	text-align: left;
	border-color: #666666;
	background-color: #ffffff;
}

hr {
	border: none;
	height: 1px;
	/* Set the hr color */
	color: #333; /* old IE */
	background-color: #333; /* Modern Browsers */
}
.container {
	position: relative;
}

.topright {
	position: absolute;
	top: 0;
	right: 0;
	font-size: 18px;
	border-width: thin;
	padding: 5px;
}
.topright2 {
	position: absolute;
	top: 30px;
	right: 50px;
	font-size: 18px;
	border: 1px solid;
	padding: 5px;
	color: red;
}
</style>

<body>
	
	<div class="wrapper">
        
		<div class="wrapper-inline">
			<!-- Header area start -->
			<header> <!-- extra class no-background -->
				<!-- <a class="go-back-link" href="javascript:history.back();"><i class="fa fa-arrow-left"></i></a> -->
				<h1 class="page-title">Pembayaran <span></h1>
				<div class="navi-menu-button">
					<em></em>
					<em></em>
					<em></em>
				</div>
			</header>
			<!-- Header area end -->
			<!-- Page content start -->
			<main class="fix-top-menu">

					<div class="container">
							<div class="topright">
									Bukti Pembayaran<br>

									<?php
											$tahun = $_GET['period'];
											$noref = $_GET['noref'];
											$siswaID = $_SESSION["student_id"];
											$siswaNIS = $_SESSION["student_nis"];
											$qsiswa    = "SELECT `student`.`student_full_name`, `student_nis`, `student_born_place`, `student_address`, `student_born_date`, `student_name_of_mother`, `student_name_of_father`, `student_parent_phone`, 
											`majors_name`, `majors_id`, `majors_short_name`, `madin_id`, `madin_name`, `class_name`, `class_id`
											FROM `student`
											LEFT JOIN `class` ON `class`.class_id = `student`.class_class_id
											LEFT JOIN `majors` ON `student`.majors_majors_id = `majors`.majors_id
											LEFT JOIN `madin` ON `madin`.`madin_id` = `student`.`student_madin`
											WHERE student_id = '$siswaID'
											ORDER BY `student_id` ASC";

											$siswa = mysqli_query($mysqli, $qsiswa);
											while($row = mysqli_fetch_array($siswa)) {
												$like     = 'SP' . $row['majors_short_name'] . $row['student_nis'];
											}

											$kwitansi = "SELECT kas_id, kas_noref, kas_input_date, kas_debit 
											FROM kas WHERE kas_noref LIKE '$like%' AND kas_period = '$tahun' 
											ORDER BY kas_id DESC";

									?>
									
									<?php
										$majors=mysqli_query($mysqli,$qsiswa);
										while($row = mysqli_fetch_array($majors)){
									?>
										<font size=9px>
											Unit Sekolah :
											<?php echo $row['majors_short_name'].'<br>' ?>
										</font>
									<?php
									}
									?>

									<?php echo $row['kas_input_date']?>
									
									<img style="width:100.5pt;height:15pt;z-index:6;" src="<?php echo 'media/barcode_transaction/'.$noref.'.png' ?>" alt="Image_4_0" /><br>
									<font size="8px"><?php echo $noref; ?></font>
									
							</div>
					</div>
					

					<?php  
						$student = mysqli_query($mysqli, $qsiswa);
						while($row = mysqli_fetch_array($student)) {
					?>
					<hr>

					<table style="padding-top: -5px; padding-bottom: 5px">
						<input type="hidden" value="<?php echo $tahun ?>" name="period">
							<tbody>
								<tr>
									<td style="width: 100px;">NIS</td>
									<td style="width: 5px;">:</td>
									<td style="width: 150px;"><?php echo $row['student_nis']?></td>
									<td style="width: 130px;">Tanggal Pembayaran</td>
									<td style="width: 5px;">:</td>
									<td style="width: 131px;"><?php echo ['kas_input_date']?></td>
								</tr>
								<tr>
									<td style="width: 100px;">Nama</td>
									<td style="width: 5px;">:</td>
									<td style="width: 150px;"><?php echo $row['student_full_name']?></td>
									<td style="width: 130px;">Tahun Ajaran</td>
									<td style="width: 5px;">:</td>
									<td style="width: 131px;"><?php echo $row['period_start'] / $row['period_end']?></td>
								</tr>
								<tr>
									<td style="width: 100px;">Kelas</td>
									<td style="width: 5px;">:</td>
									<td style="width: 150px;"><?php echo $row['class_name']?></td>
									<td style="width: 100px;">Akun Kas</td>
									<td style="width: 5px;">:</td>
									<td style="width: 150px;"><?php echo $bcode['account_description']?></td>
								</tr>
								<tr>
									<td style="width: 100px;">Kamar</td>
									<td style="width: 5px;">:</td>
									<td style="width: 150px;"><?php echo $row['madin_name']?></td>
									<td style="width: 100px;"></td>
									<td style="width: 5px;"></td>
									<td style="width: 150px;"></td>
								</tr>
							</tbody>
					</table>
					<?php
					}
					?>
					<hr>
					<p class="detail">Dengan rincian pembayaran sebagai berikut:</p>
					
					<table style="border-style: solid;">
						<tr>
							<th style="border-top: 1px solid; border-bottom: 1px solid;">No.</th>
							<th style="border-top: 1px solid; border-bottom: 1px solid;">Pembayaran</th>
							<th style="border-top: 1px solid; border-bottom: 1px solid;">Total Tagihan</th>
							<th colspan="2" style="border-top: 1px solid; border-bottom: 1px solid; text-align: center">Jumlah Pembayaran</th>
						</tr>
						<?php {

							$bulan = "SELECT DISTINCT `bulan`.`bulan_id`, `bulan_bill`, `bulan_account_id`, `bulan_noref`, `bulan_date_pay`, 
							`bulan_number_pay`, `bulan_status`, `bulan_input_date`, `bulan_last_update`, `student_student_id`, `student_img`, 
							`student_nis`, `student_full_name`, `student_name_of_mother`, `student_parent_phone`, `student`.`class_class_id`, 
							`student`.`majors_majors_id`, `majors_name`, `majors_short_name`, `class_name`, `payment_payment_id`, `period_period_id`, 
							`period_status`, `period_start`, `period_end`, `pos_name`, `account_account_id`, `payment_type`, `user_user_id`, 
							`user_full_name`, `account`.`account_id`, `account`.`account_code`, `acc`.`account_description`, `madin_id`, `madin_name`, 
							`month_month_id`, `month_name`, `bulan`.`student_student_id`, `bulan`.`student_student_id` 
							FROM `bulan` 
							LEFT JOIN `month` ON `month`.`month_id` = `bulan`.`month_month_id` 
							LEFT JOIN `student` ON `student`.`student_id` = `bulan`.`student_student_id` 
							LEFT JOIN `payment` ON `payment`.`payment_id` = `bulan`.`payment_payment_id` 
							LEFT JOIN `pos` ON `pos`.`pos_id` = `payment`.`pos_pos_id` 
							LEFT JOIN `period` ON `period`.`period_id` = `payment`.`period_period_id` 
							LEFT JOIN `class` ON `class`.`class_id` = `student`.`class_class_id` 
							LEFT JOIN `madin` ON `madin`.`madin_id` = `student`.`student_madin` 
							LEFT JOIN `majors` ON `majors`.`majors_id` = `student`.`majors_majors_id` 
							LEFT JOIN `users` ON `users`.`user_id` = `bulan`.`user_user_id` 
							LEFT JOIN `account` ON `account`.`account_id` = `pos`.`account_account_id` 
							LEFT JOIN `account` AS `acc` ON `acc`.`account_id` = `bulan`.`bulan_account_id` 
							WHERE `bulan`.`student_student_id` = '$siswaID' AND `majors_status` = '1' 
							GROUP BY `bulan`.`payment_payment_id` 
							ORDER BY `payment_payment_id`, `account_id` ASC, `month_month_id` ASC";
							// echo $bulan;
							
				
						?>

						<?php
						$i =1;
						$sasi=mysqli_query($mysqli,$bulan);
						while($row = mysqli_fetch_array($sasi)){
							$namePay = $row['pos_name'].' - T.A '.$row['period_start'].'/'.$row['period_end'];
							$mont = ($row['month_month_id']<=6) ? $row['period_start'] : $row['period_end'];
						?>
						<tr>
							<td style="border-bottom: 1px solid;padding-top: 10px; padding-bottom: 10px;"><?php echo $i ?></td>
							<td style="border-bottom: 1px solid;"><?php echo $namePay.' - ('.$row['month_name'].' '. $mont.')' ?></td>
							<td style="border-bottom: 1px solid"><?php echo 'Rp. ' . number_format($row['bulan_bill'], 0, ',', '.') ?></td>
							<td style="border-bottom: 1px solid;">Rp. </td>
							<td style="border-bottom: 1px solid; text-align: right;"><?php echo number_format($row['bulan_bill'], 0, ',', '.') ?></td>
						</tr>
						<?php 
						$i++;
						}
						?>

						<?php {
							$free = "SELECT `bebas_pay`.`bebas_pay_id`, `bebas_pay_bill`, `bebas_pay_bill`, `bebas_pay_number`, `bebas_pay_desc`, 
							`bebas_pay_input_date`, `bebas_pay_last_update`, `bebas_pay`.`bebas_bebas_id`, `bebas_bill`, `student_student_id`, 
							`student_nis`, `student`.`class_class_id`, `class_name`, `student_full_name`, `student_name_of_mother`, 
							`student_parent_phone`, `payment_payment_id`, `payment_type`, `period_start`, `period_end`, 
							`payment`.`pos_pos_id`, `pos_name`, `user_user_id`, `user_full_name`, `account_id`, `account_code` 
							FROM `bebas_pay` 
							LEFT JOIN `bebas` ON `bebas`.`bebas_id` = `bebas_pay`.`bebas_bebas_id` 
							LEFT JOIN `student` ON `student`.`student_id` = `bebas`.`student_student_id` 
							LEFT JOIN `payment` ON `payment`.`payment_id` = `bebas`.`payment_payment_id` 
							LEFT JOIN `period` ON `period`.`period_id` = `payment`.`period_period_id` 
							LEFT JOIN `pos` ON `pos`.`pos_id` = `payment`.`pos_pos_id` 
							LEFT JOIN `class` ON `class`.`class_id` = `student`.`class_class_id` 
							LEFT JOIN `users` ON `users`.`user_id` = `bebas_pay`.`user_user_id` 
							LEFT JOIN `account` ON `account`.`account_id` = `pos`.`account_account_id` 
							LEFT JOIN `majors` ON `majors`.`majors_id` = `account`.`account_majors_id` 
							WHERE `bebas_pay`.`bebas_pay_noref` = '$noref' AND `bebas`.`student_student_id` = '$siswaID' AND `student`.`student_nis` = '$siswaNIS' AND `majors_status` = '1' 
							ORDER BY `account_id` ASC, `bebas_pay_last_update` ASC";
							// echo $free;
						?>

						<?php

							$j = $i;
							$bbs=mysqli_query($mysqli,$free);
							while($row = mysqli_fetch_array($bbs)) {
							$namePayFree = $row['pos_name'].' - T.A '.$row['period_start'].'/'.$row['period_end'];
						?>

						<tr>
							<td style="border-bottom: 1px solid;padding-top: 10px; padding-bottom: 10px;"><?php echo $j ?></td>
							<td style="border-bottom: 1px solid;"><?php echo $namePayFree .' - ('.$row['bebas_pay_desc'].')' ?></td>
							<td style="border-bottom: 1px solid;"><?php echo 'Rp. ' . number_format($row['bebas_bill'], 0, ',', '.') ?></td>
							<td style="border-bottom: 1px solid;">Rp. </td>
							<td style="border-bottom: 1px solid; text-align: right;"><?php echo number_format($row['bebas_pay_bill'], 0, ',', '.') ?></td>
						</tr>
						<?php 
							$j++;
							}
						?>
						<?php {

							$sumbeb = "SELECT `bebas_pay`.`bebas_pay_id`, `bebas_pay_bill`, `bebas_pay_number`, `bebas_pay_desc`, 
							`bebas_pay_input_date`, `bebas_pay_last_update`, `bebas_pay`.`bebas_bebas_id`, `bebas_bill`, `student_student_id`,
							`student_nis`, `student`.`class_class_id`, `class_name`, `student_full_name`, `student_name_of_mother`, `student_parent_phone`, 
							`payment_payment_id`, `payment_type`, `period_start`, `period_end`, `payment`.`pos_pos_id`, `pos_name`, `user_user_id`, 
							`user_full_name`, `account_id`, `account_code` 
							FROM `bebas_pay` 
							LEFT JOIN `bebas` ON `bebas`.`bebas_id` = `bebas_pay`.`bebas_bebas_id` 
							LEFT JOIN `student` ON `student`.`student_id` = `bebas`.`student_student_id` 
							LEFT JOIN `payment` ON `payment`.`payment_id` = `bebas`.`payment_payment_id` 
							LEFT JOIN `period` ON `period`.`period_id` = `payment`.`period_period_id` 
							LEFT JOIN `pos` ON `pos`.`pos_id` = `payment`.`pos_pos_id` 
							LEFT JOIN `class` ON `class`.`class_id` = `student`.`class_class_id` 
							LEFT JOIN `users` ON `users`.`user_id` = `bebas_pay`.`user_user_id` 
							LEFT JOIN `account` ON `account`.`account_id` = `pos`.`account_account_id` 
							LEFT JOIN `majors` ON `majors`.`majors_id` = `account`.`account_majors_id` 
							WHERE `bebas_pay`.`bebas_pay_noref` = '$noref' AND `bebas`.`student_student_id` = '$siswaID' AND `student`.`student_nis` = '$siswaNIS' AND `majors_status` = '1' 
							ORDER BY `account_id` ASC, `bebas_pay_last_update` ASC";
							// echo $sumbeb;

							$summonth = "SELECT DISTINCT `bulan`.`bulan_id`, sum(`bulan_bill`) as `total`, `bulan_bill`, `bulan_date_pay`, `bulan_status`, 
							`bulan_input_date`, `bulan_last_update`, `student_student_id`, `student_img`, `student_nis`, `student_full_name`, 
							`student_name_of_mother`, `student`.`class_class_id`, `student`.`majors_majors_id`, `majors_name`, `majors_short_name`, 
							`class_name`, `payment_payment_id`, `payment_type`, `pos_name`, `payment`.`pos_pos_id`, `month_month_id`, `month_name`, 
							`bulan`.`student_student_id`, `bulan`.`student_student_id` 
							FROM `bulan` 
							LEFT JOIN `month` ON `month`.`month_id` = `bulan`.`month_month_id` 
							LEFT JOIN `student` ON `student`.`student_id` = `bulan`.`student_student_id` 
							LEFT JOIN `payment` ON `payment`.`payment_id` = `bulan`.`payment_payment_id` 
							LEFT JOIN `period` ON `period`.`period_id` = `payment`.`period_period_id` 
							LEFT JOIN `pos` ON `pos`.`pos_id` = `payment`.`pos_pos_id` 
							LEFT JOIN `class` ON `class`.`class_id` = `student`.`class_class_id` 
							LEFT JOIN `majors` ON `majors`.`majors_id` = `student`.`majors_majors_id` 
							WHERE `bulan`.`student_student_id` = '$siswaID' AND `bulan`.`bulan_noref` = '$noref' AND `bulan`.`bulan_status` = 1 AND `majors_status` = '1' 
							GROUP BY `payment_payment_id` 
							ORDER BY `month_month_id` ASC";
							// echo $summonth;
							

							$hasilbulan=mysqli_query($mysqli, $summonth);
								$i =1;
								$sumBulan  = 0;
								// $sumBebas = 0;

								while($data = mysqli_fetch_array($hasilbulan)) {
									$sumBulan += $data['total'];
									// $sumKredit += $data['kredit'];
							}

							$hasilbebas=mysqli_query($mysqli, $sumbeb);
								$i =1;
								$sumBebas  = 0;
							while($data = mysqli_fetch_array($hasilbebas)) {
								$sumBebas += $data['bebas_pay_bill'];
								// $sumKredit += $data['kredit'];
						}
						
						?>

						<tr>
							<td colspan="2" style="text-align: center;padding-top: 5px; padding-bottom: 5px;"></td>
							<td style="background-color: #dedede; font-weight:bold; border-bottom: 1px solid;">Total Pembayaran</td>
							<td style="background-color: #dedede;font-weight:bold;border-bottom: 1px solid;">Rp. </td>
							<td style="background-color: #dedede; font-weight:bold;border-bottom: 1px solid; text-align: right;"><?php echo number_format($sumBulan+$sumBebas, 0, ',', '.') ?></td>
						</tr>
						<tr>
							<td colspan="2" style="text-align: center;padding-top: 5px; padding-bottom: 5px;"><?php echo $setting_city['setting_value'] ?>, <?php echo pretty_date(date('Y-m-d'),'d F Y',false) ?></td>
							<td style="background-color: #dedede; font-weight:bold; border-bottom: 1px solid;">Terbilang</td>
							<td colspan="2" style="background-color: #dedede; font-weight:bold;border-bottom: 1px solid; text-align: right;"><?php echo $huruf ?></td>
						</tr>

						<?php
						}
						?>
						<?php
						}
						?>
						<?php
						}
						?>
						
						
							
						<?php 
							$user = "SELECT users.user_full_name AS kasir 
							FROM users 
							JOIN kas ON kas.kas_user_id = users.user_id 
							WHERE kas.kas_noref = '" . $bcode['kas_noref'] . "'";
						?>
						<?php
						$bebas=mysqli_query($mysqli,$user);
						while($row = mysqli_fetch_array($bebas)){
						?>
						
						<tr>
							<td colspan="2" style="text-align: center;"></td>
						</tr>

						<tr>
							<td colspan="2" style="text-align: center">Kasir</td>
						</tr>
						<tr>
							<td>&nbsp;</td>
						</tr>
						<tr>
							<td>&nbsp;</td>
						</tr>
						<tr>
							<td>&nbsp;</td>
						</tr>
						<tr>
							<td colspan="2" style="text-align: center"><b><?php echo ucfirst($user['kasir']); ?></b></td>
						</tr>
						<?php
						}
						?>
					</table>
					<br>

				<?php 
					include("footer/footer.php");
				?>

			</main>
			<!-- Page content end -->
		</div>
    </div>


		 <!--Page loader DOM Elements. Requared all pages-->
    <div class="sweet-loader">
		<div class="box">
		  	<div class="circle1"></div>
		  	<div class="circle2"></div>
		  	<div class="circle3"></div>
		</div>
	</div>

	<!-- JQuery library file. requared all pages -->
	<script src="js/jquery-3.2.1.min.js"></script>
    <script src="jquery-ui/jquery-ui.js" type="text/javascript"></script>
    <script src="jquery-ui/jquery-ui.min.js" type="text/javascript"></script>

	<!-- Template global script file. requared all pages -->
    <script src="js/global.script.js"></script>

    <!-- Turbo slider plugin file. requared only wizard pages -->
    <script src="plugins/turbo-slider/turbo.min.js"></script>
    <script src="js/turbo-ini.js"></script>

    <!-- Load file library jQuery -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

    <!-- Load file library Popper JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>

    <!-- Load file library JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>

    <script type="text/javascript">
    $(document).ready(function() {
        $('[data-toggle="toggle"]').change(function(){
            $(this).parents().next('.hide').toggle();
        });
    });
    </script>

	
</body>

</html>

