<?php

include_once("config/config.php");

// Fetch all data from database
$result = mysqli_query($mysqli, "SELECT * FROM `student` ORDER BY student_id ASC");
?>
<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>Epesantren Mobile</title>

	<!-- Fonts -->
	<link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

	<!-- Fontawesome font file css -->
	<link rel="stylesheet" type="text/css" href="css/font-awesome.min.css">
	<!-- <link rel="stylesheet" type="text/css" href="{{asset('/jquery-ui/jquery-ui.min.css')}}"> -->
	<link rel="stylesheet" type="text/css" href="jquery-ui/jquery-ui.min.css">

	<!--turbo slider plugin css file -->
	<link rel="stylesheet" type="text/css" href="plugins/turbo-slider/turbo.css">

	<!-- Template global css file. Requared all pages -->
	<link rel="stylesheet" type="text/css" href="css/global.style.css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
</head>

<body>
	<form action="login_action.php" method="post">
		<div class="login-wrap">
			<div class="login-html">
			<div class="text-center mt-10">
				<img src="img/ePesantren-1.png" class="logo-img" alt="" style=width:70% >
			</div>
				<input id="tab-1" type="radio" name="tab" class="sign-in" checked><label for="tab-1" class="tab"></label>
				<input id="tab-2" type="radio" name="tab" class="sign-up"><label for="tab-2" class="tab"></label>
				<div class="login-form">
					<div class="sign-in-htm">
					
						<?php 
						if (isset($_GET['password']) AND $_GET['password'] == 'salah') {?>
							<div class="alert alert-danger" role="alert">
								Password yang anda masukkan salah. Klik <a href="login.php" class="alert-link">Kembali</a> untuk memasukkan password lagi.
							</div>
						<?php } ?>
						
				</div>
			</div>
		</div>
	</form>


	<!--Page loader DOM Elements. Requared all pages-->
	<div class="sweet-loader">
		<div class="box">
		  	<div class="circle1"></div>
		  	<div class="circle2"></div>
		  	<div class="circle3"></div>
		</div>
	</div>

	<!-- JQuery library file. requared all pages -->
	<script src="js/jquery-3.2.1.min.js"></script>
    <script src="jquery-ui/jquery-ui.js" type="text/javascript"></script>
    <script src="jquery-ui/jquery-ui.min.js" type="text/javascript"></script>

    <!-- Turbo slider plugin file. requared only wizard pages -->
    <script src="plugins/turbo-slider/turbo.min.js"></script>
    <script src="js/turbo-ini.js"></script>

	<!-- Template global script file. requared all pages -->
	<script src="js/global.script.js"></script>

	
</body>

</html>