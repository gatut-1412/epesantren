<?php
    include_once("config/config.php");
    $footer = "SELECT setting_value FROM setting WHERE setting_id > 16 ORDER BY setting_id ASC";

?>



<footer>
    <div class="container">
        <ul>
            <?php
                $logo = mysqli_query($mysqli, "SELECT `setting_value` FROM `setting` WHERE `setting_id` = 91");
                $row = mysqli_fetch_array($logo);
                echo '<li><a href="https://'.$row['setting_value'].'"><img src="img/icons/www32.png" alt=""></i></a></li>';
            ?>
            <?php
                $logo = mysqli_query($mysqli, "SELECT `setting_value` FROM `setting` WHERE `setting_id` = 92");
                $row = mysqli_fetch_array($logo);
                echo '<li><a href="https://'.$row['setting_value'].'"><img src="img/icons/whatsapp32.png" alt=""></i></a></li>';
            ?>
            <?php
                $logo = mysqli_query($mysqli, "SELECT `setting_value` FROM `setting` WHERE `setting_id` = 93");
                $row = mysqli_fetch_array($logo);
                echo '<li><a href="https://'.$row['setting_value'].'"><img src="img/icons/facebook32.png" alt=""></i></a></li>';
            ?>
            <?php
                $logo = mysqli_query($mysqli, "SELECT `setting_value` FROM `setting` WHERE `setting_id` = 20");
                $row = mysqli_fetch_array($logo);
                echo '<li><a href="https://'.$row['setting_value'].'"><img src="img/icons/instagram32.png" alt=""></i></a></li>';
            ?>
            <?php
                $logo = mysqli_query($mysqli, "SELECT `setting_value` FROM `setting` WHERE `setting_id` = 21");
                $row = mysqli_fetch_array($logo);
                echo '<li><a href="https://'.$row['setting_value'].'"><img src="img/icons/youtube32.png" alt=""></i></a></li>';
            ?>
        </ul>
        <p>Copyright © All Right Reserved</p>
    </div>
</footer>

