<?php

include_once("config/config.php");

// Fetch all data from database
$result = mysqli_query($mysqli, "SELECT * FROM `student` ORDER BY student_id ASC");
?>
<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=0">
	<title>Change Password</title>

	<!-- Google font file. If you want you can change. -->
	<link href="https://fonts.googleapis.com/css?family=Nunito:400,600,700,900" rel="stylesheet">

	<!-- Fontawesome font file css -->
	<link rel="stylesheet" type="text/css" href="css/font-awesome.min.css">

	<!-- Template global css file. Requared all pages -->
	<link rel="stylesheet" type="text/css" href="css/global.style.css">
</head>

<body>
	
		<div class="login-wrap">
			<div class="login-html">
			<div class="text-center  mt-10">
				<img src="img/ePesantren-1.png" class="logo-img" alt="" style=width:70% >
			</div>
				<input id="tab-1" type="radio" name="tab" class="sign-in" checked><label for="tab-1" class="tab">Ubah Password</label>
				<input id="tab-2" type="radio" name="tab" class="sign-up"><label for="tab-2" class="tab"></label>
				<div class="login-form">
					<div class="sign-in-htm">
						<div class="group">
							<label for="user" class="label">NIS</label>
							<input id="user" type="text" class="input">
						</div>
						<div class="group">
							<label for="user" class="label">Nomor Handphone</label>
							<input id="user" type="text" class="input">
						</div>
						<div class="group">
							<label for="pass" class="label">Password Lama</label>
							<input id="pass" type="password" class="input" data-type="password">
						</div>
						<div class="group">
							<label for="pass" class="label">Password Baru</label>
							<input id="pass" type="password" class="input" data-type="password">
						</div>
						<div class="group">
							<!-- <input type="submit" class="button" value="Sign In"> -->
							<a href="#" type="submit" class="button">Ganti Password</a>
						</div>
						<div class="hr"></div>
						<!-- <div class="foot-lnk">
							<a href="{{ url('/login') }}">Kembali ke Login</a>
						</div> -->
					</div>
					<!-- <div class="sign-up-htm">
						<div class="group">
							<label for="user" class="label">Username</label>
							<input id="user" type="text" class="input">
						</div>
						<div class="group">
							<label for="pass" class="label">Password</label>
							<input id="pass" type="password" class="input" data-type="password">
						</div>
						<div class="group">
							<label for="pass" class="label">Repeat Password</label>
							<input id="pass" type="password" class="input" data-type="password">
						</div>
						<div class="group">
							<label for="pass" class="label">Email Address</label>
							<input id="pass" type="text" class="input">
						</div>
						<div class="group">
							<input type="submit" class="button" value="Sign Up">
						</div>
						<div class="hr"></div>
						<div class="foot-lnk">
							<label for="tab-1">Already Member?</a>
						</div>
					</div> -->
				</div>
			</div>
		</div>


	<!--Page loader DOM Elements. Requared all pages-->
	<div class="sweet-loader">
		<div class="box">
		  	<div class="circle1"></div>
		  	<div class="circle2"></div>
		  	<div class="circle3"></div>
		</div>
	</div>

	<!-- JQuery library file. requared all pages -->
	<script src="js/jquery-3.2.1.min.js"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>

	<!-- Template global script file. requared all pages -->
	<script src="js/global.script.js"></script>

	<script type="text/javascript">
            function isi_otomatis(){
                var nomorhp = $("#nomorhp").val();
				var nis = $("#nis").val();
                $.ajax({
                    url: 'proses-ajax.php',
					type: 'POST',
                    data:{'nis' : nis, 'nomorhp' : nomorhp} ,
                }).success(function (data) {
                    var json = data,
                    obj = JSON.parse(json);
                    $('#nama').val(obj.nama);
                });
            }
    </script>

	
	
</body>

</html>