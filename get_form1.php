<?php
include_once("config/config.php");

for($count = 0; $count < count($_POST["bulan_id"]); $count++)
        {
            $student = "SELECT  `s`.`pos_name`, `d`.`period_id`, `d`.`period_start`, `d`.`period_end`, `p`.`payment_id`, `p`.`payment_mode`, 
            `b`.`bulan_id`, `b`.`bulan_bill`
            FROM `bulan` as `b` 
            JOIN `payment` as `p` ON `b`.`payment_payment_id` = `p`.`payment_id` 
            JOIN `pos` as `s` ON `s`.`pos_id` = `p`.`pos_pos_id` 
            LEFT JOIN `account` as `a` ON `a`.`account_id` = `b`.`bulan_account_id` 
            JOIN `period` as `d` ON `d`.`period_id` = `p`.`period_period_id`
            JOIN `month` as `m` ON `m`.`month_id` = `b`.month_month_id
            WHERE b.bulan_id IN (".$_POST['bulan_id'][$count].")";

            
        
            echo   '<div class="table-responsive">
                        <input type="hidden" value="'.$row['bulan_id'].'" name="bulan_id" id="bulan_id">
                        <table class="table">
                            <thead class="thead-dark">
                                    <tr>
                                        <th>Paybulan</th>
                                        <th>Bayar</th>
                                        <th>Aksi</th>
                                    </tr>
                            </thead>
                            '.$hasil=mysqli_query($mysqli,$student);
                            while($row = mysqli_fetch_array($hasil)){
                            $sumTotal += $row['bulan_bill'];
                            $Paybulan = $row['pos_name'].' - T.A '.$row['period_start'].'/'.$row['period_end'].'
                            <tbody>
                                    <tr>
                                        <td>'.$Paybulan.'</td>
                                        <td>Rp. '.number_format($row['bulan_bill'], 0, ',', '.').'</td>
                                        <td><button class="button blue" onclick="myFunction()"><i class="fa fa-user"></i> Icon</button></td>
                                    </tr> 
                            </tbody>

                        </table>
                    </div>'
            }
        
        }

            echo    '<div class="form-row-group with-icons">
                            <div class="form-row no-padding">
                                    <input type="text" name="pembayaran" id="pembayaran" class="form-element" value=" Total : Rp. '.number_format($sumTotal, 0, ',', '.').'">
                            </div>
                    </div>';

for($jumlah = 0; $jumlah < count($_POST["bebas_id"]); $jumlah++)
        {
            $bebas = "SELECT `bebas`.`bebas_id`, `bebas_bill`, `bebas_diskon`, `bebas_total_pay`, `bebas_input_date`, 
            `bebas_last_update`, `student_student_id`, `payment_payment_id`, `pos_name`, `payment_type`, `period_period_id`, `period_start`, `period_end`
            FROM `bebas` 
            LEFT JOIN `student` ON `student`.`student_id` = `bebas`.`student_student_id` 
            LEFT JOIN `payment` ON `payment`.`payment_id` = `bebas`.`payment_payment_id` 
            LEFT JOIN `pos` ON `pos`.`pos_id` = `payment`.`pos_pos_id`
            LEFT JOIN `period` ON `period`.`period_id` = `payment`.`period_period_id`
            WHERE bebas_id IN (".$_POST['bebas_id'][$jumlah].")";

            echo   '<div class="table-responsive">
                        <input type="hidden" value="'.$row['bebas_id'].'" name="bulan_id" id="bulan_id">
                        <table class="table">
                            <thead class="thead-dark">
                                    <tr>
                                        <th>Paybebas</th>
                                        <th>Bayar</th>
                                        <th>Catatan</th>
                                    </tr>
                            </thead>

                            '.$hasil=mysqli_query($mysqli,$bebas);
                            while($row = mysqli_fetch_array($hasil)){
                            $sisa = $row['bebas_bill']-$row['bebas_total_pay'];
                            $namePay = $row['pos_name'].' - T.A '.$row['period_start'].'/'.$row['period_end'].'

                            <tbody>
                                    <tr>
                                        <td>'.$namePay.'</td>
                                        <td><input type="text" name="bill" id="bill" class="form-element"></td>
                                        <td><input type="text" name="note" id="note" class="form-element"></td>
                                    </tr> 
                            </tbody>

                        </table>
                    </div>'
                    
            }
        }
            echo    '<div class="form-row-group with-icons">
                            <div class="form-row no-padding">
                                    <input type="text" name="pembayaran" id="pembayaran" class="form-element" value=" Sisa : Rp. '.number_format($sisa, 0, ',', '.').'" readonly>
                            </div>
                    </div>';

?>