<?php

include_once("config/config.php");
include("login_action.php");




$student_full_name=$_SESSION["student_full_name"];
$class_name=$_SESSION["class_name"];
$majors_name=$_SESSION["majors_name"];
$student_id=$_SESSION["student_id"];
$student_nis=$_SESSION["student_nis"];
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=0">
    <title>Pembayaran</title>

	<!-- Google font file. If you want you can change. -->
	<link href="https://fonts.googleapis.com/css?family=Nunito:400,600,700,900" rel="stylesheet">

	<!-- Fontawesome font file css -->
    <link rel="stylesheet" type="text/css" href="css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="{{asset('/jquery-ui/jquery-ui.min.css')}}">

    <!--turbo slider plugin css file -->
    <link rel="stylesheet" type="text/css" href="plugins/turbo-slider/turbo.css">
    
	<!-- Template global css file. Requared all pages -->
    <link rel="stylesheet" type="text/css" href="css/global.style.css">
</head>

<body>
	
	<div class="wrapper">
		<div class="nav-menu">
			<nav class="menu">
                <!-- Template logo start -->
                <div class="nav-header">
                    <a href="student-profile.php">
                        <img class="image-round" alt="" src="img/user.png">
                        <span><?php echo $student_full_name; ?></span>
                        <span><?php echo $class_name; ?></span>
                        <span><?php echo $majors_name; ?></span>
                    </a>
                </div>
                <!-- Template logo end -->
                <!-- Menu navigation start -->
                <div class="nav-container">
                    <ul class="main-menu">
                    	<li class="active">
                            <a href="main.php"><img src="img/icons/home24.png" alt=""> Dashboard</a>
                        </li>
                        <!-- <li>
                            <a href="student-profile.php"><img src="img/icons/boy24.png" alt=""> Student Profile</a>
                        </li> -->
                        <li>
                            <a href="tab-top-tahfidz.php"><img src="img/icons/study24.png" alt=""> Tahfidz </a>
                        </li>
                        <li>
                            <a href="tab-top.php"><img src="img/icons/cost24.png" alt=""> Pembayaran</a>
                        </li>
                        <li>
                            <a href="tab-top-absensi.php"><img src="img/icons/permission24.png" alt=""> Absensi </a>
                        </li>
                        <li>
                            <a href="tab-top-tabungan.php"><img src="img/icons/purse24.png" alt=""> Tabungan </a>
                        </li>
                        <li>
                            <a href="product-detail-1.php"><img src="img/icons/consultation24.png" alt=""> Konseling </a>
                        </li>
                        <li>
                            <a href="product-detail-2.php"><img src="img/icons/heart24.png" alt=""> Kesehatan </a>
                        </li>

                        <li>
                            <a href="post-list-2.php"><img src="img/icons/newspaper.png" alt=""> Info Terbaru</a>
                        </li>
                        <li>
                            <a href="change-password.php"><img src="img/icons/exchange.png" alt=""> Ubah Password </a>
                        </li>
                        <li>
                            <a href="logout.php"><img src="img/icons/logout.png" alt=""> Logout</a>
                        </li>
                    </ul>
                </div>
                <!-- Menu navigation end -->
            </nav>

		</div>
		<div class="wrapper-inline">
			<!-- Header area start -->
			<header> <!-- extra class no-background -->
				<a class="go-back-link" href="javascript:history.back();"><i class="fa fa-arrow-left"></i></a>
				<h1 class="page-title">Pembayaran</h1>
				<div class="navi-menu-button">
					<em></em>
					<em></em>
					<em></em>
				</div>
			</header>
			<!-- Header area end -->
			<!-- Page content start -->
            <main class="fix-top-menu">
                    <div class="container">
                        <br>
                        <!-- <h4>Menampilkan Data pada Tabel berdasarkan pilihan Combo Box di PHP</h4> -->
                        <form action="<?php echo $_SERVER["PHP_SELF"];?>" method="get">
                            <div class="form-row no-padding">
                                <label for="sel1">Tahun Ajaran:</label>
                                <select class="form-element" name="period">
                                <?php
                                        include "config/config.php";
                                                               
                                        $sql="select * from period";

                                        $hasil=mysqli_query($mysqli,$sql);
                                        $no=0;
                                        while ($data = mysqli_fetch_array($hasil)) {
                                        $no++;

                                        $ket="";
                                            if (isset($_GET['period'])) {
                                            $period = trim($_GET['period']);

                                            if ($period==$data['period_id'])
                                            {
                                            $ket="selected";
                                            }
                                        }
                                ?>
                                <option <?php echo $ket; ?> value="<?php echo $data['period_id'];?>"><?php echo $data['period_start'];?>/<?php echo $data['period_end'];?></option>
                                <?php
                                    }
                                ?>
                                    
                                </select>
                                    <div class="form-divider"></div>
                                    <div class="col-sm-4">
                                        <button type="submit" class="button circle green"><i class="fa fa-search"> </i> Cari Pembayaran</button>
                                    </div>
                            </div>
                        </form>
                        
                        <?php if (isset($_GET['period'])) {

                            $siswaID = $_SESSION["student_id"];

                            $bebas = "SELECT `bebas`.`bebas_id`, `bebas_bill`, `bebas_diskon`, `bebas_total_pay`, `bebas_input_date`, `bebas_last_update`, `student_student_id`, `student`.`class_class_id`, `class_name`, `student_full_name`, `student_nis`, `student_name_of_mother`, `student_parent_phone`, `student`.`majors_majors_id`, `majors_name`, `majors_short_name`, `payment_payment_id`, `pos_name`, `payment_type`, `period_period_id`, `period_start`, `period_end`, `madin_id`, `madin_name` 
                            FROM `bebas` 
                            LEFT JOIN `student` ON `student`.`student_id` = `bebas`.`student_student_id` 
                            LEFT JOIN `payment` ON `payment`.`payment_id` = `bebas`.`payment_payment_id` 
                            LEFT JOIN `pos` ON `pos`.`pos_id` = `payment`.`pos_pos_id` 
                            LEFT JOIN `class` ON `class`.`class_id` = `student`.`class_class_id` 
                            LEFT JOIN `period` ON `period`.`period_id` = `payment`.`period_period_id` 
                            LEFT JOIN `majors` ON `majors`.`majors_id` = `student`.`majors_majors_id` 
                            LEFT JOIN `madin` ON `madin`.`madin_id` = `student`.`student_madin` 
                            WHERE `bebas`.`student_student_id` AND `payment`.`period_period_id` AND `majors_status` AND student_student_id = '$siswaID'
                            ORDER BY `payment_payment_id` ASC";
                            
                            $bulanan = "SELECT `t`.`student_id`, `t`.`student_nis`, `t`.`student_full_name`, `s`.`pos_name`, `d`.`period_id`, `d`.`period_start`, `d`.`period_end`, `p`.`payment_id`, `p`.`payment_mode`, 
                            SUM(`b`.`bulan_bill`) as `total`,
                            SUM(if(`b`.`bulan_status`='1',`b`.`bulan_bill`,0)) as `dibayar`,
                            SUM(if(`b`.`month_month_id`='1',`b`.`bulan_bill`,0)) as `bill_jul`, 
                            SUM(if(`b`.`month_month_id`='2',`b`.`bulan_bill`,0)) as `bill_agu`, 
                            SUM(if(`b`.`month_month_id`='3',`b`.`bulan_bill`,0)) as `bill_sep`, 
                            SUM(if(`b`.`month_month_id`='4',`b`.`bulan_bill`,0)) as `bill_okt`, 
                            SUM(if(`b`.`month_month_id`='5',`b`.`bulan_bill`,0)) as `bill_nov`, 
                            SUM(if(`b`.`month_month_id`='6',`b`.`bulan_bill`,0)) as `bill_des`, 
                            SUM(if(`b`.`month_month_id`='7',`b`.`bulan_bill`,0)) as `bill_jan`, 
                            SUM(if(`b`.`month_month_id`='8',`b`.`bulan_bill`,0)) as `bill_feb`, 
                            SUM(if(`b`.`month_month_id`='9',`b`.`bulan_bill`,0)) as `bill_mar`, 
                            SUM(if(`b`.`month_month_id`='10',`b`.`bulan_bill`,0)) as `bill_apr`, 
                            SUM(if(`b`.`month_month_id`='11',`b`.`bulan_bill`,0)) as `bill_mei`, 
                            SUM(if(`b`.`month_month_id`='12',`b`.`bulan_bill`,0)) as `bill_jun`, 
                            SUM(if(`b`.`month_month_id`='1',`b`.`bulan_status`,0)) as `status_jul`, 
                            SUM(if(`b`.`month_month_id`='2',`b`.`bulan_status`,0)) as `status_agu`, 
                            SUM(if(`b`.`month_month_id`='3',`b`.`bulan_status`,0)) as `status_sep`, 
                            SUM(if(`b`.`month_month_id`='4',`b`.`bulan_status`,0)) as `status_okt`, 
                            SUM(if(`b`.`month_month_id`='5',`b`.`bulan_status`,0)) as `status_nov`, 
                            SUM(if(`b`.`month_month_id`='6',`b`.`bulan_status`,0)) as `status_des`, 
                            SUM(if(`b`.`month_month_id`='7',`b`.`bulan_status`,0)) as `status_jan`, 
                            SUM(if(`b`.`month_month_id`='8',`b`.`bulan_status`,0)) as `status_feb`, 
                            SUM(if(`b`.`month_month_id`='9',`b`.`bulan_status`,0)) as `status_mar`, 
                            SUM(if(`b`.`month_month_id`='10',`b`.`bulan_status`,0)) as `status_apr`, 
                            SUM(if(`b`.`month_month_id`='11',`b`.`bulan_status`,0)) as `status_mei`, 
                            SUM(if(`b`.`month_month_id`='12',`b`.`bulan_status`,0)) as `status_jun`,
                            MAX(if(`b`.`month_month_id`='1',`b`.`bulan_date_pay`,0)) as `date_pay_jul`, 
                            MAX(if(`b`.`month_month_id`='2',`b`.`bulan_date_pay`,0)) as `date_pay_agu`, 
                            MAX(if(`b`.`month_month_id`='3',`b`.`bulan_date_pay`,0)) as `date_pay_sep`, 
                            MAX(if(`b`.`month_month_id`='4',`b`.`bulan_date_pay`,0)) as `date_pay_okt`, 
                            MAX(if(`b`.`month_month_id`='5',`b`.`bulan_date_pay`,0)) as `date_pay_nov`, 
                            MAX(if(`b`.`month_month_id`='6',`b`.`bulan_date_pay`,0)) as `date_pay_des`, 
                            MAX(if(`b`.`month_month_id`='7',`b`.`bulan_date_pay`,0)) as `date_pay_jan`, 
                            MAX(if(`b`.`month_month_id`='8',`b`.`bulan_date_pay`,0)) as `date_pay_feb`, 
                            MAX(if(`b`.`month_month_id`='9',`b`.`bulan_date_pay`,0)) as `date_pay_mar`, 
                            MAX(if(`b`.`month_month_id`='10',`b`.`bulan_date_pay`,0)) as `date_pay_apr`, 
                            MAX(if(`b`.`month_month_id`='11',`b`.`bulan_date_pay`,0)) as `date_pay_mei`, 
                            MAX(if(`b`.`month_month_id`='12',`b`.`bulan_date_pay`,0)) as `date_pay_jun`, 
                            MAX(if(`b`.`month_month_id`='1',`m`.`month_name`,'')) as `month_name_jul`, 
                            MAX(if(`b`.`month_month_id`='2',`m`.`month_name`,'')) as `month_name_agu`, 
                            MAX(if(`b`.`month_month_id`='3',`m`.`month_name`,'')) as `month_name_sep`, 
                            MAX(if(`b`.`month_month_id`='4',`m`.`month_name`,'')) as `month_name_okt`, 
                            MAX(if(`b`.`month_month_id`='5',`m`.`month_name`,'')) as `month_name_nov`, 
                            MAX(if(`b`.`month_month_id`='6',`m`.`month_name`,'')) as `month_name_des`, 
                            MAX(if(`b`.`month_month_id`='7',`m`.`month_name`,'')) as `month_name_jan`, 
                            MAX(if(`b`.`month_month_id`='8',`m`.`month_name`,'')) as `month_name_feb`, 
                            MAX(if(`b`.`month_month_id`='9',`m`.`month_name`,'')) as `month_name_mar`, 
                            MAX(if(`b`.`month_month_id`='10',`m`.`month_name`,'')) as `month_name_apr`, 
                            MAX(if(`b`.`month_month_id`='11',`m`.`month_name`,'')) as `month_name_mei`, 
                            MAX(if(`b`.`month_month_id`='12',`m`.`month_name`,'')) as `month_name_jun`,
                            MAX(if(`b`.`month_month_id`='1',`a`.`account_description`,'')) as `account_jul`, 
                            MAX(if(`b`.`month_month_id`='2',`a`.`account_description`,'')) as `account_agu`, 
                            MAX(if(`b`.`month_month_id`='3',`a`.`account_description`,'')) as `account_sep`, 
                            MAX(if(`b`.`month_month_id`='4',`a`.`account_description`,'')) as `account_okt`, 
                            MAX(if(`b`.`month_month_id`='5',`a`.`account_description`,'')) as `account_nov`, 
                            MAX(if(`b`.`month_month_id`='6',`a`.`account_description`,'')) as `account_des`, 
                            MAX(if(`b`.`month_month_id`='7',`a`.`account_description`,'')) as `account_jan`, 
                            MAX(if(`b`.`month_month_id`='8',`a`.`account_description`,'')) as `account_feb`, 
                            MAX(if(`b`.`month_month_id`='9',`a`.`account_description`,'')) as `account_mar`, 
                            MAX(if(`b`.`month_month_id`='10',`a`.`account_description`,'')) as `account_apr`, 
                            MAX(if(`b`.`month_month_id`='11',`a`.`account_description`,'')) as `account_mei`, 
                            MAX(if(`b`.`month_month_id`='12',`a`.`account_description`,'')) as `account_jun`, 
                            SUM(if(`b`.`month_month_id`='1',`b`.`bulan_id`,0)) as `month_id_jul`,
                            SUM(if(`b`.`month_month_id`='2',`b`.`bulan_id`,0)) as `month_id_agu`,
                            SUM(if(`b`.`month_month_id`='3',`b`.`bulan_id`,0)) as `month_id_sep`,
                            SUM(if(`b`.`month_month_id`='4',`b`.`bulan_id`,0)) as `month_id_okt`,
                            SUM(if(`b`.`month_month_id`='5',`b`.`bulan_id`,0)) as `month_id_nov`,
                            SUM(if(`b`.`month_month_id`='6',`b`.`bulan_id`,0)) as `month_id_des`,
                            SUM(if(`b`.`month_month_id`='7',`b`.`bulan_id`,0)) as `month_id_jan`,
                            SUM(if(`b`.`month_month_id`='8',`b`.`bulan_id`,0)) as `month_id_feb`,
                            SUM(if(`b`.`month_month_id`='9',`b`.`bulan_id`,0)) as `month_id_mar`,
                            SUM(if(`b`.`month_month_id`='10',`b`.`bulan_id`,0)) as `month_id_apr`,
                            SUM(if(`b`.`month_month_id`='11',`b`.`bulan_id`,0)) as `month_id_mei`,
                            SUM(if(`b`.`month_month_id`='12',`b`.`bulan_id`,0)) as `month_id_jun`
                            FROM `bulan` as `b` 
                            JOIN `payment` as `p` ON `b`.`payment_payment_id` = `p`.`payment_id` 
                            JOIN `pos` as `s` ON `s`.`pos_id` = `p`.`pos_pos_id` 
                            LEFT JOIN `account` as `a` ON `a`.`account_id` = `b`.`bulan_account_id` 
                            JOIN `period` as `d` ON `d`.`period_id` = `p`.`period_period_id`
                            JOIN `student` as `t` ON `t`.`student_id` = `b`.student_student_id
                            JOIN `month` as `m` ON `m`.`month_id` = `b`.month_month_id
                            WHERE `t`.`student_nis` AND `d`.`period_id` AND student_student_id = '$siswaID'
                            GROUP BY `p`.`payment_id`";?>
                            
                        <div class="form-divider"></div>
                        <div class="form-label-divider"><span>Pembayaran Bulanan</span></div>
                        <div class="form-divider"></div>
                        <table class="table table-striped table-hover" style="cursor: pointer;">
                            <thead>
                            <tr>
                                <th>No.</th>
                                <th>Nama Pembayaran</th>
                                <th>Total</th>
                                <th>Sudah Dibayar</th>
                                <th>Kekurangan</th>
                                <th>Status</th>
                            </tr>
                            </thead>

                            <tbody>
                            <?php
                            $i =1;  
                            include "config/config.php";
                            $hasil=mysqli_query($mysqli,$bulanan);
                            while($row = mysqli_fetch_array($hasil))
                            {
                                    $namePay = $row['pos_name'].' - T.A '.$row['period_start'].'/'.$row['period_end'];
                                    $jml  = $row['total'];
                                    $dbyr = $row['dibayar'];
                                    ?>
                                    <tr data-toggle="collapse" data-target="#demo<?php echo $row->payment_id?>" style="color:<?php echo ($row->total== $row->dibayar) ? '#00E640' : 'red' ?>">
                                        <td><?php echo $i++ ?></td>
                                        <td><?php echo $namePay ?></td>
                                        <td><?php echo 'Rp. ' . number_format($jml, 0, ',', '.') ?></td>
                                        <td><?php echo 'Rp. ' . number_format($dbyr, 0, ',', '.') ?></td>
                                        <td><?php echo 'Rp. ' . number_format($jml - $dbyr, 0, ',', '.') ?></td>
                                        <td><label class="label <?php echo ($jml== $dbyr) ? 'label-success' : 'label-warning' ?>"><?php echo ($jml== $dbyr) ? 'Lengkap' : 'Belum Lengkap' ?></label></td>
                                    </tr>
                                    <?php
                                    }
                                ?>
                            </tbody>
                            <?php 
                            // $c = count($pembayaran);
                            // if($c >0){ 
                                // foreach($bulanan as $detail)
                                $hasil=mysqli_query($mysqli,$bulanan);
                                while($row = mysqli_fetch_array($hasil)){?>
                                <tbody id="demo<?php echo $detail->payment_id ?>" class="collapse">
                                <div class="form-divider"></div>
                                    <tr>
                                        <td colspan="5" align="center" class="info">
                                            <h4><?php echo $row['pos_name'].' - T.A '.$row['period_start'].'/'.$row['period_end']; ?></h4>
                                        </td>

                                        <td></td>
                                    </tr>

                                    <tr>
                                        <th>No.</th>
                                        <th>Bulan</th>
                                        <th>Tahun</th>
                                        <th>Nominal</th>
                                        <th colspan="2" style="text-align: center;">Status</th>
                                    </tr>
                                    <tr class="<?php echo ($detail->status_jul ==1) ? 'success' : 'danger' ?>">
                                        <td>
                                            1
                                        </td>
                                        <td><?php echo $row['month_name_jul']?></td>
                                        <td><?php echo $row['period_start'] ?></td>
                                        <td><?php echo number_format($row['bill_jul'], 0, ',', '.') ?></td>
                                        <td colspan="2" align="center"><?php echo ($row['status_jul'] == 1) ? 'Lunas' : 'Belum Lunas' ?></td>
                                    </tr>
                                    <tr class="<?php echo ($detail->status_agu ==1) ? 'success' : 'danger' ?>">
                                        <td>
                                            2
                                        </td>
                                        <td><?php echo $row['month_name_agu'] ?></td>
                                        <td><?php echo $row['period_start'] ?></td>
                                        <td><?php echo number_format($row['bill_agu'], 0, ',', '.') ?></td>
                                        <td colspan="2" align="center"><?php echo ($row['status_agu'] == 1) ? 'Lunas' : 'Belum Lunas' ?></td>
                                    </tr>
                                    <tr class="<?php echo ($detail->status_sep ==1) ? 'success' : 'danger' ?>">
                                        <td>
                                            3
                                        </td>
                                        <td><?php echo $row['month_name_sep'] ?></td>
                                        <td><?php echo $row['period_start'] ?></td>
                                        <td><?php echo number_format($row['bill_sep'], 0, ',', '.') ?></td>
                                        <td colspan="2" align="center"><?php echo ($row['status_sep'] == 1) ? 'Lunas' : 'Belum Lunas' ?></td>
                                    </tr>
                                    <tr class="<?php echo ($detail->status_okt ==1) ? 'success' : 'danger' ?>">
                                        <td>
                                            4
                                        </td>
                                        <td><?php echo $row['month_name_okt'] ?></td>
                                        <td><?php echo $row['period_start'] ?></td>
                                        <td><?php echo number_format($row['bill_okt'], 0, ',', '.') ?></td>
                                        <td colspan="2" align="center"><?php echo ($row['status_okt'] == 1) ? 'Lunas' : 'Belum Lunas' ?></td>
                                    </tr>
                                    <tr class="<?php echo ($detail->status_nov ==1) ? 'success' : 'danger' ?>">
                                        <td>
                                            5
                                        </td>
                                        <td><?php echo $row['month_name_nov'] ?></td>
                                        <td><?php echo $row['period_start'] ?></td>
                                        <td><?php echo number_format($row['bill_nov'], 0, ',', '.') ?></td>
                                        <td colspan="2" align="center"><?php echo ($row['status_nov'] == 1) ? 'Lunas' : 'Belum Lunas' ?></td>
                                    </tr>
                                    <tr class="<?php echo ($detail->status_des ==1) ? 'success' : 'danger' ?>">
                                        <td>
                                            6
                                        </td>
                                        <td><?php echo $row['month_name_des']  ?></td>
                                        <td><?php echo $row['period_start'] ?></td>
                                        <td><?php echo number_format($row['bill_des'], 0, ',', '.') ?></td>
                                        <td colspan="2" align="center"><?php echo ($row['status_des'] == 1) ? 'Lunas' : 'Belum Lunas' ?></td>
                                    </tr>
                                    <tr class="<?php echo ($detail->status_jan ==1) ? 'success' : 'danger' ?>">
                                        <td>
                                            7
                                        </td>
                                        <td><?php echo $row['month_name_jan'] ?></td>
                                        <td><?php echo $row['period_end'] ?></td>
                                        <td><?php echo number_format($row['bill_jan'], 0, ',', '.') ?></td>
                                        <td colspan="2" align="center"><?php echo ($row['status_jan'] == 1) ? 'Lunas' : 'Belum Lunas' ?></td>
                                    </tr>
                                    <tr class="<?php echo ($detail->status_feb ==1) ? 'success' : 'danger' ?>">
                                        <td>
                                            8
                                        </td>
                                        <td><?php echo $row['month_name_feb'] ?></td>
                                        <td><?php echo $row['period_end'] ?></td>
                                        <td><?php echo number_format($row['bill_feb'], 0, ',', '.') ?></td>
                                        <td colspan="2" align="center"><?php echo ($row['status_feb'] == 1) ? 'Lunas' : 'Belum Lunas' ?></td>
                                    </tr>
                                    <tr class="<?php echo ($detail->status_mar ==1) ? 'success' : 'danger' ?>">
                                        <td>
                                            9
                                        </td>
                                        <td><?php echo $row['month_name_mar'] ?></td>
                                        <td><?php echo $row['period_end'] ?></td>
                                        <td><?php echo number_format($row['bill_mar'], 0, ',', '.') ?></td>
                                        <td colspan="2" align="center"><?php echo ($row['status_mar'] == 1) ? 'Lunas' : 'Belum Lunas' ?></td>
                                    </tr>
                                    <tr class="<?php echo ($detail->status_apr ==1) ? 'success' : 'danger' ?>">
                                        <td>
                                            10
                                        </td>
                                        <td><?php echo $row['month_name_apr'] ?></td>
                                        <td><?php echo $row['period_end'] ?></td>
                                        <td><?php echo number_format($row['bill_apr'], 0, ',', '.') ?></td>
                                        <td colspan="2" align="center"><?php echo ($row['status_apr'] == 1) ? 'Lunas' : 'Belum Lunas' ?></td>
                                    </tr>
                                    <tr class="<?php echo ($detail->status_mei ==1) ? 'success' : 'danger' ?>">
                                        <td>
                                            11
                                        </td>
                                        <td><?php echo $row['month_name_mei'] ?></td>
                                        <td><?php echo $row['period_end'] ?></td>
                                        <td><?php echo number_format($row['bill_mei'], 0, ',', '.') ?></td>
                                        <td colspan="2" align="center"><?php echo ($row['status_mei'] == 1) ? 'Lunas' : 'Belum Lunas' ?></td>
                                    </tr>
                                    <tr class="<?php echo ($detail->status_jun ==1) ? 'success' : 'danger' ?>">
                                        <td>
                                            12
                                        </td>
                                        <td><?php echo $row['month_name_jun'] ?></td>
                                        <td><?php echo $row['period_end'] ?></td>
                                        <td><?php echo number_format($row['bill_jun'], 0, ',', '.') ?></td>
                                        <td colspan="2" align="center"><?php echo ($row['status_jun'] == 1) ? 'Lunas' : 'Belum Lunas' ?></td>
                                    </tr>
                                </tbody>
                                <?php  }?>

                        </table>

                        <div class="form-divider"></div>
                        <div class="form-divider"></div>
                        <div class="form-label-divider"><span>Pembayaran Lainnya</span></div>
                        <div class="form-divider"></div>
                        <table class="table table-striped table-hover">
                            <thead>
                                <tr>
                                <th>No.</th>
                                <th>Jenis Pembayaran</th>
                                <th>Total</th>
                                <th>Dibayar</th>
                                <th>Kekurangan</th>
                                <th>Status</th>
                                </tr>
                            </thead>

                            <tbody>
                                <?php
                                        include "config/config.php";
                                        $hasil=mysqli_query($mysqli,$bebas);
                                        $i =1;
                                        // if (is_array($bulanan) || is_object($bulanan))
                                        // $num=count($pembayaran);
                                        // if ($num > 0){
                                            // while($row = $hasil->fetch_assoc())
                                            while($row = mysqli_fetch_array($hasil)) {
                                                // if (isset($_GET['period'])) {
                                                    $sisa = $row['bebas_bill']-$row['bebas_total_pay'];
                                                    $namePay = $row['pos_name'].' - T.A '.$row['period_start'].'/'.$row['period_end'];
                                                    ?>

                                                    <tr style="color:<?php echo ($row['bebas_bill'] == $row['bebas_total_pay']) ? '#00E640' : 'red' ?>">
                                                    <td><?php echo $i ?></td>
                                                    <td><?php echo $namePay ?></td>
                                                    <td><?php echo 'Rp. ' . number_format($row['bebas_bill'], 0, ',', '.') ?></td>
                                                    <td><?php echo 'Rp. ' . number_format($row['bebas_total_pay'], 0, ',', '.') ?></td>
                                                    <td><?php echo 'Rp. ' . number_format($sisa, 0, ',', '.') ?></td>
                                                    <td><label class="label <?php echo ($row['bebas_bill']==$row['bebas_total_pay']) ? 'label-success' : 'label-warning' ?>"><?php echo ($row['bebas_bill']==$row['bebas_total_pay']) ? 'Lunas' : 'Belum Lunas' ?></label></td>
                                                    </tr>
                                                    <?php 
                                                // }
                                                $i++;
                                            }
                                            ?>  
                            </tbody>
                        </table> 
                        <?php } ?>
                    </div>
                    <div class="form-divider"></div>
                        <div class="form-row">
                            <a href="#" class="button circle block red">Download Tagihan</a>
                        </div>
                        <div class="form-row">
                            <button class="button circle yellow block" data-popup="kwitansi">Kwitansi Pembayaran</button>
                        </div>
                        <div class="form-row">
                            <button class="button circle green block" data-popup="history">History Pembayaran</button>
                        </div>

                        <footer>
                            <div class="container">
                                <ul>
                                    <li><a href="#"><img src="img/icons/chrome32.png" alt=""></i></a></li>
                                    <li><a href="#"><img src="img/icons/whatsapp32.png" alt=""></i></a></li>
                                    <li><a href="#"><img src="img/icons/facebook32.png" alt=""></i></a></li>
                                    <li><a href="#"><img src="img/icons/instagram32.png" alt=""></i></a></li>
                                    <li><a href="#"><img src="img/icons/youtube32.png" alt=""></i></a></li>
                                </ul>
                                <p>Copyright © All Right Reserved</p>
                            </div>
                            <!-- <div class="container">
                                <a class="button circle green block" href="cobaviewpembayaran.php">Lihat View</a>
                            </div> -->
                        </footer>

			</main>
			<!-- Page content end -->
		</div>
	</div>
    <!--POPUP PENCARIAN HTML CONTENT START -->
	<div class="popup-overlay" id="formPopup">
		<div class="popup-container">
			<div class="popup-header">
				<h3 class="popup-title">Form Popup</h3>
				<span class="popup-close" data-dismiss="true"><i class="fa fa-times"></i></span>
			</div>

				<div class="form-row-group with-icons">
					<div class="form-row no-padding">
                            <div class="expandable-item accordion" data-group="accordion2">                           
                                <h4 class="list-title">Pilih Tahun Ajaran</h4>
                                <div class="form-divider"></div>
                                <div class="form-row-group with-icons">
                                    <div class="form-row no-padding">
                                        <i class="fa fa-language"></i>
                                        <select class="form-element">
                                            <option disabled selected> Pilih Tahun Pelajaran </option>
                                            
                                            <option value=""></option>
                                           
                                        </select>
                                    </div>
					            </div>

                            </div>
					</div>
                </div>
			<div class="popup-footer">
				<button class="button orange">Save</button>
			</div>
		</div>
	</div>
    <!--POPUP PENCARIAN HTML CONTENT END -->

    <!--POPUP KWITANSI HTML CONTENT START -->
    <div class="popup-overlay" id="kwitansi">
        <div class="popup-container">
            <div class="popup-header">
                <h3 class="popup-title">Info Kwitansi Pembayaran</h3>
                <span class="popup-close" data-dismiss="true"><i class="fa fa-times"></i></span>
            </div>
            <div class="popup-content">
            
                <div class="form-divider"></div>
                <div class="form-label-divider"><span>Kwitansi Pembayaran</span></div>
                <div class="form-divider"></div>
                <table class="table table-hover">
                <thead>
                <tr>
                  <th>No. Ref</th>
                  <th>Tanggal</th>
                  <th>Total Transaksi</th>
                  <th>Aksi</th>
                </tr>
                </thead>
                <tbody>
                    <?php
                    if (!empty($kwitansi)) {
                    $i = 1;
                    foreach ($kwitansi as $row):
                    ?>
                      <tr>
                      <td>
                      <?php echo $row['kas_noref'] ?>       </td>
                      <td>
                          <?php echo pretty_date($row['kas_input_date'], 'd-m-Y', false) ?>
                      </td>
                      <td>
                      <?php echo 'Rp ' . number_format($row['kas_debit'], 0, ',', '.') ?>
                      </td>
                        <td align="center">
                            <a href="<?php echo base_url() . 'student/payout/cetakBukti?n=' . $f['n'] . '&r=' . $f['r'] . '&d=' . $row['kas_input_date'] . '&f=' . $row['kas_noref'] ?>" class="btn btn-danger btn-xs" target="_blank">Cetak</a>
                        </td>
                      </tr>
                      <?php
                    $i++;
                      endforeach;
                  } else {
                  echo '<tr>
                            <td colspan="4" align="center">Belum Ada Transaksi</td>
                        </tr>';
                      }
                    ?>
                      </tbody>
                </table>
                <div class="popup-footer">
                    <!-- <button class="button orange">Cetak</button> -->
                    <button class="button" data-dismiss="true">Cancel</button>
                </div>
            </div>
        </div>
    </div>
    <!--POPUP KWITANSI HTML CONTENT END -->

    <!--POPUP HTML CONTENT START -->
	<div class="popup-overlay" id="history">
        <div class="popup-container">
			<div class="popup-header">
				<h3 class="popup-title">History Pembayaran</h3>
				<span class="popup-close" data-dismiss="true"><i class="fa fa-times"></i></span>
			</div>
			<div class="popup-content">

                <div class="form-divider"></div>
                <div class="form-label-divider"><span>History Pembayaran</span></div>
                <div class="form-divider"></div>

                        <table class="table table-responsive table-bordered" style="white-space: nowrap;">
                            <tr class="info">
                                <th>Tanggal</th>
                                <th>No. Ref</th>
                                <th>Pembayaran</th>
                                <th>Nominal</th>
                                <th>Bayar Via</th>
                            </tr>
                            <?php
                            $hasil=mysqli_query($mysqli,$bebas);
                            while($row = mysqli_fetch_array($hasil)){
                            // foreach ($log as $key) :
                            ?>
                            <tr>
                                <td><?php echo pretty_date($row['log_trx_input_date'],'d/m/Y',false)  ?></td>
                                <td>
                                    <?php echo ($row['bulan_bulan_id']!= NULL) ? $row['bulan_noref'] :  $row['bebas_pay_noref'] ?>
                                </td>
                                <td><?php echo ($row['bulan_bulan_id']!= NULL) ? $row['posmonth_name'].' - T.A '.$row['period_start_month'].'/'.$row['period_end_month'].' ('.$row['month_name'].')' : $row['posbebas_name'].' - T.A '.$row['period_start_bebas'].'/'.$row['period_end_bebas'] ?></td>
                                <td><?php echo ($row['bulan_bulan_id']!= NULL) ? 'Rp. '. number_format($row['bulan_bill'], 0, ',', '.') : 'Rp. '. number_format($row['bebas_pay_bill'], 0, ',', '.') ?></td>
                                <td><?php echo ($row['bulan_bulan_id']!= NULL) ? $row['accMonth'] : $row['accBebas'] ?></td>
                            </tr>
                            <?php //endforeach
                            }
                            ?>

                        </table> 
			</div>

                <div class="popup-footer">
                    <button class="button" data-dismiss="true">Cancel</button>
                </div>
		    </div>
            
	    </div>
    </div>
	<!--POPUP HTML CONTENT END -->

    <!--POPUP SPP MA HTML CONTENT START -->
	<div class="popup-overlay" id="largePopupCaripembayaran">
		<div class="popup-container">
			<div class="popup-header">
				<h3 class="popup-title">Pembayaran Siswa</h3>
				<span class="popup-close" data-dismiss="true"><i class="fa fa-times"></i></span>
			</div>
			<div class="form-row-group with-icons">
					<div class="form-row no-padding">
                            <div class="expandable-item accordion" data-group="accordion2">
                                <div class="expandable-header">
                                    <h3 class="list-title">Cari Pembayaran</h3>
                                </div>
                                
		</div>
	</div>
	<!--POPUP HTML CONTENT END -->

    <!--POPUP HTML CONTENT START -->
	<!-- <div class="popup-overlay" id="formPopuplain">
		<div class="popup-container">
			<div class="popup-header">
				<h3 class="popup-title">Form Popup</h3>
				<span class="popup-close" data-dismiss="true"><i class="fa fa-times"></i></span>
			</div>
			<div class="popup-content">
				
				<div class="form-row-group with-icons">
					<div class="form-row no-padding">
						<i class="fa fa-envelope"></i>
						<input type="text" name="aaa" class="form-element" placeholder="Input element">
					</div>
				</div>

				<div class="form-mini-divider"></div>

				<div class="form-row-group with-icons">
					<div class="form-row no-padding">
						<i class="fa fa-address-book-o"></i>
						<textarea class="form-element" placeholder="Textarea" rows="6"></textarea>
					</div>
				</div>

				<div class="form-mini-divider"></div>

				<div class="form-row-group with-icons">
					<div class="form-row no-padding">
						<i class="fa fa-language"></i>
						<select class="form-element">
							<option value="" selected>Select</option>
							<option value="1">English</option>
							<option value="1">Spanish</option>
							<option value="1">Turkish</option>
						</select>
					</div>
				</div>

			</div>
			<div class="popup-footer">
				<button class="button orange">Save</button>
			</div>
		</div>
	</div> -->
	<!--POPUP HTML CONTENT END -->



	 <!--Page loader DOM Elements. Requared all pages-->
     <div class="sweet-loader">
		<div class="box">
		  	<div class="circle1"></div>
		  	<div class="circle2"></div>
		  	<div class="circle3"></div>
		</div>
	</div>

	<!-- JQuery library file. requared all pages -->
	<script src="js/jquery-3.2.1.min.js"></script>
    <script src="jquery-ui/jquery-ui.js" type="text/javascript"></script>
    <script src="jquery-ui/jquery-ui.min.js" type="text/javascript"></script>

	<!-- Template global script file. requared all pages -->
    <script src="js/global.script.js"></script>

    <!-- Turbo slider plugin file. requared only wizard pages -->
    <script src="plugins/turbo-slider/turbo.min.js"></script>
    <script src="js/turbo-ini.js"></script>

    <!-- Load file library jQuery -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

    <!-- Load file library Popper JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>

    <!-- Load file library JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script> 

    <script>
    $(function() {
        $(document).ready(function() {
        $('#example').DataTable();
        });
    });
    </script>

<script type="text/javascript">
        $(function(){
            $("#nis").autocomplete({
                source:'autocompletefilter.php',
                minLength:3,
                select:function(event,data){
                    $('input[name=nama]').val(data.item.student_full_name);
                    // $('input[name=sekolahaddress]').val(data.item.alamat_sekolah);
                    // $('input[name=alamat]').val(data.item.alamat);
                }
            });
        });
	</script>

    

    <script type="text/javascript">
        // CSRF Token
        // var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
        // $(document).ready(function(){

        // $( "#cari" ).autocomplete({
        //     source: function( request, response ) {
        //     Fetch data
        //     $.ajax({
        //         url:"{{ url('/caribayar') }}",
        //         type: 'post',
        //         dataType: "json",
        //         data: {
        //         _token: CSRF_TOKEN,
        //         search: request.term
        //         },
        //         success: function( data ) {
        //         response( data );
        //         }
        //     });
        //     },
        //     select: function (event, ui) {
        //     Set selection
        //     $('#cari').val(ui.item.label); 
        //     $('#nomor').val(ui.item.id);
        //     $('#kode').val(ui.item.value);
        //     return false;
        //     }
        // });

        // });
    </script>

	
</body>

</html>