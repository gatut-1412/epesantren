<?php
    session_start();
    include_once("config/config.php");
    if(empty($_SESSION['student_nis'])){
    header('location:main.php');
    }
    if((time() - $_SESSION['auto_logout_time'])>900){
    header('location:logout1.php');
    }
?>

<?php

  $student_id=$_SESSION["student_id"];
  $student_nis=$_SESSION["student_nis"];
  $student_full_name=$_SESSION["student_full_name"];
  $class_name=$_SESSION["class_name"];
  $majors_name=$_SESSION["majors_name"];
  // memanggil file koneksi.php untuk membuat koneksi
  // include_once("config/config.php");

  // mengecek apakah di url ada nilai GET id
  if (isset($_GET['id'])) {
    // ambil nilai id dari url dan disimpan dalam variabel $id
    $id = ($_GET["id"]);

    // menampilkan data dari database yang mempunyai id=$id
    $query = "SELECT * FROM prove WHERE prove_id='$id'";
    $result = mysqli_query($mysqli, $query);
    // jika data gagal diambil maka akan tampil error berikut
    if(!$result){
      die ("Query Error: ".mysqli_errno($mysqli).
         " - ".mysqli_error($mysqli));
    }
    // mengambil data dari database
    $data = mysqli_fetch_assoc($result);
      // apabila data tidak ada pada database maka akan dijalankan perintah ini
       if (!count($data)) {
          echo "<script>alert('Data tidak ditemukan pada database');window.location='edit_upload.php';</script>";
       }
  } else {
    // apabila tidak ada data GET id pada akan di redirect ke index.php
    echo "<script>alert('Masukkan data id.');window.location='edit_upload.php';</script>";
  }         
?>

<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=0">
	<title>Edit Bukti Pembayaran</title>

	<!-- Google font file. If you want you can change. -->
	<link href="https://fonts.googleapis.com/css?family=Nunito:400,600,700,900" rel="stylesheet">

	<!-- Fontawesome font file css -->
	<link rel="stylesheet" type="text/css" href="css/font-awesome.min.css">

	<!-- Template global css file. Requared all pages -->
    <link rel="stylesheet" type="text/css" href="css/global.style.css">
    
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="/resources/demos/style.css">

</head>

<body>
	
	<div class="wrapper">
        <?php 
		include("menu/nav-menu.php");
		?>
		<div class="wrapper-inline">
			<!-- Header area start -->
			<header> <!-- extra class no-background -->
				<a class="go-back-link" href="javascript:history.back();"><i class="fa fa-arrow-left"></i></a>
				<h1 class="page-title">Editgaga Bukti Pembayaran</h1>
				<div class="navi-menu-button">
					<em></em>
					<em></em>
					<em></em>
				</div>
			</header>
			<!-- Header area end -->
			<!-- Page content start -->
			<main>
              <div class="container">

                <div class="form-divider"></div>
                
                <?php 
                  if (isset($_GET['pesan']) AND $_GET['pesan'] == 'gagal') {?>
                  <div class="alert alert-danger">
                    <strong>Maaf!</strong> Data anda gagal dijalankan. Silahkan cek kembali data yang anda masukkan.
                  </div>
                <?php } ?>

                <div class="form-label-divider"><span>Edit Gambar<?php echo $data['gambar']; ?></span></div>
                <div class="form-divider"></div>
                    <form method="POST" action="proses_edit.php" enctype="multipart/form-data" >
                    <input name="id" value="<?php echo $data['id']; ?>" hidden />
                    <input name="student_id" value="<?php echo $student_id; ?>" hidden />

                    <label>Masukkan Alasan Kenapa Transaksi Dibatalkan.[WAJIB DIISI]</label>
                    <!-- <div class="form-row-group with-icons">
                        <div class="form-row no-padding">
                            <i class="fa fa-file-text-o"></i>
                            <input type="text" name="keterangan" class="form-element" value="<?php echo $data['keterangan']; ?>">
                        </div>
                    </div> -->
                    <div class="form-row-group with-icons">
                      <div class="form-row no-padding">
                        <i class="fa fa-address-book-o"></i>
                        <textarea class="form-element" name="keterangan" placeholder="Textarea"></textarea>
                      </div>
                    </div>
                    <input name="status" value="0" hidden />

                      <div class="form-row">
                          <button type="submit" class="button circle block green">Simpan</button>
                      </div>       
                    </form>

              </div>
            
            <?php 
                include("footer/footer.php");
            ?>
			</main>
			<!-- Page content end -->
		</div>
	</div>


	<!--Page loader DOM Elements. Requared all pages-->
	<div class="sweet-loader">
		<div class="box">
		  	<div class="circle1"></div>
		  	<div class="circle2"></div>
		  	<div class="circle3"></div>
		</div>
	</div>

	<!-- JQuery library file. requared all pages -->
	<script src="js/jquery-3.2.1.min.js"></script>

	<!-- Template global script file. requared all pages -->
    <script src="js/global.script.js"></script>
    
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

    <script>
        $( function() {
            $( "#date" ).datepicker({
            dateFormat: "yy-mm-dd"
            });
        } );
    </script>

	
</body>

</html>