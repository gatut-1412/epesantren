<?php
session_start();
include_once("config/config.php");
// include("login_action.php");

$student_full_name=$_SESSION["student_full_name"];
$class_name=$_SESSION["class_name"];
$majors_name=$_SESSION["majors_name"];
?>
<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=0">
	<title>Informasi</title>

	<!-- Google font file. If you want you can change. -->
	<link href="https://fonts.googleapis.com/css?family=Nunito:400,600,700,900" rel="stylesheet">

	<!-- Fontawesome font file css -->
	<link rel="stylesheet" type="text/css" href="css/font-awesome.min.css">

	<!-- Template global css file. Requared all pages -->
	<link rel="stylesheet" type="text/css" href="css/global.style.css">
</head>

<body>
	
	<div class="wrapper">
		<?php 
		include("menu/nav-menu.php");
		?>
		<div class="wrapper-inline">
			<!-- Header area start -->
			<header class="no-background"> <!-- extra class no-background -->
				<a class="go-back-link" href="javascript:history.back();"><i class="fa fa-arrow-left"></i></a>
				<h2 class="page-title">POST DETAIL</h2>
				<div class="navi-menu-button">
					<em></em>
					<em></em>
					<em></em>
				</div>
			</header>
			<!-- Header area end -->
			<!-- Page content start -->
			<main class="no-margin">
			<div class="container">

                    <div class="form-divider"></div>
                    <!-- <div class="form-label-divider"><span>Edit Data</span></div>
                    <div class="form-divider"></div> -->

                    <!-- <form action="update.php" method="post"> -->
					<?php  { 

						$siswaID = $_SESSION["student_id"];

						$info = "SELECT * FROM `information` WHERE information_id = $_GET[i] ORDER BY `information_id` ASC";

					?>
					<?php
						$berita = mysqli_query($mysqli, $info);
							while($row = mysqli_fetch_array($berita)) {
					?>
                    
						<!-- POST DETAIL ITEM START -->
						<div class="form-divider"></div>
						<div class="post-item mt-30">
							<div class="post-asset image">
								<?php 
									if (isset($row['information_img'])) {?>
									<img class="profil-img" alt="User Avatar" src="imageinfo/<?php echo $row['information_img']; ?>" width="100" height="100">
								<?php } else { ?>
									<img class="profil-img" alt="User Avatar" src="imageinfo/image18.png" width="100" height="100">
								<?php } ?>
							</div>
							<div class="post-header">
								<h1 class="post-title"><?php echo $row['information_desc']; ?></h1>
							</div>
							<div class="post-footer">
								<div class="post-extra">
									<div class="add-favorite"><i class="fa fa-heart-o"></i></div>
									<div class="post-share"><i class="fa fa-share-alt"></i>
										<div class="social-links">
											<a href="#" class="share-facebook"><i class="fa fa-facebook"></i></a>
											<a href="#" class="share-whatsapp"><i class="fa fa-whatsapp"></i></a>
											<a href="#" class="share-instagram"><i class="fa fa-instagram"></i></a>
										</div>
									</div>
								</div>
							</div>
						</div>
						<!-- POST DETAIL ITEM END -->

						<!-- POST DETAIL CONTENT START -->
						<div class="post-detail">
						
							<p><?php echo $row['information_desc']; ?></p>
							
						</div>
						<!-- POST DETAIL CONTENT END -->

						<?php
						}
						?>

					<?php
					}
					?>

                </div>
                
                <?php 
					include("footer/footer.php");
				?>
			</main>
			<!-- Page content end -->
		</div>
	</div>


	<!--Page loader DOM Elements. Requared all pages-->
	<div class="sweet-loader">
		<div class="box">
		  	<div class="circle1"></div>
		  	<div class="circle2"></div>
		  	<div class="circle3"></div>
		</div>
	</div>

	<!-- JQuery library file. requared all pages -->
	<script src="js/jquery-3.2.1.min.js"></script>

	<!-- Template global script file. requared all pages -->
	<script src="js/global.script.js"></script>

	
</body>

</html>