<?php
session_start();
include_once("config/config.php");
// include_once("login_action.php");

// Fetch all data from database
$result = mysqli_query($mysqli, "SELECT * FROM `information` WHERE information_id ORDER BY information_input_date DESC");

$student_id=$_SESSION["student_id"];
$student_nis=$_SESSION["student_nis"];
$student_full_name=$_SESSION["student_full_name"];
$class_name=$_SESSION["class_name"];
$majors_name=$_SESSION["majors_name"];
?>
<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=0">
	<title>Informasi</title>

	<!-- Google font file. If you want you can change. -->
	<link href="https://fonts.googleapis.com/css?family=Nunito:400,600,700,900" rel="stylesheet">

	<!-- Fontawesome font file css -->
	<link rel="stylesheet" type="text/css" href="css/font-awesome.min.css">

	<!-- Template global css file. Requared all pages -->
	<link rel="stylesheet" type="text/css" href="css/global.style.css">
</head>

<body>
	
	<div class="wrapper">
        <?php 
		include("menu/nav-menu.php");
		?>
		<div class="wrapper-inline">
			<!-- Header area start -->
			<header> <!-- extra class no-background -->
				<a class="go-back-link" href="main.php"><i class="fa fa-arrow-left"></i></a>
				<h1 class="page-title">Info Terbaru </h1>
				<div class="navi-menu-button">
					<em></em>
					<em></em>
					<em></em>
				</div>
			</header>
			<!-- Header area end -->
			<!-- Page content start -->
			<main>
				<section class="container">
                    <div class="section-head">
                        <h4 class="title-main">Lebih Banyak Informasi</h4>
                        <!-- <a class="c-btn" href="{{ url('/info') }}">Lebih Banyak</a> -->
                    </div>
                    <?php
                         while($user_data = mysqli_fetch_array($result)) {  
                    ?> 
					
					<div class="news-list-item">
                        <div class="list-image">
						<?php 
							if (isset($user_data['information_img'])) {?>
							<img class="profil-img" alt="User Avatar" src="imageinfo/<?php echo $user_data['information_img']; ?>" width="100" height="100">
						<?php } else { ?>
							<img class="profil-img" alt="User Avatar" src="img/image18.png" width="100" height="100">
						<?php } ?>
						</div>
						<div class="list-content">
                            <h2 class="list-title"><a href="post-detail.php?i=<?php echo $user_data['information_id'] ?>"><?php echo $user_data['information_title']; ?> </a></h2>
							<a href="#" class="list-author"><?php echo $user_data['user_user_id']; ?></a> - <span class="list-time"><?php echo $user_data['information_input_date']; ?></span>
						</div>
                        <?php
                            }
                        ?>
					</div>

				</section>

				<?php 
					include("footer/footer.php");
				?>
			</main>
			<!-- Page content end -->
		</div>
	</div>


	<!--Page loader DOM Elements. Requared all pages-->
	<div class="sweet-loader">
		<div class="box">
		  	<div class="circle1"></div>
		  	<div class="circle2"></div>
		  	<div class="circle3"></div>
		</div>
	</div>

	<!-- JQuery library file. requared all pages -->
	<script src="js/jquery-3.2.1.min.js"></script>

	<!-- Template global script file. requared all pages -->
	<script src="js/global.script.js"></script>

	
</body>

</html>