<?php
  session_start();
  if(!isset($_SESSION["student_id"])){
    header("Location:login.php");
  }
  include_once("config/config.php");
  // include("login_action.php");

  $student_full_name=$_SESSION["student_full_name"];
  $class_name=$_SESSION["class_name"];
  $majors_name=$_SESSION["majors_name"];
  $student_id=$_SESSION["student_id"];
  $student_nis=$_SESSION["student_nis"];
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=0">
    <title>Upload Bukti</title>

    <!-- Google font file. If you want you can change. -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:400,600,700,900" rel="stylesheet">
    <!-- <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.min.css"> -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    

    <!-- Fontawesome font file css -->
    <link rel="stylesheet" type="text/css" href="css/font-awesome.min.css">

    <!-- Template global css file. Requared all pages -->
    <link rel="stylesheet" type="text/css" href="css/global.style.css">

  </head>

  <body>
    
  
    <!-- Tampilan baru -->
      <div class="wrapper">
          <?php 
            include("menu/nav-menu.php");
          ?>
        <div class="wrapper-inline">
            <!-- Header area start -->
            <header> <!-- extra class no-background -->
              <a class="go-back-link" href="main.php"><i class="fa fa-arrow-left"></i></a>
              <h1 class="page-title">Upload Bukti Pembayaran</h1>
              <div class="navi-menu-button">
                <em></em>
                <em></em>
                <em></em>
              </div>
            </header>
            <!-- Header area end -->
            <!-- Page content start -->
            <main class="fix-top-menu">
                <div class="container">
                    <div class="form-mini-divider"></div>

                    <?php
                      $siswa_id = $_SESSION["student_id"];

                      $siswa_bayar = "SELECT `student`.`student_full_name`, `student`.`student_nis`, `student`.`student_born_place`, `student`.`student_address`, 
                      `student`.`student_gender`, `student`.`student_parent_phone`,`majors`.`majors_name`, `majors`.`majors_id`, 
                      `majors`.`majors_short_name`, `class`.`class_name`, `class`.`class_id`
                      FROM `student`
                      LEFT JOIN `class` ON `class`.class_id = `student`.class_class_id
                      LEFT JOIN `majors` ON `student`.majors_majors_id = `majors`.majors_id
                      WHERE student_id = '$siswa_id' 
                      ORDER BY `student_id` ASC";
                    ?>
                    <?php
                      $bukti = mysqli_query($mysqli, $siswa_bayar);
                      while($row = mysqli_fetch_array($bukti)){
                    ?>

                    <div class="container">
                      <div class="form-divider"></div>
                      <div class="form-label-divider"><span>Data Siswa</span></div>
                      <div class="form-divider"></div>

                        <div class="form-row-group with-icons">
                            <div class="form-row no-padding">
                                <i class="fa fa-id-card-o"></i>
                                <input type="text" name="aaa" class="form-element" placeholder="Username" value="<?php echo $row['student_nis']; ?>">
                            </div>
                            <div class="form-row no-padding">
                                <i class="fa fa-user"></i>
                                <input type="email" name="aaa" class="form-element" placeholder="Email" value="<?php echo $row['student_full_name']; ?>">
                            </div>
                            <div class="form-row no-padding">
                                <i class="fa fa-graduation-cap"></i>
                                <input type="text" name="aaa" class="form-element" placeholder="Password" value="<?php echo $row['class_name']; ?>">
                            </div>
                            <div class="form-row no-padding">
                                <i class="fa fa-building-o"></i>
                                <input type="text" name="aaa" class="form-element" placeholder="Password" value="<?php echo $row['majors_name']; ?>">
                            </div>
                        </div>
                    </div>
                    <?php
                    }
                    ?>
                    
                    <div class="form-divider"></div>
                    <div class="form-row">
                        <a href="tambah_upload.php" class="button circle block green">Tambah Bukti Pembayaran</a>
                    </div>
                    
                    <hr>
                    <?php 
                      if (isset($_GET['pesan']) AND $_GET['pesan'] == 'berhasil') {?>
                      <div class="alert alert-success">
                          <strong>Berhasil!</strong> Bukti berhasil ditambahkan.
                      </div>
                    <?php } ?>

                    <?php 
                      if (isset($_GET['pesan']) AND $_GET['pesan'] == 'update') {?>
                      <div class="alert alert-success">
                        <strong>Berhasil!</strong> Bukti berhasil dibatalkan. Silahkan masukkan file (foto) yang sesuai.
                      </div>
                    <?php } ?>

                    <?php 
                      if (isset($_GET['pesan']) AND $_GET['pesan'] == 'hapus') {?>
                      <div class="alert alert-success">
                        <strong>Berhasil!</strong> Bukti Pembayaran Berhasil dihapus.
                      </div>
                    <?php } ?>

                    <section class="container">
                        <div class="table-responsive">
                          <table id="example" class="table table-striped table-bordered">
                              <thead class="thead-dark">
                                <tr>
                                  <th>No</th>
                                  <th>Tanggal</th>
                                  <th>Gambar</th>
                                  <th>Keterangan</th>
                                  <th>Status</th>
                                  <th>Action</th>
                                </tr>
                              </thead>
                              <tbody>
                                <?php
                                // jalankan query untuk menampilkan semua data diurutkan berdasarkan nim
                                $query = "SELECT * FROM prove ORDER BY prove_date DESC";
                                $result = mysqli_query($mysqli, $query);
                                //mengecek apakah ada error ketika menjalankan query
                                if(!$result){
                                  die ("Query Error: ".mysqli_errno($mysqli).
                                    " - ".mysqli_error($mysqli));
                                }

                                //buat perulangan untuk element tabel dari data mahasiswa
                                $no = 1; //variabel untuk membuat nomor urut
                                // hasil query akan disimpan dalam variabel $data dalam bentuk array
                                // kemudian dicetak dengan perulangan while
                                while($row = mysqli_fetch_assoc($result))
                                {
                                ?>
                                <tr id ="<?php echo $row['prove_id'] ?>">
                                  <td><?php echo $no; ?></td>
                                  <td><?php echo date ("d-F-Y", strtotime ($row['prove_date'])); ?></td>
                                  <td style="text-align: center;"><img src="gambar/<?php echo $row['prove_img']; ?>" style="width: 120px;"></td>
                                  <td><?php echo substr($row['prove_note'], 0, 20); ?></td>
                                  <td><?php 
                                        if ($row['prove_status']==1) { echo 'DIVERIFIKASI [Sudah Diverifikasi Admin]'; }
                                        else if ($row['prove_status']==0) { echo 'DIBATALKAN [Dibatalkan oleh Pengirim]'; }
                                        else if ($row['prove_status']==2) { echo 'DIBAYAR [Menunggu Verifikasi Admin]'; }
                                        else if ($row['prove_status']==3) { echo 'DITOLAK [Bukti Transfer Ditolak Admin]'; } 
                                      ?>
                                  </td>
                                  <td>
                                      <?php 
                                        if ($row['prove_status']==1) { echo '<a href="#" class="button circle green" disabled> Terverifikasi </a>'; }
                                        else if ($row['prove_status']==0) { echo '<a href="#" class="button circle" disabled> Dibatalkan </a>'; }
                                        else if ($row['prove_status']==2) { echo '<a href="edit_upload.php?id='.$row['prove_id'].'" class="button circle yellow"> Batalkan? </a>'; }
                                        else if ($row['prove_status']==3) { echo '<a href="#" class="button circle red" disabled> Ditolak </a>'; } 
                                      ?>
                                  </td>

                              </tr>
                                
                                  
                                <?php
                                  $no++; //untuk nomor urut terus bertambah 1
                                }
                                ?>
                              </tbody>
                          </table>
                        </div>
                    </section>
                    
                    <div class="form-divider"></div>
                    
                    <?php 
                        include("footer/footer.php");
                    ?> 
                </div>
            </main>
          <!-- Page content end -->
        </div>
      </div>
    <!-- Tampilan baru -->
      <!-- <div class="modal fade" id="modal_delete">
        <div class="modal-dialog">
          <div class="modal-content" style="margin-top:100px;">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
              <h4 class="modal-title" style="text-align:center;">Anda yakin akan menghapus data ini.. ?</h4>
            </div>
                      
            <div class="modal-footer" style="margin:0px; border-top:0px; text-align:center;">
              <a href="#" class="btn btn-danger btn-sm" id="delete_link">Hapus</a>
              <button type="button" class="btn btn-success btn-sm" data-dismiss="modal">Cancel</button>
            </div>
          </div>
        </div>
      </div> -->

      <!--Page loader DOM Elements. Requared all pages-->
      <div class="sweet-loader">
        <div class="box">
            <div class="circle1"></div>
            <div class="circle2"></div>
            <div class="circle3"></div>
        </div>
      </div>

      <!-- JQuery library file. requared all pages -->
      <script src="js/jquery-3.2.1.min.js"></script>

      <!-- Template global script file. requared all pages -->
      <script src="js/global.script.js"></script>

      <!-- JQuery DataTable -->
      <script src="js/jquery-datatable.js"></script>
      <script src="js/jquery-3.5.1.js"></script>
      <script src="js/jquery-datatable-bootstrap4.js"></script>

      <script>
          $(document).ready(function() {
          $('#example').DataTable();
      } );
      </script>

      <script type="text/javascript">
          function confirm_modal(delete_url)
          {
            $('#modal_delete').modal('show', {backdrop: 'static'});
            document.getElementById('delete_link').setAttribute('href' , delete_url);
          }
      </script>

      <script src="js/jquery.min.js"></script>
      <script src="js/bootstrap.min.js"></script>

  </body>

</html>