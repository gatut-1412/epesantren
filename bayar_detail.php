<?php
    session_start();
    include_once("config/config.php");
    if(empty($_SESSION['student_nis'])){
    header('location:main.php');
    }
    if((time() - $_SESSION['auto_logout_time'])>900){
    header('location:logout1.php');
    }
?>

<?php
    $student_full_name=$_SESSION["student_full_name"];
    $class_name=$_SESSION["class_name"];
    $majors_name=$_SESSION["majors_name"];
    $student_id=$_SESSION["student_id"];
    $student_nis=$_SESSION["student_nis"];
    $foldersekolah=$_SESSION["folder"];
?>

<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=0">
	<title>Mobile Template</title>

	<!-- Google font file. If you want you can change. -->
	<link href="https://fonts.googleapis.com/css?family=Nunito:400,600,700,900" rel="stylesheet">

	<!-- Fontawesome font file css -->
	<link rel="stylesheet" type="text/css" href="css/font-awesome.min.css">

	<!-- Template global css file. Requared all pages -->
	<link rel="stylesheet" type="text/css" href="css/global.style.css">
</head>

<body>
	
	<div class="wrapper">
		<?php 
			include("menu/nav-menu.php");
		?>
		<div class="wrapper-inline">
			<!-- Header area start -->
			<header> <!-- extra class no-background -->
				<a class="go-back-link" href="javascript:history.back();"><i class="fa fa-arrow-left"></i></a>
				<h1 class="page-title">Detail Pembayaran</h1>
				<div class="navi-menu-button">
					<em></em>
					<em></em>
					<em></em>
				</div>
			</header>
			<!-- Header area end -->
			<!-- Page content start -->
			<main>
				<section class="container">


					<!--expendable list item -->
					<div class="expandable-item accordion" data-group="accordion1">
						<div class="expandable-header">
							<h3 class="list-title">Nama Santri</h3>
							<i class="list-arrow fa fa-angle-down"></i>
						</div>

						<?php  {
							$siswaID = $_SESSION["student_id"];

							$siswa = "SELECT `student`.`student_full_name`, `student_nis`, `student_born_place`, `student_img`, `student_address`, `student_gender`, `student_born_date`, `student_name_of_mother`, `student_name_of_father`, `student_parent_phone`, 
							`majors_name`, `majors_id`, `majors_short_name`, `madin_id`, `madin_name`, `class_name`, `class_id`
							FROM `student`
							LEFT JOIN `class` ON `class`.class_id = `student`.class_class_id
							LEFT JOIN `majors` ON `student`.majors_majors_id = `majors`.majors_id
							LEFT JOIN `madin` ON `madin`.`madin_id` = `student`.`student_madin`
							WHERE student_id = '$siswaID' 
							ORDER BY `student_id` ASC";
						?>

						<?php
							$mutasi = mysqli_query($mysqli, $siswa);
							while($row = mysqli_fetch_array($mutasi)) {
						?>
						<div class="expandable-content">
							<div class="padding-content">
								<div class="form-row-group with-icons">
									<label>NIS</label>
									<div class="form-row-group with-icons">
										<div class="form-row no-padding">
											<i class="fa fa-id-card-o"></i>
											<input type="text" name="student_nis" class="form-element" placeholder="nis" value="<?php echo $row['student_nis']; ?>" readonly>
										</div>
									</div>
									<label>Nama Santri</label>
									<div class="form-row-group with-icons">
										<div class="form-row no-padding">
											<i class="fa fa-id-card-o"></i>
											<input type="text" name="student_nis" class="form-element" placeholder="nis" value="<?php echo $row['student_full_name']; ?>" readonly>
										</div>
									</div>
									<label>Kelas</label>
									<div class="form-row-group with-icons">
										<div class="form-row no-padding">
											<i class="fa fa-id-card-o"></i>
											<input type="text" name="student_nis" class="form-element" placeholder="nis" value="<?php echo $row['class_name']; ?>" readonly>
										</div>
									</div>
									<label>Unit</label>
									<div class="form-row-group with-icons">
										<div class="form-row no-padding">
											<i class="fa fa-id-card-o"></i>
											<input type="text" name="student_nis" class="form-element" placeholder="nis" value="<?php echo $row['majors_name']; ?>" readonly>
										</div>
									</div>
								</div>
			                    <!-- <div>
			                        <ul class="courses-list list-unstyled mb-0">
			                            <li>
			                                <a href="#" class="d-flex align-items-center justify-content-between">
			                                    <div class="d-flex align-items-center">
			                                        <img class="img-xs" src="img/product/con1.png" alt="Course image">
			                                        <div class="ml-10">
			                                          <h4 class="courses-name">Pronunciation Gudience</h4>
			                                          <small class="text-muted c-period"><i class="fa fa-clock-o mr-1"></i> 3 Hours</small>
			                                        </div>
			                                      </div>
			                                    </a><div><a href="#" class="d-flex align-items-center justify-content-between">
			                                        </a><div class="d-block"><a href="#" class="d-flex align-items-center justify-content-between">
			                                            </a><a href="#" class="c-fav"><i class="fa fa-heart"></i> </a>
			                                        </div>
			                                    </div>
			                                
			                            </li>
			                            <li class="s5-bg">
			                                <a href="#" class="d-flex align-items-center justify-content-between">
			                                    <div class="d-flex align-items-center">
			                                        <img class="img-xs" src="img/product/con2.png" alt="Course image">
			                                        <div class="ml-10">
			                                          <h4 class="courses-name">Common Conversation</h4>
			                                          <small class="text-muted c-period"><i class="fa fa-clock-o mr-1"></i> 3 Hours</small>
			                                        </div>
			                                      </div>
			                                    </a><div><a href="#" class="d-flex align-items-center justify-content-between">
			                                        </a><div class="d-block"><a href="#" class="d-flex align-items-center justify-content-between">
			                                            </a><a href="#" class="c-fav v2"><i class="fa fa-heart"></i> </a>
			                                        </div>
			                                    </div>
			                                
			                            </li>
			                            <li>
			                                <a href="#" class="d-flex align-items-center justify-content-between">
			                                    <div class="d-flex align-items-center">
			                                        <img class="img-xs" src="img/product/con3.png" alt="Course image">
			                                        <div class="ml-10">
			                                          <h4 class="courses-name">Most Searching Words</h4>
			                                          <small class="text-muted c-period"><i class="fa fa-clock-o mr-1"></i> 3 Hours</small>
			                                        </div>
			                                      </div>
			                                    </a><div><a href="#" class="d-flex align-items-center justify-content-between">
			                                        </a><div class="d-block"><a href="#" class="d-flex align-items-center justify-content-between">
			                                            </a><a href="#" class="c-fav"><i class="fa fa-heart"></i> </a>
			                                        </div>
			                                    </div>
			                                
			                            </li>
			                            <li>
			                                <a href="#" class="d-flex align-items-center justify-content-between">
			                                    <div class="d-flex align-items-center">
			                                        <img class="img-xs" src="img/product/con4.png" alt="Course image">
			                                        <div class="ml-10">
			                                          <h4 class="courses-name">50 Press and Idioms</h4>
			                                          <small class="text-muted c-period"><i class="fa fa-clock-o mr-1"></i> 3 Hours</small>
			                                        </div>
			                                      </div>
			                                    </a><div><a href="#" class="d-flex align-items-center justify-content-between">
			                                        </a><div class="d-block"><a href="#" class="d-flex align-items-center justify-content-between">
			                                            </a><a href="#" class="c-fav"><i class="fa fa-heart"></i> </a>
			                                        </div>
			                                    </div>
			                                
			                            </li>
			                        </ul>
			                    	

			                    </div> -->
							</div>
						</div>
						<?php
						}
						}
						?>
					</div>
					<!--expendable list item -->

					<!--expendable list item -->
					<div class="expandable-item accordion" data-group="accordion1">
						<div class="expandable-header">
							<h3 class="list-title">Detail Pembayaran</h3>
							<i class="list-arrow fa fa-angle-down"></i>
						</div>
						<div class="expandable-content">
							<div class="padding-content">

			                	<div class="section-head">
			                        <h4 class="title-main">Detail Pembayaran</h4>
			                    </div>

			                    <div>
			                        <ul class="courses-list list-unstyled mb-0">
										<div class="form-divider"></div>
										<!-- <label>Bayar Bulanan</label> -->
										<div class="form-divider"></div>
												<div class="form-label-divider"><span>Pembayaran Bulanan</span></div>
										<div class="form-divider"></div>
										<div class="form-row-group with-icons">
											<div class="form-row no-padding">
												<i class="fa fa-user"></i>
												<input type="text" name="student_full_name" class="form-element" placeholder="name" value="Rp. 500.000" >
											</div>
										</div>
										<div class="form-divider"></div>
												<div class="form-label-divider"><span>Pembayaran Bebas</span></div>
										<div class="form-divider"></div>
										<div class="form-row-group with-icons">
											<div class="form-row no-padding">
												<i class="fa fa-home"></i>
												<input type="text" name="student_born_place" class="form-element" placeholder="tempatlahir" value="Cicil 1 = Rp. 100.000" >
											</div>
										</div>
										<div class="form-divider"></div>
												<div class="form-label-divider"><span>Total</span></div>
										<div class="form-divider"></div>
										<div class="form-row-group with-icons">
											<div class="form-row no-padding">
												<i class="fa fa-calendar"></i>
												<input type="text" name = "student_born_date" id = "date" class="form-element" placeholder="tanggallahir" value="Total : Rp. .....">
											</div>
										</div>
			                        </ul>
			                    </div>

							</div>
						</div>
					</div>
					<!--expendable list item -->

					<!--expendable list item -->
					<div class="expandable-item">
						<div class="expandable-header">
							<h3 class="list-title">Bayar Via</h3>
							<i class="list-arrow fa fa-angle-down"></i>
						</div>
						<div class="expandable-content" style="">
							<div class="padding-content">
								<div class="form-row no-padding">
									<select class="form-element">
										<option value="" selected="">Virtual Account BNI</option>
										<option value="1">Virtual Account Bank Mandiri</option>
										<option value="1">Virtual Account CIMB Niaga</option>
										<option value="1">Virtual Account Arta Graha</option>
										<option value="1">Retail Alfamart/Alfamidi</option>
									</select>
								</div>
							</div>
						</div>
					</div>
					<!--expendable list item -->

					<!--expendable list item -->
					<!-- <div class="expandable-item accordion" data-group="accordion1">
						<div class="expandable-header">
							<h3 class="list-title">Angular Course</h3>
							<i class="list-arrow fa fa-angle-down"></i>
						</div>
						<div class="expandable-content">
							<div class="padding-content">
								<img src="img/product/course4.png" alt="">
								
								<div class="form-mini-divider"></div>
								<p class="mb-15">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took </p>

			                	<div class="section-head">
			                        <h4 class="title-main">Course Content</h4>
			                        <a class="c-btn" href="#">more</a>
			                    </div>

			                    <div>
			                        <ul class="courses-list list-unstyled mb-0">
			                            <li>
			                                <a href="#" class="d-flex align-items-center justify-content-between">
			                                    <div class="d-flex align-items-center">
			                                        <img class="img-xs" src="img/product/con1.png" alt="Course image">
			                                        <div class="ml-10">
			                                          <h4 class="courses-name">Pronunciation Gudience</h4>
			                                          <small class="text-muted c-period"><i class="fa fa-clock-o mr-1"></i> 3 Hours</small>
			                                        </div>
			                                      </div>
			                                    </a><div><a href="#" class="d-flex align-items-center justify-content-between">
			                                        </a><div class="d-block"><a href="#" class="d-flex align-items-center justify-content-between">
			                                            </a><a href="#" class="c-fav"><i class="fa fa-heart"></i> </a>
			                                        </div>
			                                    </div>
			                                
			                            </li>
			                            <li class="s5-bg">
			                                <a href="#" class="d-flex align-items-center justify-content-between">
			                                    <div class="d-flex align-items-center">
			                                        <img class="img-xs" src="img/product/con2.png" alt="Course image">
			                                        <div class="ml-10">
			                                          <h4 class="courses-name">Common Conversation</h4>
			                                          <small class="text-muted c-period"><i class="fa fa-clock-o mr-1"></i> 3 Hours</small>
			                                        </div>
			                                      </div>
			                                    </a><div><a href="#" class="d-flex align-items-center justify-content-between">
			                                        </a><div class="d-block"><a href="#" class="d-flex align-items-center justify-content-between">
			                                            </a><a href="#" class="c-fav v2"><i class="fa fa-heart"></i> </a>
			                                        </div>
			                                    </div>
			                                
			                            </li>
			                            <li>
			                                <a href="#" class="d-flex align-items-center justify-content-between">
			                                    <div class="d-flex align-items-center">
			                                        <img class="img-xs" src="img/product/con3.png" alt="Course image">
			                                        <div class="ml-10">
			                                          <h4 class="courses-name">Most Searching Words</h4>
			                                          <small class="text-muted c-period"><i class="fa fa-clock-o mr-1"></i> 3 Hours</small>
			                                        </div>
			                                      </div>
			                                    </a><div><a href="#" class="d-flex align-items-center justify-content-between">
			                                        </a><div class="d-block"><a href="#" class="d-flex align-items-center justify-content-between">
			                                            </a><a href="#" class="c-fav"><i class="fa fa-heart"></i> </a>
			                                        </div>
			                                    </div>
			                                
			                            </li>
			                            <li>
			                                <a href="#" class="d-flex align-items-center justify-content-between">
			                                    <div class="d-flex align-items-center">
			                                        <img class="img-xs" src="img/product/con4.png" alt="Course image">
			                                        <div class="ml-10">
			                                          <h4 class="courses-name">50 Press and Idioms</h4>
			                                          <small class="text-muted c-period"><i class="fa fa-clock-o mr-1"></i> 3 Hours</small>
			                                        </div>
			                                      </div>
			                                    </a><div><a href="#" class="d-flex align-items-center justify-content-between">
			                                        </a><div class="d-block"><a href="#" class="d-flex align-items-center justify-content-between">
			                                            </a><a href="#" class="c-fav"><i class="fa fa-heart"></i> </a>
			                                        </div>
			                                    </div>
			                                
			                            </li>
			                        </ul>
			                    	

			                    </div>

							</div>
						</div>
					</div> -->
					<!--expendable list item -->

					<div class="form-mini-divider"></div>
					<a href="#" class="button circle block orange"><i class="fa fa-shopping-basket"></i> Bayar</a>
					<div class="form-mini-divider"></div>

					<!-- <div class="form-divider"></div>
					<div class="form-label-divider"><span>OR</span></div>
					<div class="form-divider"></div> -->

					<!-- <div class="txt-center">
						<a href="product-list.html" class="mb-15 d-block">Continue shopping <i class="fa fa-arrow-right"></i></a>
					</div> -->


					

				</section>
			</main>
			<!-- Page content end -->
		</div>
	</div>


	<!--Page loader DOM Elements. Requared all pages-->
	<div class="sweet-loader">
		<div class="box">
		  	<div class="circle1"></div>
		  	<div class="circle2"></div>
		  	<div class="circle3"></div>
		</div>
	</div>

	<!-- JQuery library file. requared all pages -->
	<script src="js/jquery-3.2.1.min.js"></script>

	<!-- Template global script file. requared all pages -->
	<script src="js/global.script.js"></script>

	
</body>


</html>