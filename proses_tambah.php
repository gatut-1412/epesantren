<?php
  include_once("config/config.php");
  
	// membuat variabel untuk menampung data dari form
    $gambar_produk = $_FILES['gambar_upload']['name'];
    $deskripsi     = $_POST['keterangan'];
    $id_siswa      = $_POST['student_id'];
    $acc           = $_POST['status'];
  
//cek dulu jika ada gambar produk jalankan coding ini
if($gambar_produk != "") {
  $ekstensi_diperbolehkan = array('png','jpg'); //ekstensi file gambar yang bisa diupload 
  $x = explode('.', $gambar_produk); //memisahkan nama file dengan ekstensi yang diupload
  $ekstensi = strtolower(end($x));
  $file_tmp = $_FILES['gambar_upload']['tmp_name'];
  $angka_acak     = rand(1,999);
  $nama_gambar_baru = $angka_acak.'-'.$gambar_produk; //menggabungkan angka acak dengan nama file sebenarnya
        if(in_array($ekstensi, $ekstensi_diperbolehkan) === true)  {     
                move_uploaded_file($file_tmp, 'gambar/'.$nama_gambar_baru); //memindah file gambar ke folder gambar
                  // jalankan query INSERT untuk menambah data ke database pastikan sesuai urutan (id tidak perlu karena dibikin otomatis)
                  $query = "INSERT INTO prove (prove_img, prove_note, prove_status, prove_student_id) VALUES ('$nama_gambar_baru', '$deskripsi', '$acc', '$id_siswa')";
                  $result = mysqli_query($mysqli, $query);
                  // periska query apakah ada error
                  if(!$result){
                      die ("Query gagal dijalankan: ".mysqli_errno($mysqli).
                           " - ".mysqli_error($mysqli));
                  } else {
                    //tampil alert dan akan redirect ke halaman index.php
                    //silahkan ganti index.php sesuai halaman yang akan dituju
                    // echo "<script>alert('Data berhasil ditambah.');window.location='upload_bukti.php';</script>";
                    header("location:upload_bukti.php?pesan=berhasil");
                  }

            } else {     
             //jika file ekstensi tidak jpg dan png maka alert ini yang tampil
                // echo "<script>alert('Ekstensi gambar yang boleh hanya jpg atau png.');window.location='tambah_upload.php';</script>";
                header("location:tambah_upload.php?pesan=gagal");
            }
} else {
                  $query = "INSERT INTO prove (prove_img, prove_note, prove_status, prove_student_id) VALUES ('$nama_gambar_baru', '$deskripsi', '$acc', '$id_siswa')";
                  $result = mysqli_query($mysqli, $query);
                  // periska query apakah ada error
                  if(!$result){
                      die ("Query gagal dijalankan: ".mysqli_errno($mysqli).
                           " - ".mysqli_error($mysqli));
                  } else {
                    //tampil alert dan akan redirect ke halaman index.php
                    //silahkan ganti index.php sesuai halaman yang akan dituju
                    // echo "<script>alert('Data berhasil ditambah.');window.location='upload_bukti.php';</script>";
                    header("location:upload_bukti.php?pesan=berhasil");
                  }
}
?>