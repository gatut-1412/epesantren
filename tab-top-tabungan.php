<?php
    session_start();
    include_once("config/config.php");
    if(empty($_SESSION['student_nis'])){
    header('location:main.php');
    }
    if((time() - $_SESSION['auto_logout_time'])>900){
    header('location:logout1.php');
    }
?>

<?php
$student_full_name=$_SESSION["student_full_name"];
$class_name=$_SESSION["class_name"];
$majors_name=$_SESSION["majors_name"];
$foldersekolah=$_SESSION["folder"];
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=0">
    <title>Tabungan</title>

	<!-- Google font file. If you want you can change. -->
	<link href="https://fonts.googleapis.com/css?family=Nunito:400,600,700,900" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.min.css">

	<!-- Fontawesome font file css -->
	<link rel="stylesheet" type="text/css" href="css/font-awesome.min.css">

	<!-- Template global css file. Requared all pages -->
	<link rel="stylesheet" type="text/css" href="css/global.style.css">
</head>

<body>
	
	<div class="wrapper">
        <?php 
		    include("menu/nav-menu.php");
		?>
		<div class="wrapper-inline">
			<!-- Header area start -->
			<header> <!-- extra class no-background -->
				<a class="go-back-link" href="main.php"><i class="fa fa-arrow-left"></i></a>
				<h1 class="page-title">Tabungan Siswa</h1>
				<div class="navi-menu-button">
					<em></em>
					<em></em>
					<em></em>
				</div>
			</header>
			<!-- Header area end -->
			<!-- Page content start -->
			<main class="fix-top-menu">

                    <div class="container">
                    
                    <div class="form-mini-divider"></div>
                    
                            <section class="container">

                                <div class="content-item" id="contentRekap">
                                    
                                    <h3>Informasi Rekap Tabungan</h3>

                                    <div class="form-divider"></div>
                                    <div class="form-label-divider"><span>Rekap Tabungan</span></div>
                                    <div class="form-divider"></div>

                                    
                                    <?php 

                                            $siswaID = $_SESSION["student_id"];

                                            $tabungan = "SELECT banking_date as date, banking_note as note, banking_code as code, banking_debit AS debit, banking_kredit AS kredit, 
                                            @saldo:=@saldo+banking_debit-banking_kredit AS saldo 
                                            FROM `banking` JOIN (SELECT @saldo:=0) a WHERE banking_student_id = '$siswaID'";

                                            $hasil=mysqli_query($mysqli, $tabungan);
                                            $i =1;
                                            $sumDebit  = 0;
                                            $sumKredit = 0;

                                            while($data = mysqli_fetch_array($hasil)) {
                                                $sumDebit += $data['debit'];
                                                $sumKredit += $data['kredit'];
                                        }
                                    ?>

                                    <div class="form-row-group with-icons">
                                        <div class="form-row no-padding">
                                            <i class="fa fa-plus-circle"></i>
                                            <input type="text" name="aaa" class="form-element" placeholder="Username" value=" Masuk : <?php echo 'Rp. ' . number_format($sumDebit, 0, ',', '.') ?>" readonly>
                                        </div>
                                        <div class="form-row no-padding">
                                            <i class="fa fa-minus-circle"></i>
                                            <input type="text" name="aaa" class="form-element" placeholder="Email" value=" Tarik : <?php echo 'Rp. ' . number_format($sumKredit, 0, ',', '.') ?>" readonly>
                                        </div>
                                        <div class="form-row no-padding">
                                            <i class="fa fa-money"></i>
                                            <input type="text" name="aaa" class="form-element" placeholder="Password" value=" Saldo : <?php echo 'Rp. ' . number_format(($sumDebit) - ($sumKredit), 0, ',', '.') ?>" readonly>
                                        </div>
                                    </div>
                                    <div class="form-divider"></div>
                                    
                                    <div class="table-responsive">          
                                        <table id="example" class="table table-striped table-bordered">
                                            <thead class="thead-dark">
                                                <tr>
                                                    <th>No</th>
                                                    <th>Tanggal</th>
                                                    <th>Debit</th>
                                                    <th>Kredit</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
                                                $mutation = mysqli_query($mysqli,"SELECT banking_date as date, banking_note as note, banking_code as code, banking_debit AS debit, banking_kredit AS kredit, 
                                                @saldo:=@saldo+banking_debit-banking_kredit AS saldo 
                                                FROM `banking` JOIN (SELECT @saldo:=0) a WHERE banking_student_id = '$siswaID'  ORDER BY banking_date DESC");
                                                
                                                $no = 1;
                                                // $res = $conn->query("SELECT * FROM help_keyword");
                                                while($row = $mutation->fetch_assoc()){
                                                    echo '
                                                    <tr>
                                                        <td>'.$no.'</td>
                                                        <td>'.date ("d-F-Y", strtotime ($row['date'])).'</td>
                                                        <td> Rp. '.number_format($row['debit'], 0, ',', '.').' </td>
                                                        <td> Rp. '.number_format($row['kredit'], 0, ',', '.').' </td>
                                                    </tr>
                                                    ';
                                                    $no++;
                                                }
                                                ?>
                                            </tbody>
                                        </table>
                                    </div>

                                </div>
                                <div class="form-divider"></div>
                                <div class="form-row">
                                    <a href="<?php echo 'https://' . $foldersekolah . '.epesantren.com/manage/banking/printBook/?n=' . base64_encode($tahun) . '&r=' . base64_encode($siswaNIS) ?>" class="button circle block blue">Cetak Buku Tabungan</a>
                                </div>
                            </section>
					
                            <?php 
                                include("footer/footer.php");
                            ?>

			</main>
			<!-- Page content end -->
		</div>
    </div>
    
	<!--Page loader DOM Elements. Requared all pages-->
	<div class="sweet-loader">
		<div class="box">
		  	<div class="circle1"></div>
		  	<div class="circle2"></div>
		  	<div class="circle3"></div>
		</div>
	</div>

	<!-- JQuery library file. requared all pages -->
	<script src="js/jquery-3.2.1.min.js"></script>

	<!-- Template global script file. requared all pages -->
    <script src="js/global.script.js"></script>

    <!-- JQuery DataTable -->
    <script src="js/jquery-datatable.js"></script>
    <script src="js/jquery-3.5.1.js"></script>
    <script src="js/jquery-datatable-bootstrap4.js"></script>

    <script>
        $(document).ready(function() {
        $('#example').DataTable();
    } );
    </script>

</body>

</html>