<?php
    session_start();
    include_once("config/config.php");
    if(empty($_SESSION['student_nis'])){
    header('location:main.php');
    }
    if((time() - $_SESSION['auto_logout_time'])>900){
    header('location:logout1.php');
    }
?>

<?php
    $student_full_name=$_SESSION["student_full_name"];
    $class_name=$_SESSION["class_name"];
    $majors_name=$_SESSION["majors_name"];
    $student_id=$_SESSION["student_id"];
    $student_nis=$_SESSION["student_nis"];
    $foldersekolah=$_SESSION["folder"];
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=0">
    <title>Pembayaran</title>

	<!-- Google font file. If you want you can change. -->
	<link href="https://fonts.googleapis.com/css?family=Nunito:400,600,700,900" rel="stylesheet">

	<!-- Fontawesome font file css -->
	<link rel="stylesheet" type="text/css" href="css/font-awesome.min.css">

	<!-- Template global css file. Requared all pages -->
	<link rel="stylesheet" type="text/css" href="css/global.style.css">
</head>

<body>
	
	<div class="wrapper">
        <?php 
		include("menu/nav-menu.php");
		?>
		<div class="wrapper-inline">
			<!-- Header area start -->
			<header> <!-- extra class no-background -->
				<a class="go-back-link" href="main.php"><i class="fa fa-arrow-left"></i></a>
				<h1 class="page-title">Pembayaran <span></h1>
				<div class="navi-menu-button">
					<em></em>
					<em></em>
					<em></em>
				</div>
			</header>
			<!-- Header area end -->
			<!-- Page content start -->
			<main class="fix-top-menu">

                    <div class="container">
                        <form action="<?php echo $_SERVER["PHP_SELF"];?>" method="get">
                            <div class="form-row no-padding">
                                <label for="sel1">Tahun Ajaran:</label>
                                <select class="form-element" id = "period" name="period">
                                <?php                   
                                        $sql="select * from period";
                                        $hasil=mysqli_query($mysqli,$sql);
                                        $no=0;
                                        while ($data = mysqli_fetch_array($hasil)) {

                                        $ket="";
                                            if (isset($_GET['period'])) {
                                            $period = trim($_GET['period']);

                                            if ($period==$data['period_id'])
                                            {
                                            $ket="selected";
                                            }
                                        }
                                ?>
                                <option <?php echo $ket; ?> value="<?php echo $data['period_id'];?>"><?php echo $data['period_start'];?>/<?php echo $data['period_end'];?></option>
                                <?php
                                    }
                                ?>
                                    
                                </select>
                                    <div class="form-divider"></div>
                                    <div class="col-sm-4">
                                        <button type="submit" class="button circle green"><i class="fa fa-search"> </i> Cari Pembayaran</button>
                                    </div>
                            </div>
                        </form>
                    </div>

                    <?php if (isset($_GET['period'])) {
                            $tahun      = $_GET['period'];
                            $siswaID    = $_SESSION["student_id"];

                    ?>
                    <?php }  ?>

                    <!-- TAB CONTROL START -->
                        
                        <div class="tab-item">

                            <div class="tab-menu fix-width">
                                <a class="menu-item active" href="javascript:void(0);" data-content="contentPost"><i class="fa fa-bars"></i> Bulanan</a>
                                <a class="menu-item" href="javascript:void(0);" data-content="contentFavorites"><i class="fa fa-comments"></i> Lainnya</a>
                            </div>

                                <div class="tab-content">

                                    <div class="content-item active" id="contentPost">

                                        <div class="form-divider"></div>
                                        <div class="form-label-divider"><span>Pembayaran Bulanan</span></div>
                                        <div class="form-divider"></div>

                                        <!--expendable list item -->
                                        <?php
                                            $i =1;  
                                            $sasen = "SELECT  `s`.`pos_name`, `d`.`period_id`, `d`.`period_start`, `d`.`period_end`, 
                                                    `p`.`payment_id`, SUM(`b`.`bulan_bill`) as `total`,
                                                    SUM(if(`b`.`bulan_status`='1',`b`.`bulan_bill`,0)) as `dibayar`
                                                    FROM `bulan` as `b` 
                                                    JOIN `payment` as `p` ON `b`.`payment_payment_id` = `p`.`payment_id` 
                                                    JOIN `pos` as `s` ON `s`.`pos_id` = `p`.`pos_pos_id` 
                                                    LEFT JOIN `account` as `a` ON `a`.`account_id` = `b`.`bulan_account_id` 
                                                    JOIN `period` as `d` ON `d`.`period_id` = `p`.`period_period_id`
                                                    JOIN `student` as `t` ON `t`.`student_id` = `b`.student_student_id
                                                    JOIN `month` as `m` ON `m`.`month_id` = `b`.month_month_id
                                                    WHERE `d`.`period_id` = '$tahun' AND student_student_id = '$siswaID'
                                                    GROUP BY `p`.`payment_id`";

                                            $tes=mysqli_query($mysqli, $sasen);
                                            while($row = mysqli_fetch_array($tes)){
                                                    $namePay = $row['pos_name'].' - T.A '.$row['period_start'].'/'.$row['period_end'];
                                                    $payid = $row['payment_id'];
                                                    $jml  = $row['total'];
                                                    $dbyr = $row['dibayar'];

                                                    $bulanan = "SELECT `t`.`student_id`, `t`.`student_nis`, `t`.`student_full_name`, `s`.`pos_name`, 
                                                                `d`.`period_id`, `d`.`period_start`, `d`.`period_end`, `p`.`payment_id`, `p`.`payment_mode`, 
                                                                SUM(`b`.`bulan_bill`) as `total`,
                                                                SUM(if(`b`.`bulan_status`='1',`b`.`bulan_bill`,0)) as `dibayar`,
                                                                SUM(if(`b`.`month_month_id`='1',`b`.`bulan_bill`,0)) as `bill_jul`, 
                                                                SUM(if(`b`.`month_month_id`='2',`b`.`bulan_bill`,0)) as `bill_agu`, 
                                                                SUM(if(`b`.`month_month_id`='3',`b`.`bulan_bill`,0)) as `bill_sep`, 
                                                                SUM(if(`b`.`month_month_id`='4',`b`.`bulan_bill`,0)) as `bill_okt`, 
                                                                SUM(if(`b`.`month_month_id`='5',`b`.`bulan_bill`,0)) as `bill_nov`, 
                                                                SUM(if(`b`.`month_month_id`='6',`b`.`bulan_bill`,0)) as `bill_des`, 
                                                                SUM(if(`b`.`month_month_id`='7',`b`.`bulan_bill`,0)) as `bill_jan`, 
                                                                SUM(if(`b`.`month_month_id`='8',`b`.`bulan_bill`,0)) as `bill_feb`, 
                                                                SUM(if(`b`.`month_month_id`='9',`b`.`bulan_bill`,0)) as `bill_mar`, 
                                                                SUM(if(`b`.`month_month_id`='10',`b`.`bulan_bill`,0)) as `bill_apr`, 
                                                                SUM(if(`b`.`month_month_id`='11',`b`.`bulan_bill`,0)) as `bill_mei`, 
                                                                SUM(if(`b`.`month_month_id`='12',`b`.`bulan_bill`,0)) as `bill_jun`, 
                                                                SUM(if(`b`.`month_month_id`='1',`b`.`bulan_status`,0)) as `status_jul`, 
                                                                SUM(if(`b`.`month_month_id`='2',`b`.`bulan_status`,0)) as `status_agu`, 
                                                                SUM(if(`b`.`month_month_id`='3',`b`.`bulan_status`,0)) as `status_sep`, 
                                                                SUM(if(`b`.`month_month_id`='4',`b`.`bulan_status`,0)) as `status_okt`, 
                                                                SUM(if(`b`.`month_month_id`='5',`b`.`bulan_status`,0)) as `status_nov`, 
                                                                SUM(if(`b`.`month_month_id`='6',`b`.`bulan_status`,0)) as `status_des`, 
                                                                SUM(if(`b`.`month_month_id`='7',`b`.`bulan_status`,0)) as `status_jan`, 
                                                                SUM(if(`b`.`month_month_id`='8',`b`.`bulan_status`,0)) as `status_feb`, 
                                                                SUM(if(`b`.`month_month_id`='9',`b`.`bulan_status`,0)) as `status_mar`, 
                                                                SUM(if(`b`.`month_month_id`='10',`b`.`bulan_status`,0)) as `status_apr`, 
                                                                SUM(if(`b`.`month_month_id`='11',`b`.`bulan_status`,0)) as `status_mei`, 
                                                                SUM(if(`b`.`month_month_id`='12',`b`.`bulan_status`,0)) as `status_jun`,
                                                                MAX(if(`b`.`month_month_id`='1',`b`.`bulan_date_pay`,0)) as `date_pay_jul`, 
                                                                MAX(if(`b`.`month_month_id`='2',`b`.`bulan_date_pay`,0)) as `date_pay_agu`, 
                                                                MAX(if(`b`.`month_month_id`='3',`b`.`bulan_date_pay`,0)) as `date_pay_sep`, 
                                                                MAX(if(`b`.`month_month_id`='4',`b`.`bulan_date_pay`,0)) as `date_pay_okt`, 
                                                                MAX(if(`b`.`month_month_id`='5',`b`.`bulan_date_pay`,0)) as `date_pay_nov`, 
                                                                MAX(if(`b`.`month_month_id`='6',`b`.`bulan_date_pay`,0)) as `date_pay_des`, 
                                                                MAX(if(`b`.`month_month_id`='7',`b`.`bulan_date_pay`,0)) as `date_pay_jan`, 
                                                                MAX(if(`b`.`month_month_id`='8',`b`.`bulan_date_pay`,0)) as `date_pay_feb`, 
                                                                MAX(if(`b`.`month_month_id`='9',`b`.`bulan_date_pay`,0)) as `date_pay_mar`, 
                                                                MAX(if(`b`.`month_month_id`='10',`b`.`bulan_date_pay`,0)) as `date_pay_apr`, 
                                                                MAX(if(`b`.`month_month_id`='11',`b`.`bulan_date_pay`,0)) as `date_pay_mei`, 
                                                                MAX(if(`b`.`month_month_id`='12',`b`.`bulan_date_pay`,0)) as `date_pay_jun`, 
                                                                MAX(if(`b`.`month_month_id`='1',`m`.`month_name`,'')) as `month_name_jul`, 
                                                                MAX(if(`b`.`month_month_id`='2',`m`.`month_name`,'')) as `month_name_agu`, 
                                                                MAX(if(`b`.`month_month_id`='3',`m`.`month_name`,'')) as `month_name_sep`, 
                                                                MAX(if(`b`.`month_month_id`='4',`m`.`month_name`,'')) as `month_name_okt`, 
                                                                MAX(if(`b`.`month_month_id`='5',`m`.`month_name`,'')) as `month_name_nov`, 
                                                                MAX(if(`b`.`month_month_id`='6',`m`.`month_name`,'')) as `month_name_des`, 
                                                                MAX(if(`b`.`month_month_id`='7',`m`.`month_name`,'')) as `month_name_jan`, 
                                                                MAX(if(`b`.`month_month_id`='8',`m`.`month_name`,'')) as `month_name_feb`, 
                                                                MAX(if(`b`.`month_month_id`='9',`m`.`month_name`,'')) as `month_name_mar`, 
                                                                MAX(if(`b`.`month_month_id`='10',`m`.`month_name`,'')) as `month_name_apr`, 
                                                                MAX(if(`b`.`month_month_id`='11',`m`.`month_name`,'')) as `month_name_mei`, 
                                                                MAX(if(`b`.`month_month_id`='12',`m`.`month_name`,'')) as `month_name_jun`,
                                                                MAX(if(`b`.`month_month_id`='1',`a`.`account_description`,'')) as `account_jul`, 
                                                                MAX(if(`b`.`month_month_id`='2',`a`.`account_description`,'')) as `account_agu`, 
                                                                MAX(if(`b`.`month_month_id`='3',`a`.`account_description`,'')) as `account_sep`, 
                                                                MAX(if(`b`.`month_month_id`='4',`a`.`account_description`,'')) as `account_okt`, 
                                                                MAX(if(`b`.`month_month_id`='5',`a`.`account_description`,'')) as `account_nov`, 
                                                                MAX(if(`b`.`month_month_id`='6',`a`.`account_description`,'')) as `account_des`, 
                                                                MAX(if(`b`.`month_month_id`='7',`a`.`account_description`,'')) as `account_jan`, 
                                                                MAX(if(`b`.`month_month_id`='8',`a`.`account_description`,'')) as `account_feb`, 
                                                                MAX(if(`b`.`month_month_id`='9',`a`.`account_description`,'')) as `account_mar`, 
                                                                MAX(if(`b`.`month_month_id`='10',`a`.`account_description`,'')) as `account_apr`, 
                                                                MAX(if(`b`.`month_month_id`='11',`a`.`account_description`,'')) as `account_mei`, 
                                                                MAX(if(`b`.`month_month_id`='12',`a`.`account_description`,'')) as `account_jun`, 
                                                                SUM(if(`b`.`month_month_id`='1',`b`.`bulan_id`,0)) as `month_id_jul`,
                                                                SUM(if(`b`.`month_month_id`='2',`b`.`bulan_id`,0)) as `month_id_agu`,
                                                                SUM(if(`b`.`month_month_id`='3',`b`.`bulan_id`,0)) as `month_id_sep`,
                                                                SUM(if(`b`.`month_month_id`='4',`b`.`bulan_id`,0)) as `month_id_okt`,
                                                                SUM(if(`b`.`month_month_id`='5',`b`.`bulan_id`,0)) as `month_id_nov`,
                                                                SUM(if(`b`.`month_month_id`='6',`b`.`bulan_id`,0)) as `month_id_des`,
                                                                SUM(if(`b`.`month_month_id`='7',`b`.`bulan_id`,0)) as `month_id_jan`,
                                                                SUM(if(`b`.`month_month_id`='8',`b`.`bulan_id`,0)) as `month_id_feb`,
                                                                SUM(if(`b`.`month_month_id`='9',`b`.`bulan_id`,0)) as `month_id_mar`,
                                                                SUM(if(`b`.`month_month_id`='10',`b`.`bulan_id`,0)) as `month_id_apr`,
                                                                SUM(if(`b`.`month_month_id`='11',`b`.`bulan_id`,0)) as `month_id_mei`,
                                                                SUM(if(`b`.`month_month_id`='12',`b`.`bulan_id`,0)) as `month_id_jun`
                                                                FROM `bulan` as `b` 
                                                                JOIN `payment` as `p` ON `b`.`payment_payment_id` = `p`.`payment_id` 
                                                                JOIN `pos` as `s` ON `s`.`pos_id` = `p`.`pos_pos_id` 
                                                                LEFT JOIN `account` as `a` ON `a`.`account_id` = `b`.`bulan_account_id` 
                                                                JOIN `period` as `d` ON `d`.`period_id` = `p`.`period_period_id`
                                                                JOIN `student` as `t` ON `t`.`student_id` = `b`.student_student_id
                                                                JOIN `month` as `m` ON `m`.`month_id` = `b`.month_month_id
                                                                WHERE  `d`.`period_id` = '$tahun' AND student_student_id = '$siswaID' AND `p`.`payment_id` = '$payid'
                                                                GROUP BY `p`.`payment_id`";
                                            ?>

                                                    <div class="expandable-item accordion" data-group="accordion1">
                                                        <div class="expandable-header">
                                                            <i class="list-icon fa fa-book"></i>
                                                            <h3 class="list-title"><?php echo $namePay ?></h3>
                                                            <i class="list-arrow fa fa-angle-down"></i>
                                                        </div>
                                                        <div class="expandable-content">
                                                            <div class="padding-content">
                                                                <div class="table-responsive">
                                                                    <table class="table">
                                                                        <tbody>
                                                                            <?php
                                                                            $i =1;  
                                                                                ?>
                                                                            <?php
                                                                            include_once("config/config.php");
                                                                                $hasil=mysqli_query($mysqli,$bulanan);
                                                                                while($row = mysqli_fetch_array($hasil)){
                                                                                $detail = $row['payment_id']
                                                                            ?>
                                                                            <tbody id="demo<?php echo $detail ?>" class="collapse">
                                                                                <div class="form-divider"></div>
                                                                                    <thead class="thead-dark">    
                                                                                        <tr>
                                                                                            <th>No.</th>
                                                                                            <th>Bulan</th>
                                                                                            <th>Tahun</th>
                                                                                            <th>Nominal</th>
                                                                                            <th colspan="2" style="text-align: center;">Status</th>
                                                                                        </tr>
                                                                                    </thead>
                                                                                    <tr class="<?php echo ($row['status_jul'] == 1) ? 'success' : 'danger' ?>">
                                                                                        <td>
                                                                                            1
                                                                                        </td>
                                                                                        <td><?php echo $row['month_name_jul']?></td>
                                                                                        <td><?php echo $row['period_start'] ?></td>
                                                                                        <td><?php echo number_format($row['bill_jul'], 0, ',', '.') ?></td>
                                                                                        <td colspan="2" align="center"><?php echo ($row['status_jul'] == 1) ? 'Lunas' : 'Belum Lunas' ?></td>
                                                                                    </tr>
                                                                                    <tr class="<?php echo ($row['status_agu'] == 1) ? 'success' : 'danger' ?>">
                                                                                        <td>
                                                                                            2
                                                                                        </td>
                                                                                        <td><?php echo $row['month_name_agu'] ?></td>
                                                                                        <td><?php echo $row['period_start'] ?></td>
                                                                                        <td><?php echo number_format($row['bill_agu'], 0, ',', '.') ?></td>
                                                                                        <td colspan="2" align="center"><?php echo ($row['status_agu'] == 1) ? 'Lunas' : 'Belum Lunas' ?></td>
                                                                                    </tr>
                                                                                    <tr class="<?php echo ($row['status_sep'] == 1) ? 'success' : 'danger' ?>">
                                                                                        <td>
                                                                                            3
                                                                                        </td>
                                                                                        <td><?php echo $row['month_name_sep'] ?></td>
                                                                                        <td><?php echo $row['period_start'] ?></td>
                                                                                        <td><?php echo number_format($row['bill_sep'], 0, ',', '.') ?></td>
                                                                                        <td colspan="2" align="center"><?php echo ($row['status_sep'] == 1) ? 'Lunas' : 'Belum Lunas' ?></td>
                                                                                    </tr>
                                                                                    <tr class="<?php echo ($row['status_okt'] == 1) ? 'success' : 'danger' ?>">
                                                                                        <td>
                                                                                            4
                                                                                        </td>
                                                                                        <td><?php echo $row['month_name_okt'] ?></td>
                                                                                        <td><?php echo $row['period_start'] ?></td>
                                                                                        <td><?php echo number_format($row['bill_okt'], 0, ',', '.') ?></td>
                                                                                        <td colspan="2" align="center"><?php echo ($row['status_okt'] == 1) ? 'Lunas' : 'Belum Lunas' ?></td>
                                                                                    </tr>
                                                                                    <tr class="<?php echo ($row['status_nov'] == 1) ? 'success' : 'danger' ?>">
                                                                                        <td>
                                                                                            5
                                                                                        </td>
                                                                                        <td><?php echo $row['month_name_nov'] ?></td>
                                                                                        <td><?php echo $row['period_start'] ?></td>
                                                                                        <td><?php echo number_format($row['bill_nov'], 0, ',', '.') ?></td>
                                                                                        <td colspan="2" align="center"><?php echo ($row['status_nov'] == 1) ? 'Lunas' : 'Belum Lunas' ?></td>
                                                                                    </tr>
                                                                                    <tr class="<?php echo ($row['status_des'] == 1) ? 'success' : 'danger' ?>">
                                                                                        <td>
                                                                                            6
                                                                                        </td>
                                                                                        <td><?php echo $row['month_name_des']  ?></td>
                                                                                        <td><?php echo $row['period_start'] ?></td>
                                                                                        <td><?php echo number_format($row['bill_des'], 0, ',', '.') ?></td>
                                                                                        <td colspan="2" align="center"><?php echo ($row['status_des'] == 1) ? 'Lunas' : 'Belum Lunas' ?></td>
                                                                                    </tr>
                                                                                    <tr class="<?php echo ($row['status_jan'] == 1) ? 'success' : 'danger' ?>">
                                                                                        <td>
                                                                                            7
                                                                                        </td>
                                                                                        <td><?php echo $row['month_name_jan'] ?></td>
                                                                                        <td><?php echo $row['period_end'] ?></td>
                                                                                        <td><?php echo number_format($row['bill_jan'], 0, ',', '.') ?></td>
                                                                                        <td colspan="2" align="center"><?php echo ($row['status_jan'] == 1) ? 'Lunas' : 'Belum Lunas' ?></td>
                                                                                    </tr>
                                                                                    <tr class="<?php echo ($row['status_feb'] == 1) ? 'success' : 'danger' ?>">
                                                                                        <td>
                                                                                            8
                                                                                        </td>
                                                                                        <td><?php echo $row['month_name_feb'] ?></td>
                                                                                        <td><?php echo $row['period_end'] ?></td>
                                                                                        <td><?php echo number_format($row['bill_feb'], 0, ',', '.') ?></td>
                                                                                        <td colspan="2" align="center"><?php echo ($row['status_feb'] == 1) ? 'Lunas' : 'Belum Lunas' ?></td>
                                                                                    </tr>
                                                                                    <tr class="<?php echo ($row['status_mar'] == 1) ? 'success' : 'danger' ?>">
                                                                                        <td>
                                                                                            9
                                                                                        </td>
                                                                                        <td><?php echo $row['month_name_mar'] ?></td>
                                                                                        <td><?php echo $row['period_end'] ?></td>
                                                                                        <td><?php echo number_format($row['bill_mar'], 0, ',', '.') ?></td>
                                                                                        <td colspan="2" align="center"><?php echo ($row['status_mar'] == 1) ? 'Lunas' : 'Belum Lunas' ?></td>
                                                                                    </tr>
                                                                                    <tr class="<?php echo ($row['status_apr'] == 1) ? 'success' : 'danger' ?>">
                                                                                        <td>
                                                                                            10
                                                                                        </td>
                                                                                        <td><?php echo $row['month_name_apr'] ?></td>
                                                                                        <td><?php echo $row['period_end'] ?></td>
                                                                                        <td><?php echo number_format($row['bill_apr'], 0, ',', '.') ?></td>
                                                                                        <td colspan="2" align="center"><?php echo ($row['status_apr'] == 1) ? 'Lunas' : 'Belum Lunas' ?></td>
                                                                                    </tr>
                                                                                    <tr class="<?php echo ($row['status_mei'] == 1) ? 'success' : 'danger' ?>">
                                                                                        <td>
                                                                                            11
                                                                                        </td>
                                                                                        <td><?php echo $row['month_name_mei'] ?></td>
                                                                                        <td><?php echo $row['period_end'] ?></td>
                                                                                        <td><?php echo number_format($row['bill_mei'], 0, ',', '.') ?></td>
                                                                                        <td colspan="2" align="center"><?php echo ($row['status_mei'] == 1) ? 'Lunas' : 'Belum Lunas' ?></td>
                                                                                    </tr>
                                                                                    <tr class="<?php echo ($row['status_jun'] == 1) ? 'success' : 'danger' ?>">
                                                                                        <td>
                                                                                            12
                                                                                        </td>
                                                                                        <td><?php echo $row['month_name_jun'] ?></td>
                                                                                        <td><?php echo $row['period_end'] ?></td>
                                                                                        <td><?php echo number_format($row['bill_jun'], 0, ',', '.') ?></td>
                                                                                        <td colspan="2" align="center"><?php echo ($row['status_jun'] == 1) ? 'Lunas' : 'Belum Lunas' ?></td>
                                                                                    </tr>
                                                                            </tbody>
                                                                            <?php  
                                                                            }
                                                                            ?>
                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                        <?php
                                        }
                                        ?>
                                        <!--expendable list item -->

                                    </div>   

                                    <div class="content-item" id="contentFavorites">
                                                    
                                        <div class="form-divider"></div>
                                        <div class="form-label-divider"><span>Pembayaran Lainnya</span></div>
                                        <div class="form-divider"></div>

                                        <div class="table-responsive">
                                            <table class="table">
                                                <thead class="thead-dark">
                                                        <tr>
                                                            <th>No.</th>
                                                            <th>Jenis Pembayaran</th>
                                                            <th>Total</th>
                                                            <th>Dibayar</th>
                                                            <th>Kekurangan</th>
                                                            <th>Status</th>
                                                        </tr>
                                                </thead>

                                                <?php 
                                                    $bebas = "SELECT `bebas`.`bebas_id`, `bebas_bill`, `bebas_diskon`, `bebas_total_pay`, `bebas_input_date`, 
                                                    `bebas_last_update`, `student_student_id`, `student`.`class_class_id`, `class_name`, `student_full_name`, 
                                                    `student_nis`, `student_name_of_mother`, `student_parent_phone`, `student`.`majors_majors_id`, `majors_name`, 
                                                    `majors_short_name`, `payment_payment_id`, `pos_name`, `payment_type`, `period_period_id`, `period_start`, `period_end`, 
                                                    `madin_id`, `madin_name` 
                                                    FROM `bebas` 
                                                    LEFT JOIN `student` ON `student`.`student_id` = `bebas`.`student_student_id` 
                                                    LEFT JOIN `payment` ON `payment`.`payment_id` = `bebas`.`payment_payment_id` 
                                                    LEFT JOIN `pos` ON `pos`.`pos_id` = `payment`.`pos_pos_id` 
                                                    LEFT JOIN `class` ON `class`.`class_id` = `student`.`class_class_id` 
                                                    LEFT JOIN `period` ON `period`.`period_id` = `payment`.`period_period_id` 
                                                    LEFT JOIN `majors` ON `majors`.`majors_id` = `student`.`majors_majors_id` 
                                                    LEFT JOIN `madin` ON `madin`.`madin_id` = `student`.`student_madin` 
                                                    WHERE `bebas`.`student_student_id` = $siswaID AND `payment`.`period_period_id` = $tahun AND `majors_status` AND student_student_id = '$siswaID'
                                                    ORDER BY `payment_payment_id` ASC";

                                                    $cicil=mysqli_query($mysqli,$bebas);
                                                    $i =1;
                                                    while($row = mysqli_fetch_array($cicil)) {
                                                    $sisa = $row['bebas_bill']-$row['bebas_total_pay'];
                                                    $namePay = $row['pos_name'].' - T.A '.$row['period_start'].'/'.$row['period_end'];
                                                ?>

                                                <tbody>
                                                        <tr style="color:<?php echo ($row['bebas_bill'] == $row['bebas_total_pay']) ? '#00E640' : 'red' ?>">
                                                            <td><?php echo $i ?></td>
                                                            <td><?php echo $namePay ?></td>
                                                            <td><?php echo 'Rp. ' . number_format($row['bebas_bill'], 0, ',', '.') ?></td>
                                                            <td><?php echo 'Rp. ' . number_format($row['bebas_total_pay'], 0, ',', '.') ?></td>
                                                            <td><?php echo 'Rp. ' . number_format($sisa, 0, ',', '.') ?></td>
                                                            <td><label class="label <?php echo ($row['bebas_bill']==$row['bebas_total_pay']) ? 'label-success' : 'label-warning' ?>"><?php echo ($row['bebas_bill']==$row['bebas_total_pay']) ? 'Lunas' : 'Belum Lunas' ?></label></td>
                                                        </tr>
                                                        <?php
                                                        $i++;
                                                        }
                                                        ?>  
                                                </tbody>

                                            </table>
                                        </div>

                                    </div>

                                </div>

                                <?php 
                                    $dateM = date('m');
            
                                    if($dateM == '01'){
                                        $till = '7';
                                    } else if($dateM == '02'){
                                        $till = '8';
                                    } else if($dateM == '03'){
                                        $till = '9';
                                    } else if($dateM == '04'){
                                        $till = '10';
                                    } else if($dateM == '05'){
                                        $till = '11';
                                    } else if($dateM == '06'){
                                        $till = '12';
                                    } else if($dateM == '07'){
                                        $till = '1';
                                    } else if($dateM == '08'){
                                        $till = '2';
                                    } else if($dateM == '09'){
                                        $till = '3';
                                    } else if($dateM == '10'){
                                        $till = '4';
                                    } else if($dateM == '11'){
                                        $till = '5';
                                    } else if($dateM == '12'){
                                        $till = '6';
                                    }
                                ?>

                                <div class="form-divider"></div>
                                <div class="form-row">
                                    <a href="<?php echo 'https://' . $foldersekolah . '.epesantren.co.id/student/billing/print_bill/' .$tahun . '/1/5/' . $siswaID ?>" class="button circle block red">Download Tagihan</a>
                                </div>
                                <div class="form-row">
                                    <button class="button circle yellow block" data-popup="kwitansi">Kwitansi Pembayaran</button>
                                </div>
                                <div class="form-row">
                                    <button class="button circle green block" data-popup="history">History Pembayaran</button>
                                </div>
                                <div class="form-row">
                                    <a href="upload_bukti.php" class="button circle blue block"> Upload Bukti Pembayaran</a>
                                </div>

                        </div>
                    <!-- TAB CONTROL END -->

                    <?php 
                        include("footer/footer.php");
                    ?>

			</main>
			<!-- Page content end -->
		</div>
    </div>
    
    <!--POPUP KWITANSI HTML CONTENT START -->
    <div class="popup-overlay" id="kwitansi">
        <div class="popup-container">
            <div class="popup-header">
                <h3 class="popup-title">Info Kwitansi Pembayaran</h3>
                <span class="popup-close" data-dismiss="true"><i class="fa fa-times"></i></span>
            </div>
            <!-- <form action="kwitansi.php" method="get"> -->
            <?php
                    $tahun   = $_GET['period'];
                    $siswaNIS   = $_SESSION["student_nis"];
                    $qsiswa    = "SELECT `student`.`student_full_name`, `student_nis`, `student_born_place`, `student_address`, `student_born_date`, `student_name_of_mother`, `student_name_of_father`, `student_parent_phone`, 
                    `majors_name`, `majors_id`, `majors_short_name`, `madin_id`, `madin_name`, `class_name`, `class_id`
                    FROM `student`
                    LEFT JOIN `class` ON `class`.class_id = `student`.class_class_id
                    LEFT JOIN `majors` ON `student`.majors_majors_id = `majors`.majors_id
                    LEFT JOIN `madin` ON `madin`.`madin_id` = `student`.`student_madin`
                    WHERE student_id = '$siswaID' AND student_nis = $siswaNIS
                    ORDER BY `student_id` ASC";

                    $siswa = mysqli_query($mysqli, $qsiswa);

                    while($row = mysqli_fetch_array($siswa)) {
                        $like     = 'SP' . $row['majors_short_name'] . $row['student_nis'];
                    }

                    $kwitansi = "SELECT kas_id, kas_noref, kas_input_date, kas_debit 
                    FROM `kas` 
                    WHERE kas_noref LIKE '$like%' AND kas_period = '$tahun' 
                    ORDER BY kas_id DESC";

            ?>
            <div class="popup-content">
            
                <div class="form-divider"></div>
                <div class="form-label-divider"><span>Kwitansi Pembayaran</span></div>
                <div class="form-divider"></div>
                <input type="hidden" value="<?php echo $tahun ?>" name="period">
                <?php
                    $total=mysqli_query($mysqli,$kwitansi);
                    while($row = mysqli_fetch_array($total)){
                ?>
                
                    <div class="form-divider"></div>
                    <label>No.Ref</label>
                        <div class="form-row-group with-icons">
                            <div class="form-row no-padding">
                                <i class="fa fa-credit-card"></i>
                                <input type="text" id = "noref" name = "noref" class="form-element" placeholder="noref" value="<?php echo $row['kas_noref']; ?>" readonly>
                            </div>
                        </div>
                        <div class="table-responsive">
                            <table class="table">
                                <thead class="thead-dark">
                                    <tr>
                                        <th>Tanggal</th>
                                        <th>Total Transaksi</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                    <td>
                                        <?php echo date ("d/F/Y", strtotime ($row['kas_input_date'])); ?>
                                    </td>
                                    <td>
                                        <?php echo 'Rp ' . number_format($row['kas_debit'], 0, ',', '.') ?>
                                    </td>
                                    <td align="center">
                                        <a href="<?php echo 'https://' . $foldersekolah . '.epesantren.co.id/payout/cetak?n=' . base64_encode($tahun) . '&r=' . base64_encode($siswaNIS) . '&d=' . base64_encode($row['kas_input_date']) . '&f=' . base64_encode($row['kas_noref']) ?>" class="button red" target="_blank">Download</a>
                                    </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                <?php
                } 
                ?>
                <!-- </form> -->
                
                <div class="popup-footer">
                    <button class="button" data-dismiss="true">Cancel</button>
                </div>
            </div>
        </div>
    </div>
    <!--POPUP KWITANSI HTML CONTENT END -->

    <!--POPUP HTML CONTENT START -->
	<div class="popup-overlay" id="history">
        <div class="popup-container">
			<div class="popup-header">
				<h3 class="popup-title">History Pembayaran</h3>
				<span class="popup-close" data-dismiss="true"><i class="fa fa-times"></i></span>
			</div>
			<div class="popup-content">

            <?php
            
            $siswaID = $_SESSION["student_id"];
            $history = "SELECT `log_trx`.`log_trx_id`, `log_trx_input_date`, `log_trx_last_update`,
            `bulan_bulan_id`, `log_trx`.`student_student_id`, `student_nis`, `bulan_bill`, 
            `bulan_account_id`, `bulan_noref`, `month_name`, `bebas_pay_bill`, 
            `bebas_pay_account_id`, `bebas_pay_noref`, `accountMonth`.`account_description` AS accMonth, 
            `accountBebas`.`account_description` AS accBebas, 
            `posMonth`.`pos_name` AS `posmonth_name`, 
            `posBebas`.`pos_name` AS `posbebas_name`, 
            `periodMonth`.`period_start` AS `period_start_month`, 
            `periodMonth`.`period_end` AS `period_end_month`, 
            `periodBebas`.`period_start` AS `period_start_bebas`, 
            `periodBebas`.`period_end` AS `period_end_bebas` 
            FROM `log_trx` 
            LEFT JOIN `bulan` ON `bulan`.`bulan_id` = `log_trx`.`bulan_bulan_id` 
            LEFT JOIN `month` ON `month`.`month_id` = `bulan`.`month_month_id` 
            LEFT JOIN `bebas_pay` ON `bebas_pay`.`bebas_pay_id` = `log_trx`.`bebas_pay_bebas_pay_id` 
            LEFT JOIN `bebas` ON `bebas`.`bebas_id` = `bebas_pay`.`bebas_bebas_id` 
            LEFT JOIN `student` ON `student`.`student_id` = `log_trx`.`student_student_id` 
            LEFT JOIN `payment` AS `payMonth` ON `payMonth`.`payment_id` = `bulan`.`payment_payment_id` 
            LEFT JOIN `payment` AS `payBebas` ON `payBebas`.`payment_id` = `bebas`.`payment_payment_id` 
            LEFT JOIN `pos` AS `posMonth` ON `posMonth`.`pos_id` = `payMonth`.`pos_pos_id` 
            LEFT JOIN `pos` AS `posBebas` ON `posBebas`.`pos_id` = `payBebas`.`pos_pos_id` 
            LEFT JOIN `period` AS `periodMonth` ON `periodMonth`.`period_id` = `payMonth`.`period_period_id` 
            LEFT JOIN `period` AS `periodBebas` ON `periodBebas`.`period_id` = `payBebas`.`period_period_id` 
            LEFT JOIN `account` AS `accountMonth` ON `accountMonth`.`account_id` = `bulan`.`bulan_account_id` 
            LEFT JOIN `account` AS `accountBebas` ON `accountBebas`.`account_id` = `bebas_pay`.`bebas_pay_account_id` 
            LEFT JOIN `kas` AS `kasMonth` ON `kasMonth`.`kas_noref` = `bulan`.`bulan_noref` 
            LEFT JOIN `kas` AS `kasBebas` ON `kasBebas`.`kas_noref` = `bebas_pay`.`bebas_pay_noref` 
            WHERE (`log_trx`.`student_student_id` = '$siswaID' AND `bulan_noref` != '' AND `bulan_noref` != '') 
            OR (`bebas_pay_noref` != '' AND `log_trx`.`student_student_id` = '$siswaID') 
            ORDER BY `log_trx_id` DESC";
            ?>

                <div class="form-divider"></div>
                <div class="form-label-divider"><span>History Pembayaran</span></div>
                <div class="form-divider"></div>
                <?php
                    $total=mysqli_query($mysqli,$history);
                    while($row = mysqli_fetch_array($total)){
                ?>
                <div class="form-divider"></div>
                <div class="form-row-group with-icons">
                    <div class="form-row no-padding">
                        <i class="fa fa-credit-card"></i>
                        <input type="text" name="noref" class="form-element" placeholder="nis" value="No.Ref : <?php echo ($row['bulan_bulan_id']!= NULL) ? $row['bulan_noref'] :  $row['bebas_pay_noref'] ?>" readonly>
                    </div>
                </div>
                <!-- <label>Pembayaran</label> -->
                <div class="form-row-group with-icons">
                    <div class="form-row no-padding">
                        <i class="fa fa-money"></i>
                        <input type="text" name="noref" class="form-element" placeholder="nis" value=" Pembayaran : <?php echo ($row['bulan_bulan_id']!= NULL) ? $row['posmonth_name'].' - T.A '.$row['period_start_month'].'/'.$row['period_end_month'].' ('.$row['month_name'].')' : $row['posbebas_name'].' - T.A '.$row['period_start_bebas'].'/'.$row['period_end_bebas'] ?>" >
                    </div>
                </div>
                <div class="table-responsive">
                    <table class="table">
                        <thead class="thead-dark">
                            <th>Tanggal</th>
                            <th>Nominal</th>
                            <th>Bayar Via</th>
                        </thead>
                        
                        <tr>
                            <td><?php echo date ("d/F/Y", strtotime ($row['log_trx_input_date'])); ?></td>
                            <td><?php echo ($row['bulan_bulan_id']!= NULL) ? 'Rp. '. number_format($row['bulan_bill'], 0, ',', '.') : 'Rp. '. number_format($row['bebas_pay_bill'], 0, ',', '.') ?></td>
                            <td><?php echo ($row['bulan_bulan_id']!= NULL) ? $row['accMonth'] : $row['accBebas'] ?></td>
                        </tr>
                    </table>
                </div>
                <?php
                } 
                ?>
                 
			</div>

                <div class="popup-footer">
                    <button class="button" data-dismiss="true">Cancel</button>
                </div>
		    </div>
            
	    </div>
    </div>
	<!--POPUP HTML CONTENT END -->


		 <!--Page loader DOM Elements. Requared all pages-->
         <div class="sweet-loader">
		<div class="box">
		  	<div class="circle1"></div>
		  	<div class="circle2"></div>
		  	<div class="circle3"></div>
		</div>
	</div>

	<!-- JQuery library file. requared all pages -->
	<script src="js/jquery-3.2.1.min.js"></script>
    <script src="jquery-ui/jquery-ui.js" type="text/javascript"></script>
    <script src="jquery-ui/jquery-ui.min.js" type="text/javascript"></script>

	<!-- Template global script file. requared all pages -->
    <script src="js/global.script.js"></script>

    <!-- Turbo slider plugin file. requared only wizard pages -->
    <script src="plugins/turbo-slider/turbo.min.js"></script>
    <script src="js/turbo-ini.js"></script>

    <!-- Load file library jQuery -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

    <!-- Load file library Popper JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>

    <!-- Load file library JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>

    <script>
    $(function() {
        $(document).ready(function() {
        $('#example').DataTable();
        });
    });
    </script>

    <!-- <script type="text/javascript">
        $(function(){
            $("#nis").autocomplete({
                source:'autocompletefilter.php',
                minLength:3,
                select:function(event,data){
                    $('input[name=nama]').val(data.item.student_full_name);
                    $('input[name=sekolahaddress]').val(data.item.alamat_sekolah);
                    $('input[name=alamat]').val(data.item.alamat);
                }
            });
        });
    </script> -->
    
    <script type="text/javascript">
    $(document).ready(function() {
        $('[data-toggle="toggle"]').change(function(){
            $(this).parents().next('.hide').toggle();
        });
    });
    </script>

	
</body>

</html>

