<?php

    session_start();
    if(!isset($_SESSION["student_id"])){
    	header("Location:login.php");
    }
    include_once("config/config.php");
    
?>

<?php

    $student_id=$_SESSION["student_id"];
    $student_nis=$_SESSION["student_nis"];
    $student_full_name=$_SESSION["student_full_name"];
    $class_name=$_SESSION["class_name"];
    $majors_name=$_SESSION["majors_name"];
    $foldersekolah=$_SESSION["folder"];
    $namasekolah=$_SESSION["nama_sekolah"];
?>

<?php                   
    $period = mysqli_query($mysqli, "SELECT * FROM `period` WHERE period_status = 1");
    while ($data = mysqli_fetch_array($period)) {
        $periodActive= $data['period_id'];
    }
?>

<?php
if(isset($_POST["add_to_cart"]))
    {
    if(isset($_SESSION["shopping_cart"]))
    {
    $item_array_id = array_column($_SESSION["shopping_cart"], "item_id");
    if(!in_array($_GET["id"], $item_array_id))
    {
    $count = count($_SESSION["shopping_cart"]);
    $item_array = array(
    'item_id' => $_GET["id"],
    'item_name' => $_POST["hidden_name"],
    'item_price' => $_POST["hidden_price"],
    'item_quantity' => $_POST["quantity"]
    );
    $_SESSION["shopping_cart"][$count] = $item_array;
    echo '<script>window.location="bayar2.php?period='.$periodActive.' "</script>';
    }
    else
    {
    echo '<script>alert("Item Already Added")</script>';
    }
    }
    else
    {
    $item_array = array(
    'item_id' => $_GET["payment_id"],
    'item_name' => $_POST["hidden_name"],
    'item_price' => $_POST["hidden_price"],
    'item_quantity' => $_POST["quantity"]
    );
    $_SESSION["shopping_cart"][0] = $item_array;
    }
}
?>

<?php
    if(isset($_GET["action"]))
    {
        if($_GET["action"] == "delete")
        {
        foreach($_SESSION["shopping_cart"] as $keys => $values)
        {
        if($values["item_id"] == $_GET["id"])
        {
        unset($_SESSION["shopping_cart"][$keys]);
        // echo '<script>alert("Item Removed")</script>';
        // echo '<div class="alert alert-danger">
        //         <strong>Maaf!</strong> Data anda gagal diperbarui. Silahkan masukkan kembali.
        //     </div>';
        echo '<script>window.location="bayar2.php?period='.$periodActive.' "</script>';
        }
        }
        }
    }
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=0">
    <title>Mobile Template</title>

	<!-- Google font file. If you want you can change. -->
	<link href="https://fonts.googleapis.com/css?family=Nunito:400,600,700,900" rel="stylesheet">

	<!-- Fontawesome font file css -->
	<link rel="stylesheet" type="text/css" href="css/font-awesome.min.css">

	<!-- Template global css file. Requared all pages -->
	<link rel="stylesheet" type="text/css" href="css/global.style.css">
	
</head>

<body>
	
	<div class="wrapper">
        <?php 
			include("menu/nav-menu.php");
		?>
		<div class="wrapper-inline">
			<!-- Header area start -->
			<header> <!-- extra class no-background -->
				<a class="go-back-link" href="javascript:history.back();"><i class="fa fa-arrow-left"></i></a>
				<h1 class="page-title">List Pembayaran(<?php echo $student_full_name ?>)</h1>
				<div class="navi-menu-button">
					<em></em>
					<em></em>
					<em></em>
				</div>
			</header>
			<!-- Header area end -->
			<!-- Page content start -->
			<main>
                
                <div class="container">
                    <form action="<?php echo $_SERVER["PHP_SELF"];?>" method="get">
                        <div class="form-row no-padding">
                            <label for="sel1">Tahun Ajaran:</label>
                            <select class="form-element" id = "period" name="period">
                            <?php                   
                                    $sql="select * from period";
                                    $hasil=mysqli_query($mysqli,$sql);
                                    $no=0;
                                    while ($data = mysqli_fetch_array($hasil)) {

                                    $ket="";
                                        if (isset($_GET['period'])) {
                                        $period = trim($_GET['period']);

                                        if ($period==$data['period_id'])
                                        {
                                        $ket="selected";
                                        }
                                    }
                            ?>
                            <option <?php echo $ket; ?> value="<?php echo $data['period_id'];?>"><?php echo $data['period_start'];?>/<?php echo $data['period_end'];?></option>
                            <?php
                                }
                            ?>
                                
                            </select>
                                <div class="form-divider"></div>
                                <div class="col-sm-4">
                                    <button type="submit" class="button circle green"><i class="fa fa-search"> </i> Cari Pembayaran</button>
                                </div>
                        </div>
                    </form>
                </div>

                <?php if (isset($_GET['period'])) {
                $tahun      = $_GET['period'];
                $siswaID    = $_SESSION["student_id"];
                ?>

                <?php 
                }  
                ?>

                <div class="container">

                    <input name="nis" id="nis" value="<?php echo $siswaID; ?>" hidden />
                    <div class="form-divider"></div>
                    <h4 class="title-main">Pembayaran Bulanan</h4>
                    <div class="form-divider"></div>
                    <?php
                        $i =1;  
                        $sasen = "SELECT  `s`.`pos_name`, `d`.`period_id`, `d`.`period_start`, `d`.`period_end`, 
                                `p`.`payment_id`, SUM(`b`.`bulan_bill`) as `total`,
                                SUM(if(`b`.`bulan_status`='1',`b`.`bulan_bill`,0)) as `dibayar`
                                FROM `bulan` as `b` 
                                JOIN `payment` as `p` ON `b`.`payment_payment_id` = `p`.`payment_id` 
                                JOIN `pos` as `s` ON `s`.`pos_id` = `p`.`pos_pos_id` 
                                LEFT JOIN `account` as `a` ON `a`.`account_id` = `b`.`bulan_account_id` 
                                JOIN `period` as `d` ON `d`.`period_id` = `p`.`period_period_id`
                                JOIN `student` as `t` ON `t`.`student_id` = `b`.student_student_id
                                JOIN `month` as `m` ON `m`.`month_id` = `b`.month_month_id
                                WHERE `d`.`period_id` = '$tahun' AND student_student_id = '$siswaID'
                                GROUP BY `p`.`payment_id`";

                        $tes=mysqli_query($mysqli, $sasen);
                        while($row = mysqli_fetch_array($tes)){
                                $namePay = $row['pos_name'].' - T.A '.$row['period_start'].'/'.$row['period_end'];
                                $payid = $row['payment_id'];
                                $jml  = $row['total'];
                                $dbyr = $row['dibayar'];

                                $bulanan = "SELECT `t`.`student_id`, `t`.`student_nis`, `t`.`student_full_name`, `s`.`pos_name`, 
                                            `d`.`period_id`, `d`.`period_start`, `d`.`period_end`, `p`.`payment_id`, `p`.`payment_mode`, 
                                            SUM(`b`.`bulan_bill`) as `total`,
                                            SUM(if(`b`.`bulan_status`='1',`b`.`bulan_bill`,0)) as `dibayar`,
                                            SUM(if(`b`.`month_month_id`='1',`b`.`bulan_bill`,0)) as `bill_jul`, 
                                            SUM(if(`b`.`month_month_id`='2',`b`.`bulan_bill`,0)) as `bill_agu`, 
                                            SUM(if(`b`.`month_month_id`='3',`b`.`bulan_bill`,0)) as `bill_sep`, 
                                            SUM(if(`b`.`month_month_id`='4',`b`.`bulan_bill`,0)) as `bill_okt`, 
                                            SUM(if(`b`.`month_month_id`='5',`b`.`bulan_bill`,0)) as `bill_nov`, 
                                            SUM(if(`b`.`month_month_id`='6',`b`.`bulan_bill`,0)) as `bill_des`, 
                                            SUM(if(`b`.`month_month_id`='7',`b`.`bulan_bill`,0)) as `bill_jan`, 
                                            SUM(if(`b`.`month_month_id`='8',`b`.`bulan_bill`,0)) as `bill_feb`, 
                                            SUM(if(`b`.`month_month_id`='9',`b`.`bulan_bill`,0)) as `bill_mar`, 
                                            SUM(if(`b`.`month_month_id`='10',`b`.`bulan_bill`,0)) as `bill_apr`, 
                                            SUM(if(`b`.`month_month_id`='11',`b`.`bulan_bill`,0)) as `bill_mei`, 
                                            SUM(if(`b`.`month_month_id`='12',`b`.`bulan_bill`,0)) as `bill_jun`, 
                                            SUM(if(`b`.`month_month_id`='1',`b`.`bulan_status`,0)) as `status_jul`, 
                                            SUM(if(`b`.`month_month_id`='2',`b`.`bulan_status`,0)) as `status_agu`, 
                                            SUM(if(`b`.`month_month_id`='3',`b`.`bulan_status`,0)) as `status_sep`, 
                                            SUM(if(`b`.`month_month_id`='4',`b`.`bulan_status`,0)) as `status_okt`, 
                                            SUM(if(`b`.`month_month_id`='5',`b`.`bulan_status`,0)) as `status_nov`, 
                                            SUM(if(`b`.`month_month_id`='6',`b`.`bulan_status`,0)) as `status_des`, 
                                            SUM(if(`b`.`month_month_id`='7',`b`.`bulan_status`,0)) as `status_jan`, 
                                            SUM(if(`b`.`month_month_id`='8',`b`.`bulan_status`,0)) as `status_feb`, 
                                            SUM(if(`b`.`month_month_id`='9',`b`.`bulan_status`,0)) as `status_mar`, 
                                            SUM(if(`b`.`month_month_id`='10',`b`.`bulan_status`,0)) as `status_apr`, 
                                            SUM(if(`b`.`month_month_id`='11',`b`.`bulan_status`,0)) as `status_mei`, 
                                            SUM(if(`b`.`month_month_id`='12',`b`.`bulan_status`,0)) as `status_jun`,
                                            MAX(if(`b`.`month_month_id`='1',`b`.`bulan_date_pay`,0)) as `date_pay_jul`, 
                                            MAX(if(`b`.`month_month_id`='2',`b`.`bulan_date_pay`,0)) as `date_pay_agu`, 
                                            MAX(if(`b`.`month_month_id`='3',`b`.`bulan_date_pay`,0)) as `date_pay_sep`, 
                                            MAX(if(`b`.`month_month_id`='4',`b`.`bulan_date_pay`,0)) as `date_pay_okt`, 
                                            MAX(if(`b`.`month_month_id`='5',`b`.`bulan_date_pay`,0)) as `date_pay_nov`, 
                                            MAX(if(`b`.`month_month_id`='6',`b`.`bulan_date_pay`,0)) as `date_pay_des`, 
                                            MAX(if(`b`.`month_month_id`='7',`b`.`bulan_date_pay`,0)) as `date_pay_jan`, 
                                            MAX(if(`b`.`month_month_id`='8',`b`.`bulan_date_pay`,0)) as `date_pay_feb`, 
                                            MAX(if(`b`.`month_month_id`='9',`b`.`bulan_date_pay`,0)) as `date_pay_mar`, 
                                            MAX(if(`b`.`month_month_id`='10',`b`.`bulan_date_pay`,0)) as `date_pay_apr`, 
                                            MAX(if(`b`.`month_month_id`='11',`b`.`bulan_date_pay`,0)) as `date_pay_mei`, 
                                            MAX(if(`b`.`month_month_id`='12',`b`.`bulan_date_pay`,0)) as `date_pay_jun`, 
                                            MAX(if(`b`.`month_month_id`='1',`m`.`month_name`,'')) as `month_name_jul`, 
                                            MAX(if(`b`.`month_month_id`='2',`m`.`month_name`,'')) as `month_name_agu`, 
                                            MAX(if(`b`.`month_month_id`='3',`m`.`month_name`,'')) as `month_name_sep`, 
                                            MAX(if(`b`.`month_month_id`='4',`m`.`month_name`,'')) as `month_name_okt`, 
                                            MAX(if(`b`.`month_month_id`='5',`m`.`month_name`,'')) as `month_name_nov`, 
                                            MAX(if(`b`.`month_month_id`='6',`m`.`month_name`,'')) as `month_name_des`, 
                                            MAX(if(`b`.`month_month_id`='7',`m`.`month_name`,'')) as `month_name_jan`, 
                                            MAX(if(`b`.`month_month_id`='8',`m`.`month_name`,'')) as `month_name_feb`, 
                                            MAX(if(`b`.`month_month_id`='9',`m`.`month_name`,'')) as `month_name_mar`, 
                                            MAX(if(`b`.`month_month_id`='10',`m`.`month_name`,'')) as `month_name_apr`, 
                                            MAX(if(`b`.`month_month_id`='11',`m`.`month_name`,'')) as `month_name_mei`, 
                                            MAX(if(`b`.`month_month_id`='12',`m`.`month_name`,'')) as `month_name_jun`,
                                            MAX(if(`b`.`month_month_id`='1',`a`.`account_description`,'')) as `account_jul`, 
                                            MAX(if(`b`.`month_month_id`='2',`a`.`account_description`,'')) as `account_agu`, 
                                            MAX(if(`b`.`month_month_id`='3',`a`.`account_description`,'')) as `account_sep`, 
                                            MAX(if(`b`.`month_month_id`='4',`a`.`account_description`,'')) as `account_okt`, 
                                            MAX(if(`b`.`month_month_id`='5',`a`.`account_description`,'')) as `account_nov`, 
                                            MAX(if(`b`.`month_month_id`='6',`a`.`account_description`,'')) as `account_des`, 
                                            MAX(if(`b`.`month_month_id`='7',`a`.`account_description`,'')) as `account_jan`, 
                                            MAX(if(`b`.`month_month_id`='8',`a`.`account_description`,'')) as `account_feb`, 
                                            MAX(if(`b`.`month_month_id`='9',`a`.`account_description`,'')) as `account_mar`, 
                                            MAX(if(`b`.`month_month_id`='10',`a`.`account_description`,'')) as `account_apr`, 
                                            MAX(if(`b`.`month_month_id`='11',`a`.`account_description`,'')) as `account_mei`, 
                                            MAX(if(`b`.`month_month_id`='12',`a`.`account_description`,'')) as `account_jun`, 
                                            SUM(if(`b`.`month_month_id`='1',`b`.`bulan_id`,0)) as `month_id_jul`,
                                            SUM(if(`b`.`month_month_id`='2',`b`.`bulan_id`,0)) as `month_id_agu`,
                                            SUM(if(`b`.`month_month_id`='3',`b`.`bulan_id`,0)) as `month_id_sep`,
                                            SUM(if(`b`.`month_month_id`='4',`b`.`bulan_id`,0)) as `month_id_okt`,
                                            SUM(if(`b`.`month_month_id`='5',`b`.`bulan_id`,0)) as `month_id_nov`,
                                            SUM(if(`b`.`month_month_id`='6',`b`.`bulan_id`,0)) as `month_id_des`,
                                            SUM(if(`b`.`month_month_id`='7',`b`.`bulan_id`,0)) as `month_id_jan`,
                                            SUM(if(`b`.`month_month_id`='8',`b`.`bulan_id`,0)) as `month_id_feb`,
                                            SUM(if(`b`.`month_month_id`='9',`b`.`bulan_id`,0)) as `month_id_mar`,
                                            SUM(if(`b`.`month_month_id`='10',`b`.`bulan_id`,0)) as `month_id_apr`,
                                            SUM(if(`b`.`month_month_id`='11',`b`.`bulan_id`,0)) as `month_id_mei`,
                                            SUM(if(`b`.`month_month_id`='12',`b`.`bulan_id`,0)) as `month_id_jun`
                                            FROM `bulan` as `b` 
                                            JOIN `payment` as `p` ON `b`.`payment_payment_id` = `p`.`payment_id` 
                                            JOIN `pos` as `s` ON `s`.`pos_id` = `p`.`pos_pos_id` 
                                            LEFT JOIN `account` as `a` ON `a`.`account_id` = `b`.`bulan_account_id` 
                                            JOIN `period` as `d` ON `d`.`period_id` = `p`.`period_period_id`
                                            JOIN `student` as `t` ON `t`.`student_id` = `b`.student_student_id
                                            JOIN `month` as `m` ON `m`.`month_id` = `b`.month_month_id
                                            WHERE  `d`.`period_id` = '$tahun' AND student_student_id = '$siswaID' AND `p`.`payment_id` = '$payid'
                                            GROUP BY `p`.`payment_id`";
                    ?>

                    <div class="container">
                        <div class="expandable-item accordion" data-group="accordion1">
                            <div class="expandable-header">
                                <i class="list-icon fa fa-book"></i>
                                <h3 class="list-title"><?php echo $namePay ?></h3>
                                <i class="list-arrow fa fa-angle-down"></i>
                            </div>
                            <div class="expandable-content">
                                <div class="padding-content">
                                    <div class="table-responsive">
                                        <table class="table">
                                            <tbody>
                                                <?php
                                                $i =1;
                                                include_once("config/config.php");
                                                    $hasil=mysqli_query($mysqli,$bulanan);
                                                    // while($row = mysqli_fetch_array($hasil)){
                                                    if(mysqli_num_rows($hasil) > 0)
                                                    {
                                                    while($row = mysqli_fetch_array($hasil))
                                                    {
                                                    $detail = $row['payment_id']
                                                    // $bulanID = $row['bulan_id']
                                                ?>
                                                        <tbody id="demo<?php echo $detail ?>" class="collapse">
                                                            <div class="form-divider"></div>
                                                                <thead class="thead-dark">    
                                                                    <tr>
                                                                        <th>No.</th>
                                                                        <th>Pembayaran</th>
                                                                        <th>Aksi</th>
                                                                    </tr>
                                                                </thead>
                                                                <form method="post" action="bayar2.php?period=<?php echo $row["period_id"]; ?>&action=add&id=<?php echo $row["month_id_jul"]; ?>">
                                                                    <tr class="<?php echo ($row['status_jul'] == 1) ? 'success' : 'danger' ?>">
                                                                        <td align="center">
                                                                            1
                                                                        </td>
                                                                        <td align="center"> <?php echo $namePay ?> <?php echo $row['month_name_jul']?> Rp: <?php echo number_format($row['bill_jul'], 0, ',', '.') ?></td>
                                                                        <input type="hidden" name="hidden_name" value="<?php echo $namePay ?> - <?php echo $row['month_name_jul']?>" />
                                                                        <input type="hidden" name="hidden_price" value="<?php echo number_format($row['bill_jul'], 0, ',', '.') ?>" />
                                                                        <?php 
                                                                            if ($row['status_jul']==1) { echo '<td align="center">
                                                                            Lunas
                                                                            </td>'; }
                                                                            else if ($row['status_jul']==0) { echo '<td align="center">
                                                                                <input type="submit" name="add_to_cart" style="margin-top:5px;" class="button blue" value="Bayar" />
                                                                            </td>'; }
                                                                        ?>
                                                                    </tr>
                                                                </form>
                                                                <form method="post" action="bayar2.php?period=<?php echo $row["period_id"]; ?>&action=add&id=<?php echo $row["month_id_agu"]; ?>">
                                                                    <tr class="<?php echo ($row['status_agu'] == 1) ? 'success' : 'danger' ?>">
                                                                        <td align="center">
                                                                            2
                                                                        </td>
                                                                        <td align="center"> <?php echo $namePay ?> <?php echo $row['month_name_agu']?> Rp: <?php echo number_format($row['bill_agu'], 0, ',', '.') ?></td>
                                                                        <input type="hidden" name="hidden_name" value="<?php echo $namePay ?> - <?php echo $row['month_name_agu']?>" />
                                                                        <input type="hidden" name="hidden_price" value="<?php echo number_format($row['bill_agu'], 0, ',', '.') ?>" />
                                                                        <?php 
                                                                            if ($row['status_agu']==1) { echo '<td align="center">
                                                                            Lunas
                                                                            </td>'; }
                                                                            else if ($row['status_agu']==0) { echo '<td align="center">
                                                                                <input type="submit" name="add_to_cart" style="margin-top:5px;" class="button blue" value="Bayar" />
                                                                            </td>'; }
                                                                        ?>
                                                                    </tr>
                                                                </form>
                                                                <form method="post" action="bayar2.php?period=<?php echo $row["period_id"]; ?>&action=add&id=<?php echo $row["month_id_sep"]; ?>">
                                                                    <tr class="<?php echo ($row['status_sep'] == 1) ? 'success' : 'danger' ?>">
                                                                        <td align="center">
                                                                            3
                                                                        </td>
                                                                        <td align="center"> <?php echo $namePay ?> <?php echo $row['month_name_sep']?> Rp: <?php echo number_format($row['bill_sep'], 0, ',', '.') ?></td>
                                                                        <input type="hidden" name="hidden_name" value="<?php echo $namePay ?> - <?php echo $row['month_name_sep']?>" />
                                                                        <input type="hidden" name="hidden_price" value="<?php echo number_format($row['bill_sep'], 0, ',', '.') ?>" />
                                                                        <?php 
                                                                            if ($row['status_sep']==1) { echo '<td align="center">
                                                                            Lunas
                                                                            </td>'; }
                                                                            else if ($row['status_sep']==0) { echo '<td align="center">
                                                                                <input type="submit" name="add_to_cart" style="margin-top:5px;" class="button blue" value="Bayar" />
                                                                            </td>'; }
                                                                        ?>
                                                                    </tr>
                                                                </form>
                                                                <form method="post" action="bayar2.php?period=<?php echo $row["period_id"]; ?>&action=add&id=<?php echo $row["month_id_okt"]; ?>">
                                                                    <tr class="<?php echo ($row['status_okt'] == 1) ? 'success' : 'danger' ?>">
                                                                        <td align="center">
                                                                            4
                                                                        </td>
                                                                        <td align="center"> <?php echo $namePay ?> <?php echo $row['month_name_okt']?> Rp: <?php echo number_format($row['bill_okt'], 0, ',', '.') ?></td>
                                                                        <input type="hidden" name="hidden_name" value="<?php echo $namePay ?> - <?php echo $row['month_name_okt']?>" />
                                                                        <input type="hidden" name="hidden_price" value="<?php echo number_format($row['bill_okt'], 0, ',', '.') ?>" />
                                                                        <?php 
                                                                            if ($row['status_okt']==1) { echo '<td align="center">
                                                                            Lunas
                                                                            </td>'; }
                                                                            else if ($row['status_okt']==0) { echo '<td align="center">
                                                                                <input type="submit" name="add_to_cart" style="margin-top:5px;" class="button blue" value="Bayar" />
                                                                            </td>'; }
                                                                        ?>
                                                                    </tr>
                                                                </form>
                                                                <form method="post" action="bayar2.php?period=<?php echo $row["period_id"]; ?>&action=add&id=<?php echo $row["month_id_nov"]; ?>">
                                                                    <tr class="<?php echo ($row['status_nov'] == 1) ? 'success' : 'danger' ?>">
                                                                        <td align="center">
                                                                            5
                                                                        </td>
                                                                        <td align="center"> <?php echo $namePay ?> <?php echo $row['month_name_nov']?> Rp: <?php echo number_format($row['bill_nov'], 0, ',', '.') ?></td>
                                                                        <input type="hidden" name="hidden_name" value="<?php echo $namePay ?> - <?php echo $row['month_name_nov']?>" />
                                                                        <input type="hidden" name="hidden_price" value="<?php echo number_format($row['bill_nov'], 0, ',', '.') ?>" />
                                                                        <?php 
                                                                            if ($row['status_nov']==1) { echo '<td align="center">
                                                                            Lunas
                                                                            </td>'; }
                                                                            else if ($row['status_nov']==0) { echo '<td align="center">
                                                                                <input type="submit" name="add_to_cart" style="margin-top:5px;" class="button blue" value="Bayar" />
                                                                            </td>'; }
                                                                        ?>
                                                                    </tr>
                                                                </form>
                                                                <form method="post" action="bayar2.php?period=<?php echo $row["period_id"]; ?>&action=add&id=<?php echo $row["month_id_des"]; ?>">
                                                                    <tr class="<?php echo ($row['status_des'] == 1) ? 'success' : 'danger' ?>">
                                                                        <td align="center">
                                                                            6
                                                                        </td>
                                                                        <td align="center"> <?php echo $namePay ?> <?php echo $row['month_name_des']?> Rp: <?php echo number_format($row['bill_des'], 0, ',', '.') ?></td>
                                                                        <input type="hidden" name="hidden_name" value="<?php echo $namePay ?> - <?php echo $row['month_name_des']?>" />
                                                                        <input type="hidden" name="hidden_price" value="<?php echo number_format($row['bill_des'], 0, ',', '.') ?>" />
                                                                        <?php 
                                                                            if ($row['status_des']==1) { echo '<td align="center">
                                                                            Lunas
                                                                            </td>'; }
                                                                            else if ($row['status_des']==0) { echo '<td align="center">
                                                                                <input type="submit" name="add_to_cart" style="margin-top:5px;" class="button blue" value="Bayar" />
                                                                            </td>'; }
                                                                        ?>
                                                                    </tr>
                                                                </form>
                                                                <form method="post" action="bayar2.php?period=<?php echo $row["period_id"]; ?>&action=add&id=<?php echo $row["month_id_jan"]; ?>">
                                                                    <tr class="<?php echo ($row['status_jan'] == 1) ? 'success' : 'danger' ?>">
                                                                        <td align="center">
                                                                            7
                                                                        </td>
                                                                        <td align="center"> <?php echo $namePay ?> <?php echo $row['month_name_jan']?> Rp: <?php echo number_format($row['bill_jan'], 0, ',', '.') ?></td>
                                                                        <input type="hidden" name="hidden_name" value="<?php echo $namePay ?> - <?php echo $row['month_name_jan']?>" />
                                                                        <input type="hidden" name="hidden_price" value="<?php echo number_format($row['bill_jan'], 0, ',', '.') ?>" />
                                                                        <?php 
                                                                            if ($row['status_jan']==1) { echo '<td align="center">
                                                                            Lunas
                                                                            </td>'; }
                                                                            else if ($row['status_jan']==0) { echo '<td align="center">
                                                                                <input type="submit" name="add_to_cart" style="margin-top:5px;" class="button blue" value="Bayar" />
                                                                            </td>'; }
                                                                        ?>
                                                                    </tr>
                                                                </form>
                                                                <form method="post" action="bayar2.php?period=<?php echo $row["period_id"]; ?>&action=add&id=<?php echo $row["month_id_feb"]; ?>">
                                                                    <tr class="<?php echo ($row['status_feb'] == 1) ? 'success' : 'danger' ?>">
                                                                        <td align="center">
                                                                            8
                                                                        </td>
                                                                        <td align="center"> <?php echo $namePay ?> <?php echo $row['month_name_feb']?> Rp: <?php echo number_format($row['bill_feb'], 0, ',', '.') ?></td>
                                                                        <input type="hidden" name="hidden_name" value="<?php echo $namePay ?> - <?php echo $row['month_name_feb']?>" />
                                                                        <input type="hidden" name="hidden_price" value="<?php echo number_format($row['bill_feb'], 0, ',', '.') ?>" />
                                                                        <?php 
                                                                            if ($row['status_feb']==1) { echo '<td align="center">
                                                                            Lunas
                                                                            </td>'; }
                                                                            else if ($row['status_feb']==0) { echo '<td align="center">
                                                                                <input type="submit" name="add_to_cart" style="margin-top:5px;" class="button blue" value="Bayar" />
                                                                            </td>'; }
                                                                        ?>
                                                                    </tr>
                                                                </form>
                                                                <form method="post" action="bayar2.php?period=<?php echo $row["period_id"]; ?>&action=add&id=<?php echo $row["month_id_mar"]; ?>">
                                                                    <tr class="<?php echo ($row['status_mar'] == 1) ? 'success' : 'danger' ?>">
                                                                        <td align="center">
                                                                            9
                                                                        </td>                                                
                                                                        <td align="center"> <?php echo $namePay ?> <?php echo $row['month_name_mar']?> Rp: <?php echo number_format($row['bill_mar'], 0, ',', '.') ?></td>
                                                                        <input type="hidden" name="hidden_name" value="<?php echo $namePay ?> - <?php echo $row['month_name_mar']?>" />
                                                                        <input type="hidden" name="hidden_price" value="<?php echo number_format($row['bill_mar'], 0, ',', '.') ?>" />
                                                                        <?php 
                                                                            if ($row['status_mar']==1) { echo '<td align="center">
                                                                            Lunas
                                                                            </td>'; }
                                                                            else if ($row['status_mar']==0) { echo '<td align="center">
                                                                                <input type="submit" name="add_to_cart" style="margin-top:5px;" class="button blue" value="Bayar" />
                                                                            </td>'; }
                                                                        ?>
                                                                    </tr>
                                                                </form>
                                                                <form method="post" action="bayar2.php?period=<?php echo $row["period_id"]; ?>&action=add&id=<?php echo $row["month_id_apr"]; ?>">
                                                                    <tr class="<?php echo ($row['status_apr'] == 1) ? 'success' : 'danger' ?>">
                                                                        <td align="center">
                                                                            10
                                                                        </td>
                                                                        <td align="center"> <?php echo $namePay ?> <?php echo $row['month_name_apr']?> Rp: <?php echo number_format($row['bill_apr'], 0, ',', '.') ?></td>
                                                                        <input type="hidden" name="hidden_name" value="<?php echo $namePay ?> - <?php echo $row['month_name_apr']?>" />
                                                                        <input type="hidden" name="hidden_price" value="<?php echo number_format($row['bill_apr'], 0, ',', '.') ?>" />
                                                                        <?php 
                                                                            if ($row['status_apr']==1) { echo '<td align="center">
                                                                            Lunas
                                                                            </td>'; }
                                                                            else if ($row['status_apr']==0) { echo '<td align="center">
                                                                                <input type="submit" name="add_to_cart" style="margin-top:5px;" class="button blue" value="Bayar" />
                                                                            </td>'; }
                                                                        ?>
                                                                    </tr>
                                                                </form>
                                                                <form method="post" action="bayar2.php?period=<?php echo $row["period_id"]; ?>&action=add&id=<?php echo $row["month_id_mei"]; ?>">
                                                                    <tr class="<?php echo ($row['status_mei'] == 1) ? 'success' : 'danger' ?>">
                                                                        <td align="center">
                                                                            11
                                                                        </td>
                                                                        <td align="center"> <?php echo $namePay ?> <?php echo $row['month_name_mei']?> Rp: <?php echo number_format($row['bill_mei'], 0, ',', '.') ?></td>
                                                                        <input type="hidden" name="hidden_name" value="<?php echo $namePay ?> - <?php echo $row['month_name_mei']?>" />
                                                                        <input type="hidden" name="hidden_price" value="<?php echo number_format($row['bill_mei'], 0, ',', '.') ?>" />
                                                                        <?php 
                                                                            if ($row['status_mei']==1) { echo '<td align="center">
                                                                            Lunas
                                                                            </td>'; }
                                                                            else if ($row['status_mei']==0) { echo '<td align="center">
                                                                                <input type="submit" name="add_to_cart" style="margin-top:5px;" class="button blue" value="Bayar" />
                                                                            </td>'; }
                                                                        ?>
                                                                    </tr>
                                                                </form>
                                                                <form method="post" action="bayar2.php?period=<?php echo $row["period_id"]; ?>&action=add&id=<?php echo $row["month_id_jun"]; ?>">
                                                                    <tr class="<?php echo ($row['status_jun'] == 1) ? 'success' : 'danger' ?>">
                                                                        <td align="center">
                                                                            12
                                                                        </td>
                                                                        <td align="center"> <?php echo $namePay ?> <?php echo $row['month_name_jun']?> Rp: <?php echo number_format($row['bill_jun'], 0, ',', '.') ?></td>
                                                                        <input type="hidden" name="hidden_name" value="<?php echo $namePay ?> - <?php echo $row['month_name_jun']?>" />
                                                                        <input type="hidden" name="hidden_price" value="<?php echo number_format($row['bill_jun'], 0, ',', '.') ?>" />
                                                                        <?php 
                                                                            if ($row['status_jun']==1) { echo '<td align="center">
                                                                            Lunas
                                                                            </td>'; }
                                                                            else if ($row['status_jun']==0) { echo '<td align="center">
                                                                                <input type="submit" name="add_to_cart" style="margin-top:5px;" class="button blue" value="Bayar" />
                                                                            </td>'; }
                                                                        ?>
                                                                    </tr>
                                                                </form>
                                                        </tbody>
                                            </tbody>
                                        </table>
                                        <?php  
                                        }
                                        }
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <?php
                    }
                    ?>

                    <div class="form-divider"></div>
                    <h4 class="title-main">Pembayaran Bebas</h4>
                    <div class="form-divider"></div>

                    <div class="table-responsive">
                        <table class="table">
                            <thead class="thead-dark">
                                    <tr>
                                        <th>No.</th>
                                        <th>Jenis Pembayaran</th>
                                        <th>Dibayar</th>
                                        <th>Kekurangan</th>
                                        <th>Bayar</th>
                                        <th>Aksi</th>
                                    </tr>
                            </thead>

                            <?php 
                                $bebas = "SELECT `bebas`.`bebas_id`, `bebas_bill`, `bebas_diskon`, `bebas_total_pay`, `bebas_input_date`, 
                                `bebas_last_update`, `student_student_id`, `student`.`class_class_id`, `class_name`, `student_full_name`, 
                                `student_nis`, `student_name_of_mother`, `student_parent_phone`, `student`.`majors_majors_id`, `majors_name`, 
                                `majors_short_name`, `payment_payment_id`, `pos_name`, `payment_type`, `period_period_id`, `period_start`, `period_end`, 
                                `madin_id`, `madin_name` 
                                FROM `bebas` 
                                LEFT JOIN `student` ON `student`.`student_id` = `bebas`.`student_student_id` 
                                LEFT JOIN `payment` ON `payment`.`payment_id` = `bebas`.`payment_payment_id` 
                                LEFT JOIN `pos` ON `pos`.`pos_id` = `payment`.`pos_pos_id` 
                                LEFT JOIN `class` ON `class`.`class_id` = `student`.`class_class_id` 
                                LEFT JOIN `period` ON `period`.`period_id` = `payment`.`period_period_id` 
                                LEFT JOIN `majors` ON `majors`.`majors_id` = `student`.`majors_majors_id` 
                                LEFT JOIN `madin` ON `madin`.`madin_id` = `student`.`student_madin` 
                                WHERE `bebas`.`student_student_id` = $siswaID AND `payment`.`period_period_id` = $tahun AND `majors_status` AND student_student_id = '$siswaID'
                                ORDER BY `payment_payment_id` ASC";

                                $cicil=mysqli_query($mysqli,$bebas);
                                $i =1;
                                while($row = mysqli_fetch_array($cicil)) {
                                $sisa = $row['bebas_bill']-$row['bebas_total_pay'];
                                $namePay = $row['pos_name'].' - T.A '.$row['period_start'].'/'.$row['period_end'];
                            ?>

                            <tbody>
                                <form method="post" action="bayar2.php?period=<?php echo $row["period_id"]; ?>&action=add&id=<?php echo $row["bebas_id"]; ?>">
                                    <tr style="color:<?php echo ($row['bebas_bill'] == $row['bebas_total_pay']) ? '#00E640' : 'red' ?>">
                                        <td><?php echo $i ?></td>
                                        <td><?php echo $namePay ?> Total : <?php echo 'Rp. ' . number_format($row['bebas_bill'], 0, ',', '.') ?></td>
                                        <td><?php echo 'Rp. ' . number_format($row['bebas_total_pay'], 0, ',', '.') ?></td>
                                        <td><?php echo 'Rp. ' . number_format($sisa, 0, ',', '.') ?></td>
                                        <input type="hidden" name="hidden_name" value="<?php echo $namePay ?>" />
                                        <?php
                                            if ($row['bebas_bill']==$row['bebas_total_pay']) { echo '<td align="center">
                                            Lunas
                                            </td>'; }
                                            else if ($row['bebas_bill'] > $row['bebas_total_pay']) { echo '<td align="center">
                                                <input type="text" name="hidden_price" value="1" class="form-control" />
                                            </td>'; }
                                        ?>
                                        <?php 
                                            if ($row['bebas_bill']==$row['bebas_total_pay']) { echo '<td align="center">
                                            Lunas
                                            </td>'; }
                                            else if ($row['bebas_bill'] > $row['bebas_total_pay']) { echo '<td align="center">
                                                <input type="submit" name="add_to_cart" style="margin-top:5px;" class="button blue" value="Bayar" />
                                            </td>'; }
                                        ?>
                                    </tr>
                                </form>
                                <?php
                                $i++;
                                }
                                ?>  
                            </tbody>

                        </table>
                    </div>
                </div>
                <div class="form-divider"></div>
                    <h4 class="title-main">Detail Pembayaran</h4>
                <div class="form-divider"></div>
                <div class="table-responsive">
                    <table class="table table-bordered">
                            <tr class="thead-dark">
                                <th width="40%">Nama Pembayaran</th>
                                <th width="20%">Biaya</th>
                                <th width="15%">Dibayar</th>
                                <th width="5%">Aksi</th>
                            </tr>
                            <?php
                                if(!empty($_SESSION["shopping_cart"]))
                                {
                                $total = 0;
                                foreach($_SESSION["shopping_cart"] as $keys => $values)
                                {
                            ?>
                            <tr>
                                <td><?php echo $values["item_name"]; ?></td>
                                <td>Rp. <?php echo $values["item_price"]; ?></td>
                                <td>Rp. <?php echo $values["item_price"];?></td>
                                <td><a href="bayar2.php?period=<?php echo $tahun; ?>&action=delete&id=<?php echo $values["item_id"]; ?>"><span class="button red">Hapus</span></a></td>
                            </tr>
                            <?php
                                $total = $total + ($values["item_price"]);
                                }
                            ?>
                        <tr>
                            <td colspan="3" align="right">Total</td>
                            <td align="right">Rp. <?php echo number_format($total, 3); ?></td>
                        </tr>
                        <?php
                        }
                        ?>
                    
                    </table>
                </div>

                <div class="container">
                    <button class="button circle green block" data-popup="standartPopup">Metode Pembayaran</button>
                </div>

                <div class="clear"></div>

                <?php 
                    include("footer/footer.php");
                ?>

			</main>
			<!-- Page content end -->
		</div>
	</div>

    <div class="popup-overlay" id="standartPopup">
        <?php
                                
            $like = 'SP' . str_replace(" ", "", $majors_short . $siswaID);

            $query = "SELECT MAX(RIGHT(kas_noref,2)) AS no_max FROM kas WHERE DATE(kas_input_date)=CURDATE() AND kas_majors_id = '$id_majors' AND kas_noref LIKE '$like%' AND kas_category = '1'";
            
            $tes=mysqli_query($mysqli, $query);
            while($row = mysqli_fetch_array($tes)){
            
            if (count($query)>0){
                $tmp = ((int)$query->no_max)+1;
                $number = sprintf("%02s", $tmp);
            } else {
                $number = "01";
            }

            $noref = $like . $student_nis.date('dmy') . $number;
                
        ?>

        <?php
            $pesantren = mysqli_query($mysqli, "SELECT * 
            FROM `sekolahs` 
            ORDER BY id ASC");

            while($row = mysqli_fetch_array($pesantren)){
            $kode = $row['kode_sekolah'];
        ?>
        <!-- <form method="POST" action="insert_temp.php"> -->
        <form action="insert_temp.php" method="post" enctype="multipart/form-data">
            <div class="popup-container">
                <div class="form-row-group with-icons">
                    <div class="form-row no-padding">
                        <i class="fa fa-user"></i>
                        <input type="text" name="noref" id="noref" class="form-element" value="<?php echo $noref ?>" readonly>
                    </div>
                    <div class="form-row no-padding">
                        <i class="fa fa-user"></i>
                        <input type="text" name="nama" id="nama" class="form-element" value="<?php echo $student_full_name?>" readonly>
                    </div>
                    <div class="form-row no-padding">
                        <i class="fa fa-envelope"></i>
                        <input type="text" name="kelas" id="kelas" class="form-element" value="<?php echo $class_name?>" readonly>
                    </div>
                    <div class="form-row no-padding">
                        <i class="fa fa-lock"></i>
                        <input type="text" name="major" id="major" class="form-element" value="<?php echo $majors_short?>" readonly>
                    </div>
                </div>
                <input type="hidden" name="pondok" id="pondok" class="form-element" value="<?php echo $kode ?>" readonly>
                <!-- <input type="hidden" name="studentid" id="studentid" class="form-element" value="<?php echo $siswaID ?>"  readonly> -->
                <input type="hidden" name="studentnis" id="studentnis" class="form-element" value="<?php echo $student_nis ?>"  readonly>
                <div class="form-row-group with-icons">
                    <div class="form-row no-padding">
                        <i class="fa fa-language"></i>
                        <select class="form-element">
                            <option value="" selected="">Bayar Via</option>
                            <option name="bayarvia" id="bayarvia" value="1">Virtual Account BNI</option>
                            <option name="bayarvia" id="bayarvia" value="2">Virtual Account Bank Mandiri</option>
                            <option name="bayarvia" id="bayarvia" value="3">Virtual Account CIMB Niaga</option>
                            <option name="bayarvia" id="bayarvia" value="4">Virtual Account Arta Graha</option>
                            <option name="bayarvia" id="bayarvia" value="5">Retail Alfamart/Alfamidi</option>
                        </select>
                    </div>
                </div>
                 <div class="table-responsive">
                    <table class="table table-bordered">
                            <tr class="thead-dark">
                                <th width="40%">Nama Pembayaran</th>
                                <th width="20%">Biaya</th>
                                <th width="15%">Dibayar</th>
                                <th width="5%">Total Bayar</th>
                                <!-- <th width="5%">ID</th> -->
                            </tr>
                            <?php
                                if(!empty($_SESSION["shopping_cart"]))
                                {
                                $total = 0;
                                foreach($_SESSION["shopping_cart"] as $keys => $values)
                                {
                            ?>
                            <tr>
                                <td><?php echo $values["item_name"]; ?></td>
                                <td>Rp. <?php echo $values["item_price"]; ?></td>
                                <td>Rp. <?php echo $values["item_price"];?></td>
                                <!-- <td><a href="bayar2.php?period=<?php echo $tahun; ?>&action=delete&id=<?php echo $values["item_id"]; ?>"><span class="button red">Hapus</span></a></td> -->
                                <td><input type="hidden" name="item_id" value="<?php echo $values["item_id"]; ?>"></td>
                            </tr>
                            <?php
                                $total = $total + ($values["item_price"]);
                                }
                            ?>
                            <tr>  
                               <td colspan="3" align="right">Total</td>  
                               <td id="total" name="total" align="right">Rp. <?php echo number_format($total, 3); ?></td>
                            </tr>
                            <input type="hidden" id="total" name="total" value="<?php echo number_format($total, 3); ?>">
                        <?php
                        }
                        ?>
                    
                    </table>
                </div>
                <!-- <div id="fbatch"></div> -->
                    <div class="popup-footer">
                        <button type="submit" class="button orange">Bayar</button>
                        <!-- <a href="contact.php" class="button orange">Bayar</a> -->
                        <button type="button" class="button" data-dismiss="true">Batal</button>
                    </div>
            </div>
        </form>
        <?php
        }
        }
        ?>
	</div>
    

	<!--Page loader DOM Elements. Requared all pages-->
	<div class="sweet-loader">
		<div class="box">
		  	<div class="circle1"></div>
		  	<div class="circle2"></div>
		  	<div class="circle3"></div>
		</div>
	</div>

	<!-- JQuery library file. requared all pages -->
	<script src="js/jquery-3.2.1.min.js"></script>

	<!-- Template global script file. requared all pages -->
	<script src="js/global.script.js"></script>

    <script>

        function get_form(){
            var bulan_id = $('#bulan:checked');
            var bebas_id = $('#bebas:checked');
            if(bulan_id.length || bebas_id.length > 0)
            {
                var bulan_id_value = [];
                $(bulan_id).each(function(){
                    bulan_id_value.push($(this).val());
                });

                var bebas_id_value = [];
                $(bebas_id).each(function(){
                    bebas_id_value.push($(this).val());
                });

                $.ajax({
                    url: 'get_form.php',
                    method:"POST",
                    data: {
                            bulan_id : bulan_id_value,
                            bebas_id : bebas_id_value,
                    },
                    success: function(msg){
                            $("#fbatch").html(msg);
                    },
                    error: function(msg){
                            alert('msg');
                    }
                });
            }
            else
            {
                alert("Belum ada tagihan yang dipilih");
            }
        }
    
    </script>

    <script language="JavaScript">
        function checkChoice(whichbox){
        with (whichbox.form){
        if (whichbox.checked == false)
        hiddentotal.value = eval(hiddentotal.value) - eval(whichbox.value);
        else
        hiddentotal.value = eval(hiddentotal.value) + eval(whichbox.value);
        return(formatCurrency(hiddentotal.value));
        }
        }
        function formatCurrency(num){
        num = num.toString().replace(/\$|\,/g,'');
        if(isNaN(num)) num = "0";
        cents = Math.floor((num*100+0.5)%100);
        num = Math.floor((num*100+0.5)/100).toString();
        if(cents < 10) cents = "0" + cents;
        for (var i = 0; i < Math.floor((num.length-(1+i))/3); i++)
        num = num.substring(0,num.length-(4*i+3))+'.'+num.substring(num.length-(4*i+3));
        return ("Rp. " + num + "," + cents);
        }
    </script>

</body>


</html>