<?php
    session_start();
    include_once("config/config.php");
    if(empty($_SESSION['student_nis'])){
    header('location:main.php');
    }
    if((time() - $_SESSION['auto_logout_time'])>900){
    header('location:logout1.php');
    }

    $result = mysqli_query($mysqli, "SELECT * FROM `information` ORDER BY information_input_date DESC");
?>

<?php

    $student_id=$_SESSION["student_id"];
    $student_nis=$_SESSION["student_nis"];
    $student_full_name=$_SESSION["student_full_name"];
    $class_name=$_SESSION["class_name"];
    $majors_name=$_SESSION["majors_name"];
    $foldersekolah=$_SESSION["folder"];
    $kodesekolah=$_SESSION["kode_sekolah"];

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=0">
    <title>Epesantren Mobile</title>

    <!-- Google font file. If you want you can change. -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:300,400,600,700,900" rel="stylesheet">

    <!-- Fontawesome font file css -->
    <link rel="stylesheet" type="text/css" href="css/font-awesome.min.css">

    <!-- Animate css file for css3 animations. for more : https://daneden.github.io/animate.css -->
    <!-- Only use animate action. If you dont use animation, you don't have to add.-->
    <link rel="stylesheet" type="text/css" href="css/animate.css">

    <!-- Template global css file. Requared all pages -->
    <link rel="stylesheet" type="text/css" href="css/global.style.css">

    <!-- Swiper slider css file -->
    <link rel="stylesheet" href="css/swiper.min.css">

    <!--turbo slider plugin css file -->
    <link rel="stylesheet" type="text/css" href="plugins/turbo-slider/turbo.css">

    <!-- JQuery library file. requared all pages -->
    <script src="js/jquery-3.2.1.min.js"></script>

</head>

<body>

    <div class="wrapper">
        <?php 
		    include("menu/nav-menu.php");
		?>
        <div class="wrapper-inline">
            <!-- Header area start -->
            <header>

                <h1 class="page-title">Home Page<span></h1>
                <div class="navi-menu-button">
                    <em></em>
                    <em></em>
                    <em></em>
                </div>

            </header>
            <!-- Header area end -->

            <?php                   
                $period = mysqli_query($mysqli, "SELECT * FROM `period` WHERE period_status = 1");
                while ($data = mysqli_fetch_array($period)) {
                    $periodActive= $data['period_id'];
                }
            ?>

            <!-- Page content start -->
            <main class="margin">

                <!-- <section class="container">
                    <div class="slideshow-container">
                    <div class="mySlides fade">
                        <div class="numbertext">1 / 3</div>
                        <img src="img/Logo-ePesantren.png" style="width:100%">
                    </div>

                    <div class="mySlides fade">
                        <div class="numbertext">2 / 3</div>
                        <img src="img/235.jpg" style="width:100%">
                    </div>

                    <div class="mySlides fade">
                        <div class="numbertext">3 / 3</div>
                        <img src="img/mosque.jpg" style="width:100%">
                    </div>

                    <a class="prev" onclick="plusSlides(-1)">&#10094;</a>
                    <a class="next" onclick="plusSlides(1)">&#10095;</a>
                    </div>
                    <br>

                    <div style="text-align:center">
                    <span class="dot" onclick="currentSlide(1)"></span>
                    <span class="dot" onclick="currentSlide(2)"></span>
                    <span class="dot" onclick="currentSlide(3)"></span>
                    </div>
                </section> -->
            
                <section class="container">
                    <div class="banner-div">
                        <div class="form-row w-text txt-center mt-30">
                            <h2 class="banner-head">Selamat Datang di Pondok Pesantren <?php echo $foldersekolah ?></h2>
                            <h2 class="banner-head"> Ananda <?php echo $student_full_name ?></h2>
                            <h2 class="banner-head"> Kelas : <?php echo $class_name ?></h2>
                        </div>
                    </div>
                </section>

                <div class="clear"></div>

                <section class="container">
                    <div class="section-head">
                        <h4 class="title-main">Menu Aplikasi</h4>
                    </div>
                    <div class="c-wrapper">
                        <div class="col-3 text-center">
                            <a href="tab-top-tahfidz.php?period=<?php echo $periodActive; ?>">
                                <div class="provide-item purpule-bg">
                                    <img src="img/icons/study-1.png" alt="">
                                    <h4 class="h-text">Laporan Tahfidz</h4>
                                </div>
                            </a>
                        </div>

                        <div class="col-3 center-item text-center">
                            <a href="tab-top-perbaikan.php?period=<?php echo $periodActive; ?>">
                                <div class="provide-item pink-bg">
                                    <img src="img/icons/payment-method-1.png" alt="">
                                    <h4 class="h-text">Pembayaran Santri</h4>
                                </div>
                            </a>
                        </div>

                        <div class="col-3 text-center">
                            <a href="bayar1.php?period=<?php echo $periodActive; ?>">
                                <div class="provide-item blue-bg">
                                    <img src="img/icons/bill.png" alt="">
                                    <h4 class="h-text">Bayar</h4>
                                </div>
                            </a>
                        </div>
                    </div>

                    <div class="form-divider"></div>

                    <div class="c-wrapper">
                        <div class="col-3 text-center">
                            <a href="tab-top-absensi.php?period=<?php echo $periodActive; ?>">
                                <div class="provide-item blue-bg">
                                    <img src="img/icons/checked-1.png" alt="">
                                    <h4 class="h-text">Absensi dan Izin</h4>
                                </div>
                            </a>
                        </div>

                        <div class="col-3 center-item text-center">
                            <a href="product-detail-1.php?period=<?php echo $periodActive; ?>">
                                <div class="provide-item orange-bg">
                                    <img src="img/icons/consultation-1.png" alt="">
                                    <h4 class="h-text">Konseling Santri</h4>
                                </div>
                            </a>
                        </div>

                        <div class="col-3 text-center">
                            <a href="tab-top-tabungan.php?period=<?php echo $periodActive; ?>">
                                <div class="provide-item grey-bg">
                                    <img src="img/icons/wallet-1.png" alt="">
                                    <h4 class="h-text">Tabungan Siswa</h4>
                                </div>
                            </a>
                        </div>
                    </div>

                    <div class="form-divider"></div>

                    <div class="c-wrapper">
                        <div class="col-3 text-center">
                            <a href="product-detail-2.php?period=<?php echo $periodActive; ?>">
                                <div class="provide-item dark-blue-bg">
                                    <img src="img/icons/heart-1.png" alt="">
                                    <h4 class="h-text">Rekam Medis</h4>
                                </div>
                            </a>
                        </div>

                        <div class="col-3 center-item text-center">
                            <a href="izin.php?period=<?php echo $periodActive; ?>">
                                <div class="provide-item red-bg">
                                    <img src="img/icons/exit(1).png" alt="">
                                    <h4 class="h-text">Izin Santri</h4>
                                </div>
                            </a>
                        </div>

                        <div class="col-3 text-center">
                            <a href="izin_pulang.php?period=<?php echo $periodActive; ?>">
                                <div class="provide-item yellow-bg">
                                    <img src="img/icons/home(1).png" alt="">
                                    <h4 class="h-text">Izin Pulang</h4>
                                </div>
                            </a>
                        </div>
                    </div>

                </section>
                
                <!--end tab-->

                <div class="clear"></div>

                <section class="container">

                    <div class="section-head">
                        <h4 class="title-main">INFORMASI</h4>
                        <a class="c-btn" href="post-list-2.php">Lebih Banyak</a>
                    </div>
                    
                    <?php  
                         while($user_data = mysqli_fetch_array($result)) {  
                    ?> 
					<div class="news-list-item">
                        <div class="list-image">
                        <?php 
							if (isset($user_data['information_img'])) {?>
							<img class="profil-img" alt="User Avatar" src="imageinfo/<?php echo $user_data['information_img']; ?>" width="100" height="100">
						<?php } else { ?>
							<img class="profil-img" alt="User Avatar" src="imageinfo/image18.png" width="100" height="100">
						<?php } ?>
						</div>
						<div class="list-content">
							<!-- <span class="list-category green">Travel</span> -->
							<h2 class="list-title"><a href="post-detail.php?i=<?php echo $user_data['information_id'] ?>"> <?php echo $user_data['information_title']; ?> </a></h2>
                            <span class="list-time"><?php echo date ("d-F-Y", strtotime ($user_data['information_input_date'])); ?></span>
						</div>
                        <?php
                        }
                        ?>
					</div>

					<div class="form-mini-divider"></div>

				</section>
                <?php 
                    include("footer/footer.php");
                ?>
            </main>
            <!-- Page content end -->
        </div>
    </div>

    <!--Page loader DOM Elements. Requared all pages-->
    <div class="sweet-loader">
        <div class="box">
            <div class="circle1"></div>
            <div class="circle2"></div>
            <div class="circle3"></div>
        </div>
    </div>

    <!-- Swiper JS -->
    <script src="js/swiper.min.js"></script>

    <!-- Initialize Swiper -->
    <script>
        
    </script>

    <!-- Template global script file. requared all pages -->
    <script src="js/global.script.js"></script>

    <!-- Turbo slider plugin file. requared only wizard pages -->
    <script src="plugins/turbo-slider/turbo.min.js"></script>
    <script src="js/turbo-ini.js"></script>
    <script>
        var slideIndex = 0;
        showSlides();
        function showSlides() {
        var i;
        var slides = document.getElementsByClassName("mySlides");
        for (i = 0; i < slides.length; i++) {
            slides[i].style.display = "none";
        }
        slideIndex++;
        if (slideIndex > slides.length) {slideIndex = 1}
        slides[slideIndex-1].style.display = "block";
        setTimeout(showSlides, 5000); // Change image every 2 seconds
        }
    </script>

    
   
</body>

</html>