<?php
    session_start();
    include_once("config/config.php");
    if(empty($_SESSION['student_nis'])){
    header('location:main.php');
    }
    if((time() - $_SESSION['auto_logout_time'])>900){
    header('location:logout1.php');
    }
?>

<?php
	$student_full_name=$_SESSION["student_full_name"];
	$class_name=$_SESSION["class_name"];
	$majors_name=$_SESSION["majors_name"];
	$student_id=$_SESSION["student_id"];
	$student_nis=$_SESSION["student_nis"];
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=0">
    <title>Absensi</title>

	<!-- Google font file. If you want you can change. -->
	<link href="https://fonts.googleapis.com/css?family=Nunito:400,600,700,900" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.min.css">

	<!-- Fontawesome font file css -->
	<link rel="stylesheet" type="text/css" href="css/font-awesome.min.css">

	<!-- Template global css file. Requared all pages -->
	<link rel="stylesheet" type="text/css" href="css/global.style.css">
</head>

<body>
	
<div class="wrapper">
        <?php 
		    include("menu/nav-menu.php");
		?>
		<div class="wrapper-inline">
			<!-- Header area start -->
			<header> <!-- extra class no-background -->
                <a class="go-back-link" href="main.php"><i class="fa fa-arrow-left"></i></a>
				<h1 class="page-title">Absensi Siswa</h1>
				<div class="navi-menu-button">
					<em></em>
					<em></em>
					<em></em>
				</div>
			</header>
			<!-- Header area end -->
			<!-- Page content start -->
			<main class="wizard-page">
				<div class="wizard">
					<div class="wizard-item"> 
						<div class="wizard-content">

							<div class="form-mini-divider"></div>

							<img src="img/icons/404.jpg" alt="" width="250">

							<div class="form-mini-divider"></div>

							<h3>Mohon Maaf! Fitur belum tersedia</h3>

							<p>Fitur Ini Sedang dalam tahap Development (Pengembangan)</p>

							<div class="form-divider"></div>

							<a href="main.php" class="button circle red">Kembali ke Dashboard</a>

						</div>
					</div>
				</div>
				<a class="wizard-skip-link" href="main.php">SKIP</a>		

			</main>
            <!-- Page content end -->
        </div>
        
</div>

	<!--Page loader DOM Elements. Requared all pages-->
	<div class="sweet-loader">
		<div class="box">
		  	<div class="circle1"></div>
		  	<div class="circle2"></div>
		  	<div class="circle3"></div>
		</div>
	</div>

	<!-- JQuery library file. requared all pages -->
	<script src="js/jquery-3.2.1.min.js"></script>

	<!-- Template global script file. requared all pages -->
	<script src="js/global.script.js"></script>

    <!-- JQuery DataTable -->
    <script src="js/jquery-datatable.js"></script>
    <script src="js/jquery-3.5.1.js"></script>
    <script src="js/jquery-datatable-bootstrap4.js"></script>

    <script>
        $(document).ready(function() {
        $('#example').DataTable();
    } );
    </script>
    <!-- JQuery TableAbsensi -->
    <!-- <script src="js/tabel-absensi.js"></script> -->


	
</body>

</html>