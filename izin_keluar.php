<?php
    session_start();
    include_once("config/config.php");
    if(empty($_SESSION['student_nis'])){
    header('location:main.php');
    }
    if((time() - $_SESSION['auto_logout_time'])>900){
    header('location:logout1.php');
    }
?>

<?php
    $student_full_name=$_SESSION["student_full_name"];
    $class_name=$_SESSION["class_name"];
    $majors_name=$_SESSION["majors_name"];
    $foldersekolah=$_SESSION["folder"];
?>

<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=0">
	<title>Izin Keluar Santri</title>

	<!-- Google font file. If you want you can change. -->
	<link href="https://fonts.googleapis.com/css?family=Nunito:400,600,700,900" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.min.css">

	<!-- Fontawesome font file css -->
    <link rel="stylesheet" type="text/css" href="css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="{{asset('/jquery-ui/jquery-ui.min.css')}}">

    <!--turbo slider plugin css file -->
    <link rel="stylesheet" type="text/css" href="plugins/turbo-slider/turbo.css">
    
	<!-- Template global css file. Requared all pages -->
    <link rel="stylesheet" type="text/css" href="css/global.style.css">
</head>

<body>
	
	<div class="wrapper">
        <?php 
		include("menu/nav-menu.php");
		?>
		<div class="wrapper-inline">
			<!-- Header area start -->
			<header> <!-- extra class no-background -->
                <a class="go-back-link" href="main.php"><i class="fa fa-arrow-left"></i></a>
				<h2 class="page-title">Izin Santri </h2>
				<div class="navi-menu-button">
					<em></em>
					<em></em>
					<em></em>
				</div>
			</header>
			<!-- Header area end -->

			<!-- Page content start -->
			<main>

                <section class="container">
                    <div class="section-head">
                        <h4 class="title-main">Izin Keluar Santri</h4>
                    </div>

                    <div class="form-mini-divider"></div>
                    <form action="<?php echo $_SERVER["PHP_SELF"];?>" method="get">
                            <div class="form-row no-padding">
                                <label for="sel1">Tahun Ajaran:</label>
                                <select class="form-element" name="period">
                                <?php
                                                               
                                        $sql="select * from period";

                                        $hasil=mysqli_query($mysqli,$sql);
                                        $no=0;
                                        while ($data = mysqli_fetch_array($hasil)) {
                                        $no++;

                                        $ket="";
                                            if (isset($_GET['period'])) {
                                            $period = trim($_GET['period']);

                                            if ($period==$data['period_id'])
                                            {
                                            $ket="selected";
                                            }
                                        }
                                ?>
                                <option <?php echo $ket; ?> value="<?php echo $data['period_id'];?>"><?php echo $data['period_start'];?>/<?php echo $data['period_end'];?></option>
                                <?php
                                    }
                                ?>
                                    
                                </select>
                                    <div class="form-divider"></div>
                                    <div class="col-sm-4">
                                        <button type="submit" class="button circle green"><i class="fa fa-search"> </i> Cari Data</button>
                                    </div>
                            </div>
                        </form>

                    <div class="container">
                        <?php if (isset($_GET['period'])) {
                            $tahun   = $_GET['period'];
                            $siswaID = $_SESSION["student_id"];

                            $keluar = "SELECT `izin`.`izin_date`, `izin`.`izin_time`, `student_full_name`, `student_nis`, `period_start`, `period_end`,`user_email`,`user_password`,`user_full_name`
                            FROM `izin` 
                            LEFT JOIN `period` ON `izin`.izin_period_id = `period`.period_id 
                            LEFT JOIN `student` ON `student`.student_id = `izin`.izin_student_id
                            LEFT JOIN `users` ON `users`.user_id = `izin`.izin_user_id
                            WHERE izin_student_id = '$siswaID' AND izin_period_id = '$tahun'
                            ORDER BY `izin_id` ASC";
                            
                        ?>
                        
                        <div class="form-divider"></div>
                        <div class="form-label-divider"><span>Info Izin Keluar Santri</span></div>
                        <div class="form-divider"></div>

                        <div class="table-responsive">          
                            <table id="example" class="table table-striped table-bordered">
                                <thead class="thead-dark">
                                    <tr>
                                        <th>No</th>
                                        <th>Tanggal</th>
                                        <th>Jam</th>
                                        <th>Keperluan</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                         $out = mysqli_query($mysqli, $keluar);

                                         $permission = mysqli_query($mysqli,"SELECT `izin`.`izin_date`, `izin`.`izin_time`, `izin`.`izin_keperluan`, `student_full_name`, `student_nis`, `period_start`, `period_end`,`user_email`,`user_password`,`user_full_name`
                                         FROM `izin` 
                                         LEFT JOIN `period` ON `izin`.izin_period_id = `period`.period_id 
                                         LEFT JOIN `student` ON `student`.student_id = `izin`.izin_student_id
                                         LEFT JOIN `users` ON `users`.user_id = `izin`.izin_user_id
                                         WHERE izin_student_id = '$siswaID' AND izin_period_id = '$tahun'
                                         ORDER BY `izin_id` DESC");
                                        $no = 1;
                                        while($row = $permission->fetch_assoc()){
                                        echo '
                                        <tr>
                                            <td>'.$no.'</td>
                                            <td>'.date ("d-F-Y", strtotime ($row['izin_date'])).'</td>
                                            <td>'.$row['izin_time'].'</td>
                                            <td>'.$row['izin_keperluan'].'</td>
                                        </tr>
                                        ';
                                        $no++;
                                    }
                                    ?>
                                </tbody>
                            </table>
                        </div>

                        <?php } ?>

                        <div class="form-divider"></div>
                        <div class="form-row">
                            <a href="<?php echo 'https://' . $foldersekolah . '.epesantren.co.id/manage/kesehatan/printBook/?n=' . base64_encode($tahun) . '&r=' . base64_encode($siswaNIS) ?>" class="button circle block blue">Cetak Riwayat Izin</a>
                        </div>
                    </div>
                </section>

                <footer>
                    <div class="container">
                        <ul>
                            <li><a href="#"><img src="img/icons/www32.png" alt=""></i></a></li>
                            <li><a href="#"><img src="img/icons/whatsapp32.png" alt=""></i></a></li>
                            <li><a href="#"><img src="img/icons/facebook32.png" alt=""></i></a></li>
                            <li><a href="#"><img src="img/icons/instagram32.png" alt=""></i></a></li>
                            <li><a href="#"><img src="img/icons/youtube32.png" alt=""></i></a></li>
                        </ul>
                        <p>Copyright © All Right Reserved</p>
                    </div>
                </footer>
            </main>
            <!-- Page content end -->
        </div>
    </div>

	<!--Page loader DOM Elements. Requared all pages-->
	<div class="sweet-loader">
		<div class="box">
		  	<div class="circle1"></div>
		  	<div class="circle2"></div>
		  	<div class="circle3"></div>
		</div>
	</div>

	<!-- JQuery library file. requared all pages -->
	<script src="js/jquery-3.2.1.min.js"></script>
    <script src="jquery-ui/jquery-ui.js" type="text/javascript"></script>
    <script src="jquery-ui/jquery-ui.min.js" type="text/javascript"></script>

	<!-- Template global script file. requared all pages -->
    <script src="js/global.script.js"></script>

    <!-- Turbo slider plugin file. requared only wizard pages -->
    <script src="plugins/turbo-slider/turbo.min.js"></script>
    <script src="js/turbo-ini.js"></script>

    <!-- JQuery DataTable -->
    <script src="js/jquery-datatable.js"></script>
    <script src="js/jquery-3.5.1.js"></script>
    <script src="js/jquery-datatable-bootstrap4.js"></script>

    <script>
        $(document).ready(function() {
        $('#example').DataTable();
    } );
    </script>

</body>

</html>