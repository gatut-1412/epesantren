<?php

include_once("config/config.php");

// Fetch all data from database
$result = mysqli_query($mysqli, "SELECT * FROM `sekolahs` ORDER BY id ASC");
?>
<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>Epesantren Mobile</title>

    <link rel="icon" type="image/png" href="images/icons/favicon.ico"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/bootstrap-contact/css/bootstrap.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="fonts/Linearicons-Free-v1.0.0/icon-font.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/animate-contact/animate.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/select2-contact/select2.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="css/util.css">
	<link rel="stylesheet" type="text/css" href="css/main.css">
<!--===============================================================================================-->
    
  
</head>

<body>
	<form action="login-pesantren.php" method="post">
        <div class="wrap-contact100">
			<form class="contact100-form validate-form">
				<span class="contact100-form-title">
					Get in Touch
				</span>

				<div class="wrap-input100 rs1-wrap-input100 validate-input" data-validate="Name is required">
					<span class="label-input100">Kode Pesantren</span>
					<input type="text" maxlength="10" class="input100" style="width:100%;" id="kodesearch" name="kodesearch" placeholder="Masukkan ID Pesantren" onkeyup="load_data()"><input type="text" maxlength="10" class="input" style="width:100%;" id="kodesearch" name="kodesearch" placeholder="Masukkan ID Pesantren" onkeyup="load_data()">
				</div>

				<div class="wrap-input100 rs1-wrap-input100 validate-input" data-validate = "Valid email is required: ex@abc.xyz">
					<span class="label-input100">Nama Pesantren</span>
                    <input type="text" class="input"  id="sekolah" name="sekolah" placeholder="Masukkan Nama Pesantren" readonly>
				</div>

				<div class="wrap-input100 validate-input" data-validate = "Message is required">
					<span class="label-input100">Message</span>
					<textarea id="sekolahaddress" name="sekolahaddress" placeholder="Masukkan Alamat Pesantren"></textarea>
				</div>

				<div class="container-contact100-form-btn">
					<div class="wrap-contact100-form-btn">
						<div class="contact100-form-bgbtn"></div>
						<button class="contact100-form-btn">
							Submit
						</button>
					</div>
				</div>
			</form>
		</div>
	</form>


	<!--Page loader DOM Elements. Requared all pages-->
	<div class="sweet-loader">
		<div class="box">
		  	<div class="circle1"></div>
		  	<div class="circle2"></div>
		  	<div class="circle3"></div>
		</div>
	</div>

	<!--===============================================================================================-->
	<script src="vendor/jquery-contact/jquery-3.2.1.min.js"></script>
    <!--===============================================================================================-->
        <script src="vendor/bootstrap-contact/js/popper.js"></script>
        <script src="vendor/bootstrap-contact/js/bootstrap.min.js"></script>
    <!--===============================================================================================-->
        <script src="vendor/select2-contact/select2.min.js"></script>
    <!--===============================================================================================-->
        <script src="js/maincontact.js"></script>

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-23581568-13"></script>
    <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-23581568-13');
    </script>

	<script>
		function load_data()
		{
			var kode = $('#kodesearch').val();
			// alert(kode);
			$.ajax({
				url:"ajaxlivesearch.php",
				method:"POST",
				data:{
					'query':kode
					},
				success:function(data)
				{
			$('#pesantren').html(data);
				}
			});
			
		}
		$(document).ready(function(){
		//load_data();
		});
	</script>

	<!-- <script>
		$(document).ready(function(){
			load_data();
			function load_data(keyword)
			{
				$.ajax({
					method:"POST",
					url:"data-pesantren.php",
					data: {keyword:keyword},
					success:function(hasil)
					{
						$('.result').html(hasil);
					}
				});
			}
			$('#kodesearch').keyup(function(){
				// var jurusan = $("#s_jurusan").val();
				var keyword = $("#kodesearch").val();
				load_data(keyword);
			});
			$('#s_jurusan').change(function(){
				// var jurusan = $("#s_jurusan").val();
				var keyword = $("#kodesearch").val();
				load_data(keyword);
			});
		});
	</script> -->

	<!-- <script>
		$(document).ready(function(){
		$("input").keydown(function(){
			$("input").css("background-color", "lightblue");
		});
		$("input").keyup(function(){
			$("input").css("background-color", "lavender");
		});
		$("#btn1").click(function(){
			$("input").keydown();
		});  
		$("#btn2").click(function(){
			$("input").keyup();
		});  
		});
	</script> -->

</body>

</html>