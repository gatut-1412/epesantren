<?php

include_once("config/config.php");

// Fetch all data from database
$result = mysqli_query($mysqli, "SELECT * FROM `student` ORDER BY student_id ASC");
?>
<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=0">
	<title>Forgot Password</title>

	<!-- Google font file. If you want you can change. -->
	<link href="https://fonts.googleapis.com/css?family=Nunito:400,600,700,900" rel="stylesheet">

	<!-- Fontawesome font file css -->
	<link rel="stylesheet" type="text/css" href="css/font-awesome.min.css">

	<!-- Template global css file. Requared all pages -->
	<link rel="stylesheet" type="text/css" href="css/global.style.css">
</head>

<body>
	
		<div class="login-wrap">
			<div class="login-html">
			<!-- <form action=""> -->
			<div class="text-center  mt-10">
				<img src="img/ePesantren-1.png" class="logo-img" alt="" style=width:70% >
			</div>
				<input id="tab-1" type="radio" name="tab" class="sign-in" checked><label for="tab-1" class="tab">Lupa Password?</label>
				<input id="tab-2" type="radio" name="tab" class="sign-up"><label for="tab-2" class="tab"></label>
				<!-- <form action="secure_wa_code.php" id="form" method="post" name="form"> -->
					<div class="login-form" action="proses-ajax.php">
						<div class="sign-in-htm">
							<div class="group">
								<label for="user" class="label">NIS Siswa</label>
								<input type="text" class="input" name="nis" id="nis" placeholder="Masukkan NIS">
							</div>
							<div class="group">
								<label for="pass" class="label">Nomor Handphone</label>
								<input type="text" class="input" onkeyup="isi_otomatis()" id="nomorhp" name="nomorhp" placeholder="Masukkan Nomor HP">
							</div>
							<div class="group">
								<label for="user" class="label">Nama Siswa</label>
								<input type="text" class="input" name="nama" id='nama' readonly>
							</div>
							<!-- <div class="group">
								<input id="check" type="checkbox" class="check">
								<label for="check"><span class="icon"></span> Tetap izinkan untuk masuk</label>
							</div> -->
							<div class="group">
								<input type="submit" class="button" value="Kirim Password">
							</div>
							<div class="hr"></div>
							<div class="foot-lnk">
								<a href="login.php">Kembali ke Login</a>
							</div>
						</div>
					</div>
				<!-- </form> -->
			</div>
		</div>
	


	<!--Page loader DOM Elements. Requared all pages-->
	<div class="sweet-loader">
		<div class="box">
		  	<div class="circle1"></div>
		  	<div class="circle2"></div>
		  	<div class="circle3"></div>
		</div>
	</div>

	<!-- JQuery library file. requared all pages -->
	<script src="js/jquery-3.2.1.min.js"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>

	<!-- Template global script file. requared all pages -->
	<script src="js/global.script.js"></script>

	<script type="text/javascript">
            function isi_otomatis(){
                var nomorhp = $("#nomorhp").val();
				var nis = $("#nis").val();
                $.ajax({
                    url: 'proses-ajax.php',
					type: 'POST',
                    data:{'nis' : nis, 'nomorhp' : nomorhp} ,
                }).success(function (data) {
                    var json = data,
                    obj = JSON.parse(json);
                    $('#nama').val(obj.nama);
                });
            }
    </script>

	
	
</body>

</html>