<?php

    session_start();
    if(!isset($_SESSION["student_id"])){
    	header("Location:login.php");
    }
    include_once("config/config.php");
    
?>

<?php

    $student_id=$_SESSION["student_id"];
    $student_nis=$_SESSION["student_nis"];
    $student_full_name=$_SESSION["student_full_name"];
    $class_name=$_SESSION["class_name"];
    $majors_name=$_SESSION["majors_name"];
    $foldersekolah=$_SESSION["folder"];
    $namasekolah=$_SESSION["nama_sekolah"];
?>

<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=0">
	<title>Mobile Template</title>

	<!-- Google font file. If you want you can change. -->
	<link href="https://fonts.googleapis.com/css?family=Nunito:400,600,700,900" rel="stylesheet">

	<!-- Fontawesome font file css -->
	<link rel="stylesheet" type="text/css" href="css/font-awesome.min.css">

	<!-- Template global css file. Requared all pages -->
	<link rel="stylesheet" type="text/css" href="css/global.style.css">
</head>

<body>
	
	<div class="wrapper">
		<?php 
			include("menu/nav-menu.php");
		?>
		<div class="wrapper-inline">
			<!-- Header area start -->
			<header> <!-- extra class no-background -->
				<!-- <a class="go-back-link" href="javascript:history.back();"><i class="fa fa-arrow-left"></i></a> -->
				<h1 class="page-title">Nomor Virtual Account</h1>
				<div class="navi-menu-button">
					<em></em>
					<em></em>
					<em></em>
				</div>
			</header>
			<!-- Header area end -->
			<!-- Page content start -->
			<main>
				<?php  { 

				$siswaID = $_SESSION["student_id"];

				// $siswa = "SELECT `student`.`student_full_name`, `student_nis`, `student_born_place`, `student_address`, 
				// `student_gender`, `student_img`, `student_born_date`, `student_name_of_mother`, `student_name_of_father`, 
				// `student_parent_phone`, `majors_name`, `majors_id`, `majors_short_name`, `madin_id`, `madin_name`, 
				// `class_name`, `class_id`, `data_pembayaran`.`bayar_via`, `nominal`, `nis`, `kode_pondok`, `noref` 
				// FROM `student` 
				// LEFT JOIN `class` ON `class`.class_id = `student`.class_class_id 
				// LEFT JOIN `majors` ON `student`.majors_majors_id = `majors`.majors_id 
				// LEFT JOIN `madin` ON `madin`.`madin_id` = `student`.`student_madin` 
				// LEFT JOIN `data_pembayaran` ON `data_pembayaran`.`nis` = `student`.`student_nis` 
				// WHERE student_id = '$siswaID' 
				// ORDER BY `student_id` ASC";

				$siswa = "SELECT `student`.`student_full_name`, `student_nis`, `student_born_place`, `student_address`, 
				`student_gender`, `student_img`, `student_born_date`, `student_name_of_mother`, `student_name_of_father`, 
				`student_parent_phone`, `majors_name`, `majors_id`, `majors_short_name`, `madin_id`, `madin_name`, 
				`class_name`, `class_id` 
				FROM `student` 
				LEFT JOIN `class` ON `class`.class_id = `student`.class_class_id 
				LEFT JOIN `majors` ON `student`.majors_majors_id = `majors`.majors_id 
				LEFT JOIN `madin` ON `madin`.`madin_id` = `student`.`student_madin`
				WHERE student_id = '$siswaID' 
				ORDER BY `student_id` ASC";
				?>
				<?php
					$mutasi = mysqli_query($mysqli, $siswa);
					while($row = mysqli_fetch_array($mutasi)) {
				?>

				<div class="contact-info">
					<h3 class="address-name">Terima Kasih sudah melakukan pembayaran. Berikut nomor Virtual Account anda:</h3>
					<p class="address">Nomor Virtual Account : 111112222233333</p>
				</div>

				<div class="contact-address">
				<p class="address">Detail Pembayaran :</p>
					<div class="contact-item">
						<i class="fa fa-map-marker"></i> Nama Siswa :<?php echo $row['student_full_name'];?>
					</div>

					<div class="contact-item">
						<i class="fa fa-clock-o"></i> Kelas : <?php echo $row['majors_name'];?>
					</div>

					<div class="contact-item">
						<i class="fa fa-phone"></i> Unit : <?php echo $row['madin_name'];?>
					</div>

					<div class="contact-item">
						<i class="fa fa-globe"></i> Bayar Via : ......
					</div>

					<div class="contact-item">
						<i class="fa fa-envelope"></i> Total Pembayaran : ......
					</div>
				</div>

				<div class="contact-short-info">
					<h3 class="title">Tata Cara Pembayaran</h3>
					<p>Assalamualaikum Warahmatullahi Wabarakatuh</p>
					<p>Yth wali santri atas nama <?php echo $row['student_full_name']?>, terima kasih telah menggunakan Aplikasi ADM ePesantren Mobile. </p>
				</div>
				<?php
				}
				}
				?>

				<div class="form-row">
						<a href="main.php" class="button circle block red">Kembali ke Dashboard</a>
					</div>
				<?php 
                    include("footer/footer.php");
                ?>
			</main>
			<!-- Page content end -->
		</div>
	</div>


	<!--Page loader DOM Elements. Requared all pages-->
	<div class="sweet-loader">
		<div class="box">
		  	<div class="circle1"></div>
		  	<div class="circle2"></div>
		  	<div class="circle3"></div>
		</div>
	</div>

	<!-- JQuery library file. requared all pages -->
	<script src="js/jquery-3.2.1.min.js"></script>

	<!-- Template global script file. requared all pages -->
	<script src="js/global.script.js"></script>
	
</body>

</html>