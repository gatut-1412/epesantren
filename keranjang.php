<?php
    session_start();
    include_once("config/config.php");
    if(empty($_SESSION['student_nis'])){
    header('location:main.php');
    }
    if((time() - $_SESSION['auto_logout_time'])>900){
    header('location:logout1.php');
    }
?>

<?php
    $student_full_name=$_SESSION["student_full_name"];
    $class_name=$_SESSION["class_name"];
    $majors_name=$_SESSION["majors_name"];
    $student_id=$_SESSION["student_id"];
    $student_nis=$_SESSION["student_nis"];
    $foldersekolah=$_SESSION["folder"];
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=0">
    <title>Mobile Template</title>

	<!-- Google font file. If you want you can change. -->
	<link href="https://fonts.googleapis.com/css?family=Nunito:400,600,700,900" rel="stylesheet">

	<!-- Fontawesome font file css -->
	<link rel="stylesheet" type="text/css" href="css/font-awesome.min.css">

	<!-- Template global css file. Requared all pages -->
	<link rel="stylesheet" type="text/css" href="css/global.style.css">

    <!-- Bootstrap CSS -->
    <!-- <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"> -->
</head>

<body>
	
	<div class="wrapper">
        <?php 
			include("menu/nav-menu.php");
		?>
		<div class="wrapper-inline">
			<!-- Header area start -->
			<header> <!-- extra class no-background -->
				<a class="go-back-link" href="javascript:history.back();"><i class="fa fa-arrow-left"></i></a>
				<h1 class="page-title">Keranjang</h1>
				<div class="navi-menu-button">
					<em></em>
					<em></em>
					<em></em>
				</div>
			</header>
			<!-- Header area end -->
			<!-- Page content start -->
			<main>
                <div class="container">
                    <form action="<?php echo $_SERVER["PHP_SELF"];?>" method="get">
                        <div class="form-row no-padding">
                            <label for="sel1">Tahun Ajaran:</label>
                            <select class="form-element" id = "period" name="period">
                            <?php                   
                                    $sql="select * from period";
                                    $hasil=mysqli_query($mysqli,$sql);
                                    $no=0;
                                    while ($data = mysqli_fetch_array($hasil)) {

                                    $ket="";
                                        if (isset($_GET['period'])) {
                                        $period = trim($_GET['period']);

                                        if ($period==$data['period_id'])
                                        {
                                        $ket="selected";
                                        }
                                    }
                            ?>
                            <option <?php echo $ket; ?> value="<?php echo $data['period_id'];?>"><?php echo $data['period_start'];?>/<?php echo $data['period_end'];?></option>
                            <?php
                                }
                            ?>
                                
                            </select>
                                <div class="form-divider"></div>
                                <div class="col-sm-4">
                                    <button type="submit" class="button circle green"><i class="fa fa-search"> </i> Cari Pembayaran</button>
                                </div>
                        </div>
                    </form>
                </div>

                <?php if (isset($_GET['period'])) {
                $tahun      = $_GET['period'];
                $siswaID    = $_SESSION["student_id"];

                ?>
                <?php 
                }  
                ?>

                <?php
                    $i =1;  
                    $sasen = "SELECT  `s`.`pos_name`, `d`.`period_id`, `d`.`period_start`, `d`.`period_end`, 
                            `p`.`payment_id`, SUM(`b`.`bulan_bill`) as `total`,
                            SUM(if(`b`.`bulan_status`='1',`b`.`bulan_bill`,0)) as `dibayar`
                            FROM `bulan` as `b` 
                            JOIN `payment` as `p` ON `b`.`payment_payment_id` = `p`.`payment_id` 
                            JOIN `pos` as `s` ON `s`.`pos_id` = `p`.`pos_pos_id` 
                            LEFT JOIN `account` as `a` ON `a`.`account_id` = `b`.`bulan_account_id` 
                            JOIN `period` as `d` ON `d`.`period_id` = `p`.`period_period_id`
                            JOIN `student` as `t` ON `t`.`student_id` = `b`.student_student_id
                            JOIN `month` as `m` ON `m`.`month_id` = `b`.month_month_id
                            WHERE `d`.`period_id` = '$tahun' AND student_student_id = '$siswaID'
                            GROUP BY `p`.`payment_id`";

                    $tes=mysqli_query($mysqli, $sasen);
                    while($row = mysqli_fetch_array($tes)){
                            $namePay = $row['pos_name'].' - T.A '.$row['period_start'].'/'.$row['period_end'];
                            $payid = $row['payment_id'];
                            $jml  = $row['total'];
                            $dbyr = $row['dibayar'];

                            $bulanan = "SELECT `t`.`student_id`, `t`.`student_nis`, `t`.`student_full_name`, `s`.`pos_name`, 
                                        `d`.`period_id`, `d`.`period_start`, `d`.`period_end`, `p`.`payment_id`, `p`.`payment_mode`, 
                                        SUM(`b`.`bulan_bill`) as `total`,
                                        SUM(if(`b`.`bulan_status`='1',`b`.`bulan_bill`,0)) as `dibayar`,
                                        SUM(if(`b`.`month_month_id`='1',`b`.`bulan_bill`,0)) as `bill_jul`, 
                                        SUM(if(`b`.`month_month_id`='2',`b`.`bulan_bill`,0)) as `bill_agu`, 
                                        SUM(if(`b`.`month_month_id`='3',`b`.`bulan_bill`,0)) as `bill_sep`, 
                                        SUM(if(`b`.`month_month_id`='4',`b`.`bulan_bill`,0)) as `bill_okt`, 
                                        SUM(if(`b`.`month_month_id`='5',`b`.`bulan_bill`,0)) as `bill_nov`, 
                                        SUM(if(`b`.`month_month_id`='6',`b`.`bulan_bill`,0)) as `bill_des`, 
                                        SUM(if(`b`.`month_month_id`='7',`b`.`bulan_bill`,0)) as `bill_jan`, 
                                        SUM(if(`b`.`month_month_id`='8',`b`.`bulan_bill`,0)) as `bill_feb`, 
                                        SUM(if(`b`.`month_month_id`='9',`b`.`bulan_bill`,0)) as `bill_mar`, 
                                        SUM(if(`b`.`month_month_id`='10',`b`.`bulan_bill`,0)) as `bill_apr`, 
                                        SUM(if(`b`.`month_month_id`='11',`b`.`bulan_bill`,0)) as `bill_mei`, 
                                        SUM(if(`b`.`month_month_id`='12',`b`.`bulan_bill`,0)) as `bill_jun`, 
                                        SUM(if(`b`.`month_month_id`='1',`b`.`bulan_status`,0)) as `status_jul`, 
                                        SUM(if(`b`.`month_month_id`='2',`b`.`bulan_status`,0)) as `status_agu`, 
                                        SUM(if(`b`.`month_month_id`='3',`b`.`bulan_status`,0)) as `status_sep`, 
                                        SUM(if(`b`.`month_month_id`='4',`b`.`bulan_status`,0)) as `status_okt`, 
                                        SUM(if(`b`.`month_month_id`='5',`b`.`bulan_status`,0)) as `status_nov`, 
                                        SUM(if(`b`.`month_month_id`='6',`b`.`bulan_status`,0)) as `status_des`, 
                                        SUM(if(`b`.`month_month_id`='7',`b`.`bulan_status`,0)) as `status_jan`, 
                                        SUM(if(`b`.`month_month_id`='8',`b`.`bulan_status`,0)) as `status_feb`, 
                                        SUM(if(`b`.`month_month_id`='9',`b`.`bulan_status`,0)) as `status_mar`, 
                                        SUM(if(`b`.`month_month_id`='10',`b`.`bulan_status`,0)) as `status_apr`, 
                                        SUM(if(`b`.`month_month_id`='11',`b`.`bulan_status`,0)) as `status_mei`, 
                                        SUM(if(`b`.`month_month_id`='12',`b`.`bulan_status`,0)) as `status_jun`,
                                        MAX(if(`b`.`month_month_id`='1',`b`.`bulan_date_pay`,0)) as `date_pay_jul`, 
                                        MAX(if(`b`.`month_month_id`='2',`b`.`bulan_date_pay`,0)) as `date_pay_agu`, 
                                        MAX(if(`b`.`month_month_id`='3',`b`.`bulan_date_pay`,0)) as `date_pay_sep`, 
                                        MAX(if(`b`.`month_month_id`='4',`b`.`bulan_date_pay`,0)) as `date_pay_okt`, 
                                        MAX(if(`b`.`month_month_id`='5',`b`.`bulan_date_pay`,0)) as `date_pay_nov`, 
                                        MAX(if(`b`.`month_month_id`='6',`b`.`bulan_date_pay`,0)) as `date_pay_des`, 
                                        MAX(if(`b`.`month_month_id`='7',`b`.`bulan_date_pay`,0)) as `date_pay_jan`, 
                                        MAX(if(`b`.`month_month_id`='8',`b`.`bulan_date_pay`,0)) as `date_pay_feb`, 
                                        MAX(if(`b`.`month_month_id`='9',`b`.`bulan_date_pay`,0)) as `date_pay_mar`, 
                                        MAX(if(`b`.`month_month_id`='10',`b`.`bulan_date_pay`,0)) as `date_pay_apr`, 
                                        MAX(if(`b`.`month_month_id`='11',`b`.`bulan_date_pay`,0)) as `date_pay_mei`, 
                                        MAX(if(`b`.`month_month_id`='12',`b`.`bulan_date_pay`,0)) as `date_pay_jun`, 
                                        MAX(if(`b`.`month_month_id`='1',`m`.`month_name`,'')) as `month_name_jul`, 
                                        MAX(if(`b`.`month_month_id`='2',`m`.`month_name`,'')) as `month_name_agu`, 
                                        MAX(if(`b`.`month_month_id`='3',`m`.`month_name`,'')) as `month_name_sep`, 
                                        MAX(if(`b`.`month_month_id`='4',`m`.`month_name`,'')) as `month_name_okt`, 
                                        MAX(if(`b`.`month_month_id`='5',`m`.`month_name`,'')) as `month_name_nov`, 
                                        MAX(if(`b`.`month_month_id`='6',`m`.`month_name`,'')) as `month_name_des`, 
                                        MAX(if(`b`.`month_month_id`='7',`m`.`month_name`,'')) as `month_name_jan`, 
                                        MAX(if(`b`.`month_month_id`='8',`m`.`month_name`,'')) as `month_name_feb`, 
                                        MAX(if(`b`.`month_month_id`='9',`m`.`month_name`,'')) as `month_name_mar`, 
                                        MAX(if(`b`.`month_month_id`='10',`m`.`month_name`,'')) as `month_name_apr`, 
                                        MAX(if(`b`.`month_month_id`='11',`m`.`month_name`,'')) as `month_name_mei`, 
                                        MAX(if(`b`.`month_month_id`='12',`m`.`month_name`,'')) as `month_name_jun`,
                                        MAX(if(`b`.`month_month_id`='1',`a`.`account_description`,'')) as `account_jul`, 
                                        MAX(if(`b`.`month_month_id`='2',`a`.`account_description`,'')) as `account_agu`, 
                                        MAX(if(`b`.`month_month_id`='3',`a`.`account_description`,'')) as `account_sep`, 
                                        MAX(if(`b`.`month_month_id`='4',`a`.`account_description`,'')) as `account_okt`, 
                                        MAX(if(`b`.`month_month_id`='5',`a`.`account_description`,'')) as `account_nov`, 
                                        MAX(if(`b`.`month_month_id`='6',`a`.`account_description`,'')) as `account_des`, 
                                        MAX(if(`b`.`month_month_id`='7',`a`.`account_description`,'')) as `account_jan`, 
                                        MAX(if(`b`.`month_month_id`='8',`a`.`account_description`,'')) as `account_feb`, 
                                        MAX(if(`b`.`month_month_id`='9',`a`.`account_description`,'')) as `account_mar`, 
                                        MAX(if(`b`.`month_month_id`='10',`a`.`account_description`,'')) as `account_apr`, 
                                        MAX(if(`b`.`month_month_id`='11',`a`.`account_description`,'')) as `account_mei`, 
                                        MAX(if(`b`.`month_month_id`='12',`a`.`account_description`,'')) as `account_jun`, 
                                        SUM(if(`b`.`month_month_id`='1',`b`.`bulan_id`,0)) as `month_id_jul`,
                                        SUM(if(`b`.`month_month_id`='2',`b`.`bulan_id`,0)) as `month_id_agu`,
                                        SUM(if(`b`.`month_month_id`='3',`b`.`bulan_id`,0)) as `month_id_sep`,
                                        SUM(if(`b`.`month_month_id`='4',`b`.`bulan_id`,0)) as `month_id_okt`,
                                        SUM(if(`b`.`month_month_id`='5',`b`.`bulan_id`,0)) as `month_id_nov`,
                                        SUM(if(`b`.`month_month_id`='6',`b`.`bulan_id`,0)) as `month_id_des`,
                                        SUM(if(`b`.`month_month_id`='7',`b`.`bulan_id`,0)) as `month_id_jan`,
                                        SUM(if(`b`.`month_month_id`='8',`b`.`bulan_id`,0)) as `month_id_feb`,
                                        SUM(if(`b`.`month_month_id`='9',`b`.`bulan_id`,0)) as `month_id_mar`,
                                        SUM(if(`b`.`month_month_id`='10',`b`.`bulan_id`,0)) as `month_id_apr`,
                                        SUM(if(`b`.`month_month_id`='11',`b`.`bulan_id`,0)) as `month_id_mei`,
                                        SUM(if(`b`.`month_month_id`='12',`b`.`bulan_id`,0)) as `month_id_jun`
                                        FROM `bulan` as `b` 
                                        JOIN `payment` as `p` ON `b`.`payment_payment_id` = `p`.`payment_id` 
                                        JOIN `pos` as `s` ON `s`.`pos_id` = `p`.`pos_pos_id` 
                                        LEFT JOIN `account` as `a` ON `a`.`account_id` = `b`.`bulan_account_id` 
                                        JOIN `period` as `d` ON `d`.`period_id` = `p`.`period_period_id`
                                        JOIN `student` as `t` ON `t`.`student_id` = `b`.student_student_id
                                        JOIN `month` as `m` ON `m`.`month_id` = `b`.month_month_id
                                        WHERE  `d`.`period_id` = '$tahun' AND student_student_id = '$siswaID' AND `p`.`payment_id` = '$payid'
                                        GROUP BY `p`.`payment_id`";
                ?>
				

                <div class="clear"></div>
                

                <?php 
                    include("footer/footer.php");
                ?>

			</main>
			<!-- Page content end -->
		</div>
	</div>

	<!--Page loader DOM Elements. Requared all pages-->
	<div class="sweet-loader">
		<div class="box">
		  	<div class="circle1"></div>
		  	<div class="circle2"></div>
		  	<div class="circle3"></div>
		</div>
	</div>

	<!-- JQuery library file. requared all pages -->
	<script src="js/jquery-3.2.1.min.js"></script>

	<!-- Template global script file. requared all pages -->
	<script src="js/global.script.js"></script>

	
</body>


</html>