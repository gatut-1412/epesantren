<?php
session_start();
if(!isset($_SESSION["student_id"])){
	header("Location:login.php");
}
include_once("config/config.php");


// Fetch all data from database
$result = mysqli_query($mysqli, "SELECT * FROM student 
LEFT JOIN majors ON student.majors_majors_id = majors.majors_id 
LEFT JOIN class ON student.class_class_id = class.class_id
LEFT JOIN madin ON majors.majors_id = madin.madin_majors_id
ORDER BY student_id ASC");

$student_id=$_SESSION["student_id"];
$student_nis=$_SESSION["student_nis"];
$student_full_name=$_SESSION["student_full_name"];
$class_name=$_SESSION["class_name"];
$majors_name=$_SESSION["majors_name"];
?>
<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=0">
	<title>Profil</title>

	<!-- Google font file. If you want you can change. -->
	<link href="https://fonts.googleapis.com/css?family=Nunito:400,600,700,900" rel="stylesheet">

	<!-- Fontawesome font file css -->
	<link rel="stylesheet" type="text/css" href="css/font-awesome.min.css">

	<!-- Template global css file. Requared all pages -->
	<link rel="stylesheet" type="text/css" href="css/global.style.css">
</head>

<body>
	
	<div class="wrapper">
		<?php 
		include("menu/nav-menu.php");
		?>
		<div class="wrapper-inline">
			<!-- Header area start -->
			<header> <!-- extra class no-background -->
				<a class="go-back-link" href="main.php"><i class="fa fa-arrow-left"></i></a>
				<h1 class="page-title">Student Profile</h1>
				<div class="navi-menu-button">
					<em></em>
					<em></em>
					<em></em>
				</div>
			</header>
			<!-- Header area end -->
			<!-- Page content start -->
			<main>
				<?php  { 

				$siswaID = $_SESSION["student_id"];

				$siswa = "SELECT `student`.`student_full_name`, `student_nis`, `student_born_place`, `student_address`, `student_gender`, `student_img`, `student_born_date`, `student_name_of_mother`, `student_name_of_father`, `student_parent_phone`, 
				`majors_name`, `majors_id`, `majors_short_name`, `madin_id`, `madin_name`, `class_name`, `class_id`
				FROM `student`
				LEFT JOIN `class` ON `class`.class_id = `student`.class_class_id
				LEFT JOIN `majors` ON `student`.majors_majors_id = `majors`.majors_id
				LEFT JOIN `madin` ON `madin`.`madin_id` = `student`.`student_madin`
				WHERE student_id = '$siswaID' 
				ORDER BY `student_id` ASC";
				?>
				<?php
					$mutasi = mysqli_query($mysqli, $siswa);
					while($row = mysqli_fetch_array($mutasi)) {
				?>
				<div class="container">
					<div class="profile-bg">
						<div class="form-divider"></div>
						<div class="form-row txt-center">
							<div class="profile-image">
							<?php 
								if (isset($row['student_img'])) {?>
								<img class="profil-img" alt="User Avatar" src="images/<?php echo $row['student_img']; ?>" width="100" height="100">
							<?php } else { ?>
								<img class="profil-img" alt="User Avatar" src="img/user.png" width="100" height="100">
							<?php } ?>
								<!-- <a href="javascript:void(0);" class="update-btn"><i class="fa fa-camera"></i></a> -->
							</div>

						</div>
						<div class="container">
							<div class="student-name">
								<div class="star-icon"><i class="fa fa-star"></i></div>
								<h3><?php echo $student_full_name; ?></h3>
								<p>Kelas : <?php echo $class_name; ?> </p>
							</div>
						</div>
					</div>

					<div class="form-divider"></div>
					<div class="form-label-divider"><span>ACCOUNT INFO</span></div>
					<div class="form-divider"></div>

					<form action="card-student.php" method="get">
						<input type="hidden" value="<?php echo $siswaID ?>" name="student_id">

								<?php 
									if (isset($_GET['pesan']) AND $_GET['pesan'] == 'berhasil') {?>
									<div class="alert alert-success">
										<strong>Berhasil!</strong> Data anda berhasil diperbarui.
									</div>
								<?php } ?>

								<div class="form-label-divider"><span>Data Pribadi</span></div>
								<div class="form-divider"></div>
								<label>Nama Lengkap</label>
								<div class="form-row-group with-icons">
									<div class="form-row no-padding">
										<i class="fa fa-user"></i>
										<input type="text" name="student_full_name" id="student_full_name" class="form-element" placeholder="name" value="<?php echo $row['student_full_name']; ?>" readonly>
									</div>
								</div>
								<label>Jenis Kelamin</label>
								<div class="form-row-group with-icons">
									<div class="form-row no-padding">
										<i class="fa fa-transgender"></i>
										<input type="text" name="student_gender" id="student_gender" class="form-element" placeholder="gender" value="<?php echo $row['student_gender']; ?>" readonly>
									</div>
								</div>
								<label>Tempat Lahir</label>
								<div class="form-row-group with-icons">
									<div class="form-row no-padding">
										<i class="fa fa-home"></i>
										<input type="text" name="student_born_place" id="student_born_place" class="form-element" placeholder="tempatlahir" value="<?php echo $row['student_born_place']; ?>" readonly>
									</div>
								</div>
								<label>Tanggal Lahir</label>
								<div class="form-row-group with-icons">
									<div class="form-row no-padding">
										<i class="fa fa-calendar"></i>
										<input type="text" name="student_born_date" id="student_born_date" class="form-element" placeholder="tanggallahir" value="<?php echo date('d F Y', strtotime($row['student_born_date'])); ?>" readonly>
									</div>
								</div>
								<label>Alamat</label>
								<div class="form-row-group with-icons">
									<div class="form-row no-padding">
										<i class="fa fa-address-card-o"></i>
										<input type="text" name="student_address" id="student_address" class="form-element" placeholder="Alamat" value="<?php echo $row['student_address']; ?>" readonly>
									</div>
								</div>
								<div class="form-divider"></div>
								<div class="form-label-divider"><span>Data Pesantren</span></div>
								<label>NIS</label>
								<div class="form-row-group with-icons">
									<div class="form-row no-padding">
										<i class="fa fa-id-card-o"></i>
										<input type="text" name="student_nis" id="student_nis" class="form-element" placeholder="nis" value="<?php echo $row['student_nis']; ?>" readonly >
									</div>
								</div>
								<label>Kelas</label>
								<div class="form-row-group with-icons">
									<div class="form-row no-padding">
										<i class="fa fa-book"></i>
										<input type="text" name="student_class" id="student_class" class="form-element" placeholder="nis" value="<?php echo $row['class_name']; ?>" readonly >
									</div>
								</div>
								<label>Unit</label>
								<div class="form-row-group with-icons">
									<div class="form-row no-padding">
										<i class="fa fa-building-o"></i>
										<input type="text" name="student_unit" id="student_unit" class="form-element" placeholder="nis" value="<?php echo $row['majors_name']; ?>" readonly >
									</div>
								</div>
								<label>Madin</label>
								<div class="form-row-group with-icons">
									<div class="form-row no-padding">
										<i class="fa fa-bed"></i>
										<input type="text" name="student_madin" id="student_madin" class="form-element" placeholder="nis" value="<?php echo $row['madin_name']; ?>" readonly >
									</div>
								</div>
								<div class="form-divider"></div>
								<div class="form-label-divider"><span>Data Keluarga</span></div>
								<label>Nama Ayah</label>
								<div class="form-row-group with-icons">
									<div class="form-row no-padding">
										<i class="fa fa-user"></i>
										<input type="text" name="student_name_of_father" id="student_name_of_father" class="form-element" placeholder="ayah" value="<?php echo $row['student_name_of_father']; ?>" readonly>
									</div>
								</div>
								<label>Nama Ibu</label>
								<div class="form-row-group with-icons">
									<div class="form-row no-padding">
										<i class="fa fa-user"></i>
										<input type="text" name="student_name_of_mother" id="student_name_of_mother" class="form-element" placeholder="ibu" value="<?php echo $row['student_name_of_mother']; ?>" readonly>
									</div>
								</div>
								<label>No.Whatsapp Orang Tua</label>
								<div class="form-row-group with-icons">
									<div class="form-row no-padding">
										<i class="fa fa-phone"></i>
										<input type="text" name="student_parent_phone" id="student_parent_phone" class="form-element" placeholder="kamar" value="<?php echo $row['student_parent_phone']; ?>" readonly>
									</div>
								</div>
								<?php
								}
								}
								?>
					</form>
				
					<div class="form-divider"></div>

					<div class="form-row">
						<a href="<?php echo 'https://' . $foldersekolah . '.epesantren.co.id/student/profile/printPdf/' . $siswaID ?>" class="button circle block orange" target="_blank">Cetak Kartu</a>
					</div>
					<div class="form-row">
						<a href="student_profil_edit.php" class="button circle yellow block">Edit</a>
					</div>
					<div class="form-row">
						<a href="ganti-password.php" class="button circle red block">Ganti Password</a>
					</div>

					<div class="form-divider"></div>

				</div>
				<?php 
					include("footer/footer.php");
				?>
			</main>
			<!-- Page content end -->
		</div>
	</div>


	<!--Page loader DOM Elements. Requared all pages-->
	<div class="sweet-loader">
		<div class="box">
		  	<div class="circle1"></div>
		  	<div class="circle2"></div>
		  	<div class="circle3"></div>
		</div>
	</div>

	<!-- JQuery library file. requared all pages -->
	<script src="js/jquery-3.2.1.min.js"></script>

	<!-- Template global script file. requared all pages -->
	<script src="js/global.script.js"></script>

	
</body>
<style>
.profil-img {
  border-radius: 50%;
}
</style>
</html>