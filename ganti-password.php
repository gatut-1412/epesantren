<?php
session_start();
if(!isset($_SESSION["student_id"])){
	header("Location:login.php");
}
include_once("config/config.php");
// include_once("login_action.php");

// Fetch all data from database

$id = $_GET['id'];
// $nomor = 1;

//$row = $result->fetch_assoc();

$student_id=$_SESSION["student_id"];
$student_nis=$_SESSION["student_nis"];
$student_full_name=$_SESSION["student_full_name"];
$student_password=$_SESSION["student_password"];
$class_name=$_SESSION["class_name"];
$majors_name=$_SESSION["majors_name"];

?>
<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=0">
	<title>Profil</title>

	<!-- Google font file. If you want you can change. -->
	<link href="https://fonts.googleapis.com/css?family=Nunito:400,600,700,900" rel="stylesheet">

	<!-- Fontawesome font file css -->
	<link rel="stylesheet" type="text/css" href="css/font-awesome.min.css">

	<!-- Template global css file. Requared all pages -->
    <link rel="stylesheet" type="text/css" href="css/global.style.css">
    
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="/resources/demos/style.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">

</head>

<body>
	
	<div class="wrapper">
		<div class="nav-menu">
			<nav class="menu">
                <!-- Template logo start -->
                <div class="nav-header">
                    <a href="student-profile.php">
                        <img class="image-round" alt="" src="img/user.png">
                        <span><?php echo $student_full_name; ?></span>
                        <span><?php echo $class_name; ?></span>
                        <span><?php echo $majors_name; ?></span>
                    </a>
                </div>
                <!-- Template logo end -->
                <!-- Menu navigation start -->
				<div class="nav-container">
                    <ul class="main-menu">
                    	<li class="active">
                            <a href="main.php"><img src="img/icons/home24.png" alt=""> Dashboard</a>
                        </li>
                        <!-- <li>
                            <a href="student-profile.php"><img src="img/icons/boy24.png" alt=""> Student Profile</a>
                        </li> -->
                        <li>
                            <a href="tab-top-tahfidz.php"><img src="img/icons/study24.png" alt=""> Tahfidz </a>
                        </li>
                        <li>
                            <a href="tab-top-perbaikan.php"><img src="img/icons/cost24.png" alt=""> Pembayaran</a>
                        </li>
                        <li>
                            <a href="tab-top-absensi.php"><img src="img/icons/permission24.png" alt=""> Absensi </a>
                        </li>
                        <li>
                            <a href="tab-top-tabungan.php"><img src="img/icons/purse24.png" alt=""> Tabungan </a>
                        </li>
                        <li>
                            <a href="product-detail-1.php"><img src="img/icons/consultation24.png" alt=""> Konseling </a>
                        </li>
                        <li>
                            <a href="product-detail-2.php"><img src="img/icons/heart24.png" alt=""> Kesehatan </a>
                        </li>

                        <li>
                            <a href="post-list-2.php"><img src="img/icons/newspaper.png" alt=""> Info Terbaru</a>
                        </li>
                        <!-- <li>
                            <a href="change-password.php"><img src="img/icons/exchange.png" alt=""> Ubah Password </a>
                        </li> -->
                        <li>
                            <a href="logout.php"><img src="img/icons/logout.png" alt=""> Logout</a>
                        </li>
                    </ul>
                </div>
                <!-- Menu navigation end -->
            </nav>

		</div>
		<div class="wrapper-inline">
			<!-- Header area start -->
			<header> <!-- extra class no-background -->
				<!-- <a class="go-back-link" href="javascript:history.back();"><i class="fa fa-arrow-left"></i></a> -->
				<h1 class="page-title">Ganti Password</h1>
				<div class="navi-menu-button">
					<em></em>
					<em></em>
					<em></em>
				</div>
			</header>
			<!-- Header area end -->
			<!-- Page content start -->
			<main>
			<div class="container">

                    <div class="form-divider"></div>
                    <?php  { 

                            $siswaID = $_SESSION["student_id"];

                            // $infosiswa = "SELECT `student`.`student_full_name`, `student_nis`, `student_password`, `student_born_place`, `student_address`, `student_gender`, `student_born_date`, `student_name_of_mother`, `student_name_of_father`, `student_parent_phone`
                            // FROM `student`
                            // WHERE student_id = '$siswaID' 
                            // ORDER BY `student_id` ASC";

                            //mengatasi error notice dan warning
                            //error ini biasa muncul jika dijalankan di localhost, jika online tidak ada masalah
                            error_reporting(E_ALL ^ (E_NOTICE | E_WARNING));

                            //koneksi ke database
                            include_once("config/config.php");

                            //proses jika tombol rubah di klik
                            if($_POST['submit']){
                                //membuat variabel untuk menyimpan data inputan yang di isikan di form
                                $password_lama			= $_POST['password_lama'];
                                $password_baru			= $_POST['password_baru'];
                                $konfirmasi_password	= $_POST['konfirmasi_password'];
                                
                                //cek dahulu ke database dengan query SELECT
                                //kondisi adalah WHERE (dimana) kolom password adalah $password_lama di encrypt m5
                                //encrypt -> md5($password_lama)
                                $password_lama	= md5($password_lama);
                                $cek 			= mysqli_query($mysqli, "SELECT student_password 
                                                  FROM student 
                                                  WHERE student_password ='$password_lama'");
                                
                                if($cek->num_rows){
                                    //kondisi ini jika password lama yang dimasukkan sama dengan yang ada di database
                                    //membuat kondisi minimal password adalah 5 karakter
                                    if(strlen($password_baru) >= 6){
                                        //jika password baru sudah 5 atau lebih, maka lanjut ke bawah
                                        //membuat kondisi jika password baru harus sama dengan konfirmasi password
                                        if($password_baru == $konfirmasi_password){
                                            //jika semua kondisi sudah benar, maka melakukan update kedatabase
                                            //query UPDATE SET password = encrypt md5 password_baru
                                            //kondisi WHERE id user = session id pada saat login, maka yang di ubah hanya user dengan id tersebut
                                            $password_baru 	    = md5($password_baru);
                                            $id_student 		= $siswaID; //ini dari session saat login
                                            
                                            $update 		= mysqli_query($mysqli, "UPDATE student SET student_password='$password_baru' WHERE student_id='$id_student'");
                                            if($update){
                                                //kondisi jika proses query UPDATE berhasil
                                                echo '<div class="alert alert-success">
                                                        <strong>Berhasil!</strong> Password anda berhasil diperbarui. Klik Kembali.
                                                        </div>
                                                        <a href="student-profile.php" class="button circle block green">Kembali</a>';
                                            }else {
                                                //kondisi jika proses query gagal
                                                echo '<div class="alert alert-danger">
                                                        <strong>Gagal!</strong> Password anda gagal diperbarui.
                                                    </div>';
                                            }					
                                        }else{
                                            //kondisi jika password baru beda dengan konfirmasi password
                                            echo '<div class="alert alert-danger">
                                                        <strong>Gagal!</strong> Password baru anda berbeda dengan konfirmasi password.
                                                    </div>';
                                        }
                                    }else{
                                        //kondisi jika password baru yang dimasukkan kurang dari 5 karakter
                                        echo '<div class="alert alert-danger">
                                                        <strong>Gagal!</strong> password baru yang anda masukkan kurang dari 5 karakter.
                                                    </div>';
                                    }
                                }else{
                                    //kondisi jika password lama tidak cocok dengan data yang ada di database
                                    echo '<div class="alert alert-danger">
                                                <strong>Gagal!</strong> password lama anda tidak cocok dengan password yang ada di database.
                                            </div>';
                                }
                            }

                    ?>
                
                    <form method="post" action="">
                        <input type="hidden" value="<?php echo $siswaID ?>" name="student_id">

                    <div class="form-divider"></div>
                    <label>Password Lama</label>
                        <div class="form-row-group with-icons">
                            <div class="form-row no-padding">
                                <i class="fa fa-user"></i>
                                <input type="password" name="password_lama" class="form-element" placeholder="Masukkan Password Lama" required>
                            </div>
                        </div>

                    <label>Password Baru</label>
                        <div class="form-row-group with-icons">
                            <div class="form-row no-padding">
                                <i class="fa fa-user"></i>
                                <input type="password" name="password_baru" class="form-element" placeholder="Masukkan Password Baru" required>
                            </div>
                        </div>
                    <label>Konfirmasi Password</label>
                        <div class="form-row-group with-icons">
                            <div class="form-row no-padding">
                                <i class="fa fa-user"></i>
                                <input type="password" name="konfirmasi_password" class="form-element" placeholder="Konfirmasi Password Baru" required>
                            </div>
                        </div>
                        <div class="form-row">
                            <input type="submit" name="submit" class="button circle block green" value="Simpan">
                        </div>
                        <div class="form-row">
                            <a href="javascript:history.back();" class="button circle block red">Batal</a>
                        </div>
                    </form>
					<?php
                    }
					?>

                </div>
                
                <footer>
                    <div class="container">
                        <ul>
                            <li><a href="#"><img src="img/icons/www32.png" alt=""></i></a></li>
                            <li><a href="#"><img src="img/icons/whatsapp32.png" alt=""></i></a></li>
                            <li><a href="#"><img src="img/icons/facebook32.png" alt=""></i></a></li>
                            <li><a href="#"><img src="img/icons/instagram32.png" alt=""></i></a></li>
                            <li><a href="#"><img src="img/icons/youtube32.png" alt=""></i></a></li>
                        </ul>
                        <p>Copyright © All Right Reserved</p>
                    </div>
                </footer>
			</main>
			<!-- Page content end -->
		</div>
	</div>


	<!--Page loader DOM Elements. Requared all pages-->
	<div class="sweet-loader">
		<div class="box">
		  	<div class="circle1"></div>
		  	<div class="circle2"></div>
		  	<div class="circle3"></div>
		</div>
	</div>

	<!-- JQuery library file. requared all pages -->
	<script src="js/jquery-3.2.1.min.js"></script>

	<!-- Template global script file. requared all pages -->
    <script src="js/global.script.js"></script>
    
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

    <script>
        $( function() {
            $( "#date" ).datepicker({
            dateFormat: "yy-mm-dd"
            });
        } );
    </script>

	
</body>

</html>