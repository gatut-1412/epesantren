<?php

session_start();

$_SESSION['student_id']='';
$_SESSION['student_nis']='';
$_SESSION['student_full_name']='';
$_SESSION["folder"];

unset($_SESSION['student_id']);
unset($_SESSION['student_nis']);
unset($_SESSION['student_full_name']);
unset($_SESSION["folder"]);

session_unset();
session_destroy();
header('Location:index.php');

?>