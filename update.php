<?php 

error_reporting(E_ALL & ~E_NOTICE);error_reporting(0);
include_once("config/config.php");

//buat variabel dari setiap field name form student

$id = $_POST['student_id'] ?? '';
$nama = $_POST['student_full_name'] ?? '';
$nama_baru = str_replace("'","`", $nama);    
$alamat = $_POST['student_address'] ?? '';   
$tempatlahir = $_POST['student_born_place'] ?? ''; 
$tanggallahir = $_POST['student_born_date'] ?? '';        
$nomorwa = $_POST['student_parent_phone'] ?? '';
$gender = $_POST['student_gender'] ?? '';       
$ayah = $_POST['student_name_of_father'] ?? '';
$ibu = $_POST['student_name_of_mother'] ?? '';

$nomorwa= str_replace(" ","",$nomorwa);
$nomorwa= str_replace("(","",$nomorwa);
$nomorwa= str_replace(")","",$nomorwa);
$nomorwa= str_replace(".","",$nomorwa);

if(!preg_match('/[^+0-9]/',trim($nomorwa)))
{
        if(substr(trim($nomorwa), 0, 1)=='+')
        {
        $hp = trim($nomorwa);
        }
        elseif(substr(trim($nomorwa), 0, 1)=='0')
        {
        $hp = '+62'.substr(trim($nomorwa), 1);
        }
        elseif(substr(trim($nomorwa), 0, 2)=='62')
        {
        $hp = '+'.trim($nomorwa);
        }
        elseif(substr(trim($nomorwa), 0, 1)=='8')
        {
        $hp = '+62'.trim($nomorwa);
        }
        else
        {
        $hp = '+'.trim($nomorwa);
        }		 
}
  $gambar_produk = $_FILES['student_img']['name'];
  //cek dulu jika merubah gambar produk jalankan coding ini
  if($gambar_produk != "") {
    $ekstensi_diperbolehkan = array('png','jpg'); //ekstensi file gambar yang bisa diupload 
    $x = explode('.', $gambar_produk); //memisahkan nama file dengan ekstensi yang diupload
    $ekstensi = strtolower(end($x));
    $file_tmp = $_FILES['student_img']['tmp_name'];
    $angka_acak     = rand(1,999);
    $nama_gambar_baru = $angka_acak.'-'.$gambar_produk; //menggabungkan angka acak dengan nama file sebenarnya
    if(in_array($ekstensi, $ekstensi_diperbolehkan) === true)  {
                  move_uploaded_file($file_tmp, 'images/'.$nama_gambar_baru); //memindah file gambar ke folder gambar
                      
                    // jalankan query UPDATE berdasarkan ID yang produknya kita edit
                   $querygambar  = "UPDATE student SET student_full_name ='$nama_baru', student_address ='$alamat', student_img='$nama_gambar_baru',
                        student_born_date ='$tanggallahir',student_gender = '$gender', student_born_place = '$tempatlahir', 
                        student_name_of_mother = '$ibu', student_name_of_father = '$ayah',
                        student_parent_phone = '$hp' WHERE student_id ='$id'";
                    $result = mysqli_query($mysqli, $querygambar);
                    // periska query apakah ada error
                    if(!$result){
                        die ("Query gagal dijalankan: ".mysqli_errno($mysqli).
                             " - ".mysqli_error($mysqli));
                    } else {
                      //tampil alert dan akan redirect ke halaman index.php
                      //silahkan ganti index.php sesuai halaman yang akan dituju
                      echo "<script>alert('Data berhasil diubah.');window.location='student-profile.php';</script>";
                    }
              } else {     
               //jika file ekstensi tidak jpg dan png maka alert ini yang tampil
                  echo "<script>alert('Ekstensi gambar yang boleh hanya jpg atau png.');window.location='student_profil_edit.php';</script>";
              }
    } else {
      // jalankan query UPDATE berdasarkan ID yang produknya kita edit
      $query  = "UPDATE student SET student_full_name ='$nama_baru', student_address ='$alamat', 
                student_born_date ='$tanggallahir',student_gender = '$gender', student_born_place = '$tempatlahir', 
                student_name_of_mother = '$ibu', student_name_of_father = '$ayah',
                student_parent_phone = '$hp' WHERE student_id ='$id'";
      $result = mysqli_query($mysqli, $query);
      // periska query apakah ada error
      if(!$result){
            die ("Query gagal dijalankan: ".mysqli_errno($mysqli).
                             " - ".mysqli_error($mysqli));
      } else {
        //tampil alert dan akan redirect ke halaman index.php
        //silahkan ganti index.php sesuai halaman yang akan dituju
          echo "<script>alert('Data berhasil diubah.');window.location='student-profile.php';</script>";
      }
    }


header("location:student-profile.php?pesan=berhasil");
// header("location:student_profile_edit.php?pesan=gagal");

?>

