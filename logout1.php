<?php
session_start();

unset($_SESSION['student_id']);
unset($_SESSION['student_nis']);
unset($_SESSION['student_full_name']);

session_unset();
session_destroy();
header('Location:index.php');
?>