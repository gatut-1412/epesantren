<?php
    session_start();
    include_once("config/config.php");
    if(empty($_SESSION['student_nis'])){
    header('location:main.php');
    }
    if((time() - $_SESSION['auto_logout_time'])>900){
    header('location:logout1.php');
    }
?>

<?php
    $student_full_name=$_SESSION["student_full_name"];
    $class_name=$_SESSION["class_name"];
    $majors_name=$_SESSION["majors_name"];
    $student_id=$_SESSION["student_id"];
    $student_nis=$_SESSION["student_nis"];
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=0">
    <title>Absensi</title>

	<!-- Google font file. If you want you can change. -->
	<link href="https://fonts.googleapis.com/css?family=Nunito:400,600,700,900" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.min.css">

	<!-- Fontawesome font file css -->
	<link rel="stylesheet" type="text/css" href="css/font-awesome.min.css">

	<!-- Template global css file. Requared all pages -->
	<link rel="stylesheet" type="text/css" href="css/global.style.css">
</head>

<body>
	
<div class="wrapper">
        <?php 
		    include("menu/nav-menu.php");
		?>
		<div class="wrapper-inline">
			<!-- Header area start -->
			<header> <!-- extra class no-background -->
                <a class="go-back-link" href="main.php"><i class="fa fa-arrow-left"></i></a>
				<h1 class="page-title">Absensi Siswa</h1>
				<div class="navi-menu-button">
					<em></em>
					<em></em>
					<em></em>
				</div>
			</header>
			<!-- Header area end -->
			<!-- Page content start -->
			<main class="fix-top-menu">

                <div class="container">
                    <form action="<?php echo $_SERVER["PHP_SELF"];?>" method="get">
                        <div class="form-row no-padding">
                            <label for="sel1">Tahun Ajaran:</label>
                            <select class="form-element" name="period">
                                <?php
                                        include "config/config.php";
                                                            
                                        $sql="select * from period";

                                        $hasil=mysqli_query($mysqli,$sql);
                                        $no=0;
                                        while ($data = mysqli_fetch_array($hasil)) {
                                        $no++;

                                        $ket="";
                                            if (isset($_GET['period'])) {
                                            $period = trim($_GET['period']);

                                            if ($period==$data['period_id'])
                                            {
                                            $ket="selected";
                                            }
                                        }
                                ?>
                                    <option <?php echo $ket; ?> value="<?php echo $data['period_id'];?>"><?php echo $data['period_start'];?>/<?php echo $data['period_end'];?></option>
                                <?php
                                }
                                ?>
                                
                            </select>
                            <br>
                            <label for="sel1">Tanggal:</label>
                            <input type="date" name="tanggal" class="form-element">
                            <br>
                            <label for="sel1">Kelas:</label>
                            <select class="form-element" name="kelas">
                                <?php
                                        include "config/config.php";
                                                            
                                        $sql="select * from class";

                                        $hasil=mysqli_query($mysqli,$sql);
                                        $no=0;
                                        while ($data = mysqli_fetch_array($hasil)) {
                                        $no++;

                                        $ket="";
                                            if (isset($_GET['kelas'])) {
                                            $class = trim($_GET['kelas']);

                                            if ($class==$data['class_id'])
                                            {
                                            $ket="selected";
                                            }
                                        }
                                ?>
                                <option <?php echo $ket; ?> value="<?php echo $data['class_id'];?>"><?php echo $data['class_name'];?></option>
                                <?php
                                }
                                ?>
                            </select>
                            <div class="form-divider"></div>
                            <div class="col-sm-4">
                                <button type="submit" class="button circle green"><i class="fa fa-search"> </i> Cari Data</button>
                            </div>
                        </div>
                    </form>
                </div>

                <?php if (isset($_GET['period']) && ($_GET['bulan'])) {
                        $tahun      = $_GET['period'];
                        $bulan      = $_GET['month'];
                        $siswaID    = $_SESSION["student_id"];
                ?>

                <?php 
                }  
                ?>

                <div class="container">
                    <div class="form-divider"></div>
                    <div class="form-label-divider"><span>Tabel Absensi Siswa (Next Update)</span></div>
                    <div class="form-divider"></div>
                        <div class="table-responsive">
                            <table id="example" class="table table-striped table-bordered">
                                <thead class="thead-dark">
                                    <tr>
                                        <th>No</th>
                                        <th>Tanggal</th>
                                        <th>Keterangan</th>
                                    </tr>
                                </thead>
                                    <tbody>
                                        <tr>
                                            <td>1</td>
                                            <td>26-04-2021</td>
                                            <td>
                                                <button class="button circle darkblue" data-popup="260421">Lihat</button>
                                            </td>
                                        </tr>
                                    </tbody>
                            </table>
                        </div>
                </div>

                <div class="form-divider"></div>
                <div class="container">
                    <div class="form-row">
                        <a href="<?php echo 'https://' . $foldersekolah . '.epesantren.com/manage/tahfidz/printBook/?n=' . base64_encode($tahun) . '&r=' . base64_encode($siswaNIS) ?>" class="button circle block blue">Cetak Laporan Absensi</a>
                    </div>
                </div>
                <?php 
                    include("footer/footer.php");
                ?>
			</main>
            <!-- Page content end -->
        </div>
        
</div>

<!-- <div class="popup-overlay" id="260421" style="display: none;">
    <div class="popup-container">
        <div class="popup-header">
            <h3 class="popup-title">26-04-2021</h3>
            <span class="popup-close" data-dismiss="true"><i class="fa fa-times"></i></span>
        </div>
        <div class="popup-content">
            <div class="table-responsive">
                <table id="example" class="table table-striped table-bordered">
                    <thead class="thead-dark">
                        <tr>
                            <th>No</th>
                            <th>Waktu</th>
                            <th>Jam Ke</th>
                            <th>Mata Pelajaran</th>
                            <th>Masuk</th>
                            <th>Sakit</th>
                            <th>Izin</th>
                            <th>Alasan</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>1</td>
                            <td>07.00-07.45</td>
                            <td>1</td>
                            <td>Bahasa Indonesia</td>
                            <td><input type="radio" name="gender"></td>
                            <td><input type="radio" name="gender"></td>
                            <td><input type="radio" name="gender"></td>
                            <td><input type="radio" name="gender"></td>
                        </tr>
                        <tr>
                            <td>2</td>
                            <td>07.45-08.30</td>
                            <td>2</td>
                            <td>Bahasa Inggris</td>
                            <td><input type="radio" name="gender"></td>
                            <td><input type="radio" name="gender"></td>
                            <td><input type="radio" name="gender"></td>
                            <td><input type="radio" name="gender"></td>
                        </tr>
                        <tr>
                            <td>3</td>
                            <td>08.30-09.15</td>
                            <td>3</td>
                            <td>Matematika</td>
                            <td><input type="radio" name="gender"></td>
                            <td><input type="radio" name="gender"></td>
                            <td><input type="radio" name="gender"></td>
                            <td><input type="radio" name="gender"></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        <div class="popup-footer">
            <button class="button orange">Save</button>
            <button class="button" data-dismiss="true">Cancel</button>
        </div>
    </div>
</div> -->

	<!--Page loader DOM Elements. Requared all pages-->
	<div class="sweet-loader">
		<div class="box">
		  	<div class="circle1"></div>
		  	<div class="circle2"></div>
		  	<div class="circle3"></div>
		</div>
	</div>

	<!-- JQuery library file. requared all pages -->
	<script src="js/jquery-3.2.1.min.js"></script>

	<!-- Template global script file. requared all pages -->
	<script src="js/global.script.js"></script>

    <!-- JQuery DataTable -->
    <script src="js/jquery-datatable.js"></script>
    <script src="js/jquery-3.5.1.js"></script>
    <script src="js/jquery-datatable-bootstrap4.js"></script>

    <script>
        $(document).ready(function() {
        $('#example').DataTable();
    } );
    </script>
    <!-- JQuery TableAbsensi -->
    <!-- <script src="js/tabel-absensi.js"></script> -->
    <script>
        $( ".selector" ).datepicker({
            dateFormat: "dd/mm/yyyy"
        });
    </script>


	
</body>

</html>