<?php
include_once("config/config.php");

for($count = 0; $count < count($_POST["bulan_id"]); $count++)
        {
            $student = "SELECT  `s`.`pos_name`, `d`.`period_id`, `d`.`period_start`, `d`.`period_end`, `p`.`payment_id`, `p`.`payment_mode`, 
            `b`.`bulan_id`, `b`.`bulan_bill`
            FROM `bulan` as `b` 
            JOIN `payment` as `p` ON `b`.`payment_payment_id` = `p`.`payment_id` 
            JOIN `pos` as `s` ON `s`.`pos_id` = `p`.`pos_pos_id` 
            LEFT JOIN `account` as `a` ON `a`.`account_id` = `b`.`bulan_account_id` 
            JOIN `period` as `d` ON `d`.`period_id` = `p`.`period_period_id`
            JOIN `month` as `m` ON `m`.`month_id` = `b`.month_month_id
            WHERE b.bulan_id IN (".$_POST['bulan_id'][$count].")";

        //     echo $student;
        
        //     foreach($student as $row){
    	// 	    echo '<input type="text" name="student_id[]" id="student_id" value="'.$row['bulan_id'].'">';
    	//     }
        $hasil=mysqli_query($mysqli,$student);
        while($row = mysqli_fetch_array($hasil)){
        $sumTotal += $row['bulan_bill'];
        $Paybulan = $row['pos_name'].' - T.A '.$row['period_start'].'/'.$row['period_end'];
        // $detail = $row['payment_id']
        // $bulanID = $row['bulan_id']
        echo    '<div class="form-divider"></div>
                        <h4 class="title-main">Pembayaran Bulanan</h4>
                <div class="form-divider"></div>
                <input type="hidden" name="bulan_id" id="bulan_id" value="'.$row['bulan_id'].'">
                <div class="form-row-group with-icons">
                        <div class="form-row no-padding">
                                <input type="text" name="pembayaran" id="pembayaran" class="form-element" value="'.$Paybulan.'">
                        </div>
                        <div class="form-row no-padding">
                                <input type="text" name="jumlah" id="jumlah" class="form-element" value = "Rp. '.number_format($row['bulan_bill'], 0, ',', '.').'">
                        </div>
                </div>';
                
        }
        
}
                echo    '<div class="form-divider"></div>
                         <div class="form-row-group with-icons">
                                <div class="form-row no-padding">
                                        <input type="text" name="pembayaran" id="pembayaran" class="form-element" value=" Total : Rp. '.number_format($sumTotal, 0, ',', '.').'">
                                </div>
                         </div>';

for($jumlah = 0; $jumlah < count($_POST["bebas_id"]); $jumlah++)
        {
            $bebas = "SELECT `bebas`.`bebas_id`, `bebas_bill`, `bebas_diskon`, `bebas_total_pay`, `bebas_input_date`, 
            `bebas_last_update`, `student_student_id`, `payment_payment_id`, `pos_name`, `payment_type`, `period_period_id`, `period_start`, `period_end`
            FROM `bebas` 
            LEFT JOIN `student` ON `student`.`student_id` = `bebas`.`student_student_id` 
            LEFT JOIN `payment` ON `payment`.`payment_id` = `bebas`.`payment_payment_id` 
            LEFT JOIN `pos` ON `pos`.`pos_id` = `payment`.`pos_pos_id`
            LEFT JOIN `period` ON `period`.`period_id` = `payment`.`period_period_id`
            WHERE bebas_id IN (".$_POST['bebas_id'][$jumlah].")";

        //     echo $bebas;
        
        //     foreach($student as $row){
    	// 	    echo '<input type="text" name="student_id[]" id="student_id" value="'.$row['bulan_id'].'">';
    	//     }
        $hasil=mysqli_query($mysqli,$bebas);
        while($row = mysqli_fetch_array($hasil)){
        $sisa = $row['bebas_bill']-$row['bebas_total_pay'];
        $namePay = $row['pos_name'].' - T.A '.$row['period_start'].'/'.$row['period_end'];
        // $detail = $row['payment_id']
        // $bulanID = $row['bulan_id']
        echo  '<div class="form-divider"></div>
                        <h4 class="title-main">Pembayaran Bebas</h4>
                <div class="form-divider"></div>
                <input type="hidden" name="bebas_id" id="bebas_id" value="'.$row['bebas_id'].'">
                <div class="form-row-group with-icons">
                        <div class="form-row no-padding">
                                <input type="text" name="pembayaran" id="pembayaran" class="form-element" value="'.$namePay.'" readonly>
                        </div>
                        <label>Isi Nominal</label>
                        <div class="form-row no-padding">
                                <input type="text" name="bill" id="bill" class="form-element">
                        </div>
                        <label>Masukkan Catatan Pembayaran</label>
                        <div class="form-row no-padding">
                                <input type="text" name="note" id="note" class="form-element">
                        </div>
                </div>';
                
        }

        echo    '<div class="form-divider"></div>
                 <div class="form-row-group with-icons">
                        <div class="form-row no-padding">
                                <input type="text" name="pembayaran" id="pembayaran" class="form-element" value=" Sisa : Rp. '.number_format($sisa, 0, ',', '.').'" readonly>
                        </div>
                 </div>';
}
?>

<script>
function remove(elem){
  elem.parentNode.removeChild(elem);
}
</script>