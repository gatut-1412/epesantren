<?php
    session_start();
    if(!isset($_SESSION["student_id"])){
        header("Location:login.php");
    }
    include_once("config/config.php");
    $student_full_name=$_SESSION["student_full_name"];
    $class_name=$_SESSION["class_name"];
    $majors_name=$_SESSION["majors_name"];
?>

<?php 

$siswaID = $_SESSION["student_id"];

$siswa = "SELECT `student`.`student_full_name`, `student_nis`, `student_born_place`, `student_address`, `student_gender`, `student_img`, `student_born_date`, `student_name_of_mother`, `student_name_of_father`, `student_parent_phone`, 
`majors_name`, `majors_id`, `majors_short_name`, `madin_id`, `madin_name`, `class_name`, `class_id`
FROM `student`
LEFT JOIN `class` ON `class`.class_id = `student`.class_class_id
LEFT JOIN `majors` ON `student`.majors_majors_id = `majors`.majors_id
LEFT JOIN `madin` ON `madin`.`madin_id` = `student`.`student_madin`
WHERE student_id = '$siswaID' 
ORDER BY `student_id` ASC";
?>

<?php
    $mutasi = mysqli_query($mysqli, $siswa);
    while($row = mysqli_fetch_array($mutasi)) {
?>
<head>
    <!-- Google font file. If you want you can change. -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:300,400,600,700,900" rel="stylesheet">

    <!-- Fontawesome font file css -->
    <link rel="stylesheet" type="text/css" href="css/font-awesome.min.css">

    <!-- Animate css file for css3 animations. for more : https://daneden.github.io/animate.css -->
    <!-- Only use animate action. If you dont use animation, you don't have to add.-->
    <link rel="stylesheet" type="text/css" href="css/animate.css">

    <!-- Template global css file. Requared all pages -->
    <link rel="stylesheet" type="text/css" href="css/global.style.css">

    <!-- Bootstrap css file. -->
    <link rel="stylesheet" type="text/css" href="cssbootstrap/bootstrap.css">

    <!-- Swiper slider css file -->
    <link rel="stylesheet" href="css/swiper.min.css">

    <!--turbo slider plugin css file -->
    <link rel="stylesheet" type="text/css" href="plugins/turbo-slider/turbo.css">

    <!-- JQuery library file. requared all pages -->
    <script src="js/jquery-3.2.1.min.js"></script>
</head>
<div class="nav-menu">
    <nav class="menu">
        <!-- Template logo start -->
        <div class="nav-header">
            <a href="student-profile.php">
                <div class="profile-image">
                    <?php 
                        if (isset($row['student_img'])) {?>
                        <img class="profil-img" alt="User Avatar" src="images/<?php echo $row['student_img']; ?>" width="100" height="100">
                    <?php } else { ?>
                        <img class="profil-img" alt="User Avatar" src="img/user.png" width="100" height="100">
                    <?php } ?>
                    <a href="student-profile.php" class="update-btn"><i class="fa fa-pencil-square-o"></i></a>
                </div>
                    <span><?php echo $student_full_name; ?></span>
                    <span> Kelas : <?php echo $class_name; ?></span>
                    <span>Unit : <?php echo $majors_name; ?></span>
            </a>
        </div>
        <!-- Template logo end -->
        <!-- Menu navigation start -->
        <?php                   
            $period = mysqli_query($mysqli, "SELECT * FROM `period` WHERE period_status = 1");
            while ($data = mysqli_fetch_array($period)) {
                $periodActive= $data['period_id'];
            }
        ?>
        <div class="nav-container">
            <ul class="main-menu">
                <li class="active">
                    <a href="main.php"><img src="img/icons/home24.png" alt="" /> Dashboard</a>
                </li>
                <li>
                    <a href="tab-top-tahfidz.php?period=<?php echo $periodActive; ?>"><img src="img/icons/study24.png" alt="" /> Tahfidz </a>
                </li>
                <li>
                    <a href="tab-top-perbaikan.php?period=<?php echo $periodActive; ?>"><img src="img/icons/cost24.png" alt="" /> Pembayaran</a>
                </li>
                <li>
                    <a href="upload_bukti.php"><img src="img/icons/bill-wrn.png" alt="" /> Upload Pembayaran</a>
                </li>
                <li>
                    <a href="tab-top-absensi.php?period=<?php echo $periodActive; ?>"><img src="img/icons/permission24.png" alt="" /> Absensi </a>
                </li>
                <li>
                    <a href="tab-top-tabungan.php?period=<?php echo $periodActive; ?>"><img src="img/icons/purse24.png" alt="" /> Tabungan </a>
                </li>
                <li>
                    <a href="product-detail-1.php?period=<?php echo $periodActive; ?>"><img src="img/icons/consultation24.png" alt="" /> Konseling </a>
                </li>
                <li>
                    <a href="product-detail-2.php?period=<?php echo $periodActive; ?>"><img src="img/icons/heart24.png" alt="" /> Rekam Medis </a>
                </li>
                <li>
                    <a href="izin_keluar.php?period=<?php echo $periodActive; ?>"><img src="img/icons/keluar.png" alt="" /> Izin Keluar </a>
                </li>
                <li>
                    <a href="izin_pulang.php?period=<?php echo $periodActive; ?>"><img src="img/icons/pulang.png" alt="" /> Izin Pulang </a>
                </li>
                <li>
                    <a href="post-list-2.php"><img src="img/icons/newspaper.png" alt="" /> Info Terbaru</a>
                </li>
                <li>
                    <a href="logout.php"><img src="img/icons/logout.png" alt="" /> Logout</a>
                </li>
            </ul>
        </div>
        <!-- Menu navigation end -->
    </nav>
</div>
<?php
}
?>
<style>
.profil-img {
  border-radius: 50%;
}
</style>