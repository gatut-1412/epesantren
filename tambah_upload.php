<?php
    session_start();
    if(!isset($_SESSION["student_id"])){
        header("Location:login.php");
    }
    include_once("config/config.php");
    // include("login_action.php");

    $student_full_name=$_SESSION["student_full_name"];
    $class_name=$_SESSION["class_name"];
    $majors_name=$_SESSION["majors_name"];
    $student_id=$_SESSION["student_id"];
    $student_nis=$_SESSION["student_nis"];

?>
<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=0">
	<title>Upload Bukti Pembayaran</title>

	<!-- Google font file. If you want you can change. -->
	<link href="https://fonts.googleapis.com/css?family=Nunito:400,600,700,900" rel="stylesheet">

	<!-- Fontawesome font file css -->
	<link rel="stylesheet" type="text/css" href="css/font-awesome.min.css">

	<!-- Template global css file. Requared all pages -->
    <link rel="stylesheet" type="text/css" href="css/global.style.css">
    
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="/resources/demos/style.css">

</head>

<body>
	
	<div class="wrapper">
        <?php 
		include("menu/nav-menu.php");
		?>
		<div class="wrapper-inline">
			<!-- Header area start -->
			<header> <!-- extra class no-background -->
				<a class="go-back-link" href="javascript:history.back();"><i class="fa fa-arrow-left"></i></a>
				<h1 class="page-title">Upload Bukti Pembayaran</h1>
				<div class="navi-menu-button">
					<em></em>
					<em></em>
					<em></em>
				</div>
			</header>
			<!-- Header area end -->
			<!-- Page content start -->
			<main>
            <div class="container">

                <div class="form-divider"></div>

                <?php 
                  if (isset($_GET['pesan']) AND $_GET['pesan'] == 'gagal') {?>
                  <div class="alert alert-danger">
                    <strong>Maaf!</strong> Data anda gagal dijalankan. Silahkan cek kembali format gambar yang anda masukkan.
                  </div>
                <?php } ?>

                <div class="form-label-divider"><span>Tambah Bukti Pembayaran</span></div>
                <div class="form-divider"></div>
                <form method="POST" action="proses_tambah.php" enctype="multipart/form-data">
                    <input name="student_id" value="<?php echo $student_id; ?>" hidden />
                    <label>Bukti Pembayaran</label>
                    <div class="form-row-group with-icons">
                        <div class="form-row no-padding">
                            <i class="fa fa-file"></i>
                            <input type="file" name="gambar_upload" class="form-element" placeholder="Bukti Pembayaran" accept='image/*' multiple='multiple' capture='camera' required="">
                        </div>
                    </div>
                    <label>Keterangan Pembayaran</label>
                    <div class="form-row-group with-icons">
                        <div class="form-row no-padding">
                            <i class="fa fa-file-text-o"></i>
                            <input type="text" name="keterangan" class="form-element" placeholder="Keterangan Pembayaran">
                        </div>
                    </div>
                    <input name="status" value="2" hidden />

                    <div class="form-row">
                        <button type="submit" name="simpan" class="button circle block green">Simpan</button>
                    </div>       
                </form>

            </div>
            
            <?php 
                include("footer/footer.php");
            ?>
			</main>
			<!-- Page content end -->
		</div>
	</div>


	<!--Page loader DOM Elements. Requared all pages-->
	<div class="sweet-loader">
		<div class="box">
		  	<div class="circle1"></div>
		  	<div class="circle2"></div>
		  	<div class="circle3"></div>
		</div>
	</div>

	<!-- JQuery library file. requared all pages -->
	<script src="js/jquery-3.2.1.min.js"></script>

	<!-- Template global script file. requared all pages -->
    <script src="js/global.script.js"></script>
    
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

    <script>
        $( function() {
            $( "#date" ).datepicker({
            dateFormat: "yy-mm-dd"
            });
        } );
    </script>

	
</body>

</html>