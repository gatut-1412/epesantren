<?php
session_start();

$_SESSION["kode_sekolah"];
$_SESSION["nama_sekolah"];
$_SESSION["alamat_sekolah"];
$_SESSION["db"];
$_SESSION["folder"];

unset ($_SESSION["kode_sekolah"]);
unset ($_SESSION["nama_sekolah"]);
unset ($_SESSION["alamat_sekolah"]);
unset ($_SESSION["db"]);
unset ($_SESSION["folder"]);

session_unset();
session_destroy();
header('Location:index.php');

?>