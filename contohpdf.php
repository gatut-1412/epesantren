<?php
session_start();
if(!isset($_SESSION["student_id"])){
	header("Location:login.php");
}
include_once("config/config.php");
// $s_db=$_SESSION["db"];
$student_full_name=$_SESSION["student_full_name"];
$class_name=$_SESSION["class_name"];
$majors_name=$_SESSION["majors_name"];
$foldersekolah=$_SESSION["folder"];

// memanggil library FPDF
require('fpdf181/fpdf.php');
// intance object dan memberikan pengaturan halaman PDF
$pdf = new FPDF('l','mm','A5');
// membuat halaman baru
$pdf->AddPage();
// setting jenis font yang akan digunakan
$pdf->SetFont('Arial','B',16);
// mencetak string 
$pdf->Cell(190,7,'Buku Tabungan',0,1,'C');
$pdf->SetFont('Arial','B',12);
// $pdf->Cell(190,7,'DAFTAR SISWA KELAS IX JURUSAN REKAYASA PERANGKAT LUNAK',0,1,'C');

// Memberikan space kebawah agar tidak terlalu rapat
$pdf->Cell(10,7,'',0,1);

$pdf->SetFont('Arial','B',10);
$pdf->Cell(20,6,'Tanggal',1,0);
$pdf->Cell(85,6,'Debit',1,0);
$pdf->Cell(27,6,'Kredit',1,0);
// $pdf->Cell(25,6,'TANGGAL LHR',1,1);

$pdf->SetFont('Arial','',10);
// $mutasi = mysqli_query($mysqli, $tabungan);
$siswaID = $_SESSION["student_id"];
$mutation = mysqli_query($mysqli,"SELECT banking_date as date, banking_note as note, banking_code as code, banking_debit AS debit, banking_kredit AS kredit, 
            @saldo:=@saldo+banking_debit-banking_kredit AS saldo 
            FROM `banking` JOIN (SELECT @saldo:=0) a WHERE banking_student_id = '$siswaID'  ORDER BY banking_date DESC");
while ($row = mysqli_fetch_array($mutation)){
    $pdf->Cell(20,6,$row['date'],1,0);
    $pdf->Cell(85,6,$row['debit'],1,0);
    $pdf->Cell(27,6,$row['kredit'],1,0); 
}

$pdf->Output();
?>