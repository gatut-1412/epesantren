<?php
	session_start();
	include_once("config/config.php");

	// Fetch all data from database
	$result = mysqli_query($mysqli, "SELECT * FROM `student` ORDER BY student_id ASC");

?>

<?php
	$kodesekolah=$_SESSION["kode_sekolah"];
	$namasekolah=$_SESSION["nama_sekolah"];
	$alamatsekolah=$_SESSION["alamat_sekolah"];
	$dbsekolah=$_SESSION["db"];
	$foldersekolah=$_SESSION["folder"];
	// $picsekolah=$_SESSION["pic"];
?>

<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>Epesantren Mobile</title>

	<!-- Fonts -->
	<link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

	<!-- Fontawesome font file css -->
	<link rel="stylesheet" type="text/css" href="css/font-awesome.min.css">
	<!-- <link rel="stylesheet" type="text/css" href="{{asset('/jquery-ui/jquery-ui.min.css')}}"> -->
	<link rel="stylesheet" type="text/css" href="jquery-ui/jquery-ui.min.css">

	<!--turbo slider plugin css file -->
	<link rel="stylesheet" type="text/css" href="plugins/turbo-slider/turbo.css">

	<!-- Template global css file. Requared all pages -->
	<link rel="stylesheet" type="text/css" href="css/global.style.css">
	<script src="js/jquery-3.2.1.min.js" charset="utf-8"></script>
</head>

<body>


	<form action="login_action1.php" method="post">
	<!-- <form action="auth.php" method="post"> -->
		<div class="login-wrap">
			<div class="login-html">
			<div class="text-center mt-10">
				<!-- <img src="img/ePesantren-1.png" class="logo-img" alt="" style=width:70% > -->
				<?php
					$logo = mysqli_query($mysqli, "SELECT `setting_value` FROM `setting` WHERE `setting_id` = 6");
					$row = mysqli_fetch_array($logo);
					echo '<img src="logo/'. $row['setting_value'].'" class="logo-img" alt="" style=width:70% >';
				?>
			</div>
				<input id="tab-1" type="radio" name="tab" class="sign-in" checked><label for="tab-1" class="tab"></label>
				<input id="tab-2" type="radio" name="tab" class="sign-up"><label for="tab-2" class="tab"></label>
				<?php 
					if (isset($_GET['password']) AND $_GET['password'] == 'salah') {?>
					<div class="alert alert-danger">
						Password yang anda masukkan salah. 
						Silahkan Klik Coba Lagi.
					</div>
					<a href="back.php" class="button circle block red">Coba Lagi</a>
				<?php } ?>
				<div class="login-form">
					<div class="sign-in-htm">
						<section class="container">
							<div class="form-row txt-center mt-30">
								<h1 class="banner-head-login">Selamat Datang di Pondok Pesantren <?php echo $namasekolah ?></h1>
							</div>
						</section>
						<div class="group">
							<label for="user" class="label">NIS Santri</label>
							<input id="nis" name="nis" type="text" class="input" placeholder="Masukkan NIS Siswa (Isi minimal 4 digit)" onkeyup="load_data()">
						</div>
						<div id="hasilnama">
							<div class="group">
								<label for="user" class="label">Nama Santri</label>
								<input id="username" name="username" type="text" class="input" readonly>
							</div>
						</div>
						<div class="group">
							<label for="pass" class="label">Password</label>
							<input id="password" name="password" type="password" class="input" data-type="password">
						</div>
						<div class="group">
							<input type="submit" name="submit" id="submit" class="button" value="Masuk">
						</div>
						<div class="group">
							<a href="back.php"><button type="button" class="button red">Kembali</button></a>
						</div>
						<div class="hr"></div>
						<div class="foot-lnk">
							<a href="forgot-password.php">Lupa Password?</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</form>


	<!--Page loader DOM Elements. Requared all pages-->
	<div class="sweet-loader">
		<div class="box">
		  	<div class="circle1"></div>
		  	<div class="circle2"></div>
		  	<div class="circle3"></div>
		</div>
	</div>

	<!-- JQuery library file. requared all pages -->
	<script src="js/jquery-3.2.1.min.js"></script>
    <script src="jquery-ui/jquery-ui.js" type="text/javascript"></script>
	<script src="jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
	<script type="text/javascript" src="js/login.js"></script>

    <!-- Turbo slider plugin file. requared only wizard pages -->
    <script src="plugins/turbo-slider/turbo.min.js"></script>
    <script src="js/turbo-ini.js"></script>

	<!-- Template global script file. requared all pages -->
	<script src="js/global.script.js"></script>

	<script>
		function load_data()
		{
			var nis_siswa = $('#nis').val();
			//alert (nis_siswa);
			$.ajax({
				url:"ajaxlivesearch_siswa.php",
				method:"POST",
				data:{
					'query':nis_siswa
					},
				success:function(data)
				{
			$('#hasilnama').html(data);
				}
			});
			
		}
		$(document).ready(function(){
		});
	</script>
	

	
</body>

</html>