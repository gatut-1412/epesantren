<?php
session_start();
if(!isset($_SESSION["student_id"])){
	header("Location:login.php");
}
include_once("config/config.php");
// include_once("login_action.php");

// Fetch all data from database

//$row = $result->fetch_assoc();

$student_id=$_SESSION["student_id"];
$student_nis=$_SESSION["student_nis"];
$student_full_name=$_SESSION["student_full_name"];
// $student_password=$_SESSION["student_password"];
$class_name=$_SESSION["class_name"];
$majors_name=$_SESSION["majors_name"];

?>
<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=0">
	<title>Profil</title>

	<!-- Google font file. If you want you can change. -->
	<link href="https://fonts.googleapis.com/css?family=Nunito:400,600,700,900" rel="stylesheet">

	<!-- Fontawesome font file css -->
	<link rel="stylesheet" type="text/css" href="css/font-awesome.min.css">

	<!-- Template global css file. Requared all pages -->
    <link rel="stylesheet" type="text/css" href="css/global.style.css">
    
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="/resources/demos/style.css">

</head>

<body>
	
	<div class="wrapper">
        <?php 
		include("menu/nav-menu.php");
		?>
		<div class="wrapper-inline">
			<!-- Header area start -->
			<header> <!-- extra class no-background -->
				<!-- <a class="go-back-link" href="javascript:history.back();"><i class="fa fa-arrow-left"></i></a> -->
				<h1 class="page-title">Edit Data</h1>
				<div class="navi-menu-button">
					<em></em>
					<em></em>
					<em></em>
				</div>
			</header>
			<!-- Header area end -->
			<!-- Page content start -->
			<main>
			<div class="container">

                    <div class="form-divider"></div>
                    <?php  { 

                    $siswaID = $_SESSION["student_id"];

                    $siswa = "SELECT `student`.`student_full_name`, `student_nis`, `student_born_place`, `student_img`, `student_address`, `student_gender`, `student_born_date`, `student_name_of_mother`, `student_name_of_father`, `student_parent_phone`, 
                    `majors_name`, `majors_id`, `majors_short_name`, `madin_id`, `madin_name`, `class_name`, `class_id`
                    FROM `student`
                    LEFT JOIN `class` ON `class`.class_id = `student`.class_class_id
                    LEFT JOIN `majors` ON `student`.majors_majors_id = `majors`.majors_id
                    LEFT JOIN `madin` ON `madin`.`madin_id` = `student`.`student_madin`
                    WHERE student_id = '$siswaID' 
                    ORDER BY `student_id` ASC";
                    ?>
                    <form action="update.php" method="post" enctype="multipart/form-data">
                        <input type="hidden" value="<?php echo $siswaID ?>" name="student_id">
                        
                        <?php
                            $mutasi = mysqli_query($mysqli, $siswa);
                            while($row = mysqli_fetch_array($mutasi)) {
                        ?>
                        
                        <input type="hidden" name="student_img_old" class="form-element" placeholder="foto" value="<?php echo $row['student_img']; ?>" >
                        <?php 
                            if (isset($_GET['pesan']) AND $_GET['pesan'] == 'gagal') {?>
                            <div class="alert alert-danger">
                                <strong>Maaf!</strong> Data anda gagal diperbarui. Silahkan masukkan kembali.
                            </div>
                        <?php } ?>
                        
                        <div class="form-label-divider"><span>Data Pribadi</span></div>
                        <div class="form-divider"></div>
                        <label>Nama Lengkap</label>
                        <div class="form-row-group with-icons">
                            <div class="form-row no-padding">
                                <i class="fa fa-user"></i>
                                <input type="text" name="student_full_name" class="form-element" placeholder="name" value="<?php echo $row['student_full_name']; ?>" >
                            </div>
                        </div>
                        <label>Foto</label>
                        <div class="form-row-group with-icons">
                            <div class="form-row no-padding">
                                <i class="fa fa-user"></i>
                                <input type="file" name="student_img" class="form-element" placeholder="foto" >
                            </div>
                        </div>
                        <label>Jenis Kelamin</label>
                        <div class="form-row-group">
                            <div class="form-row no-padding">
                                <label class="form-element"><input type="radio" name="student_gender" value="L" <?php if($row['student_gender']=='L') echo 'checked'?> /> Laki-Laki</label>
                                <label class="form-element"><input type="radio" name="student_gender" value="P" <?php if($row['student_gender']=='P') echo 'checked'?> /> Perempuan</label>
                            </div>
                        </div>
                        <label>Tempat Lahir</label>
                        <div class="form-row-group with-icons">
                            <div class="form-row no-padding">
                                <i class="fa fa-home"></i>
                                <input type="text" name="student_born_place" class="form-element" placeholder="tempatlahir" value="<?php echo $row['student_born_place']; ?>" >
                            </div>
                        </div>
                        <label>Tanggal Lahir</label>
                        <div class="form-row-group with-icons">
                            <div class="form-row no-padding">
                                <i class="fa fa-calendar"></i>
                                <input type="date" name = "student_born_date" id = "date" class="form-element" placeholder="tanggallahir" value="<?php echo date('d F Y', strtotime($row['student_born_date'])); ?>">
                            </div>
                        </div>
                        <label>Alamat</label>
                        <div class="form-row-group with-icons">
                            <div class="form-row no-padding">
                                <i class="fa fa-address-card-o"></i>
                                <input type="text" name="student_address" class="form-element" placeholder="alamat" value="<?php echo $row['student_address']; ?>" >
                            </div>
                        </div>

                        <div class="form-divider"></div>
                        <div class="form-label-divider"><span>Data Pesantren</span></div>
                        <label>NIS</label>
                        <div class="form-row-group with-icons">
                            <div class="form-row no-padding">
                                <i class="fa fa-id-card-o"></i>
                                <input type="text" name="student_nis" class="form-element" placeholder="nis" value="<?php echo $row['student_nis']; ?>" readonly>
                            </div>
                        </div>
                        <label>Kelas</label>
                        <div class="form-row-group with-icons">
                            <div class="form-row no-padding">
                                <i class="fa fa-book"></i>
                                <input type="text" name="student_class" class="form-element" placeholder="kelas" value="<?php echo $row['class_name']; ?>" readonly >
                            </div>
                        </div>
                        <label>Unit</label>
                        <div class="form-row-group with-icons">
                            <div class="form-row no-padding">
                                <i class="fa fa-building-o"></i>
                                <input type="text" name="student_majors" class="form-element" placeholder="unit" value="<?php echo $row['majors_name']; ?>" readonly >
                            </div>
                        </div>
                        <label>Madin</label>
                        <div class="form-row-group with-icons">
                            <div class="form-row no-padding">
                                <i class="fa fa-bed"></i>
                                <input type="text" name="student_madin" class="form-element" placeholder="madin" value="<?php echo $row['madin_name']; ?>" readonly >
                            </div>
                        </div>
                        <div class="form-divider"></div>
                        <div class="form-label-divider"><span>Data Keluarga</span></div>
                        <label>Nama Ayah</label>
                        <div class="form-row-group with-icons">
                            <div class="form-row no-padding">
                                <i class="fa fa-user"></i>
                                <input type="text" name="student_name_of_father" class="form-element" placeholder="ayah" value="<?php echo $row['student_name_of_father']; ?>" >
                            </div>
                        </div>
                        <label>Nama Ibu</label>
                        <div class="form-row-group with-icons">
                            <div class="form-row no-padding">
                                <i class="fa fa-user"></i>
                                <input type="text" name="student_name_of_mother" class="form-element" placeholder="ibu" value="<?php echo $row['student_name_of_mother']; ?>" >
                            </div>
                        </div>
                        <label>No.Whatsapp Orang Tua</label>
                        <div class="form-row-group with-icons">
                            <div class="form-row no-padding">
                                <i class="fa fa-phone"></i>
                                <input type="text" name="student_parent_phone" class="form-element" placeholder="kamar" value="<?php echo $row['student_parent_phone']; ?>" >
                            </div>
                        </div>
                        <div class="form-row">
                            <button type="submit" name="simpan" class="button circle block green">Simpan</button>
                        </div>
                        <div class="form-row">
                            <a href="javascript:history.back();" class="button circle block red">Batal</a>
                        </div>
						
                    </form>
					<?php
                    }
                    }
					?>

                </div>
                
                <?php 
                    include("footer/footer.php");
                ?>
			</main>
			<!-- Page content end -->
		</div>
	</div>


	<!--Page loader DOM Elements. Requared all pages-->
	<div class="sweet-loader">
		<div class="box">
		  	<div class="circle1"></div>
		  	<div class="circle2"></div>
		  	<div class="circle3"></div>
		</div>
	</div>

	<!-- JQuery library file. requared all pages -->
	<script src="js/jquery-3.2.1.min.js"></script>

	<!-- Template global script file. requared all pages -->
    <script src="js/global.script.js"></script>
    
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

    <script>
        $( function() {
            $( "#date" ).datepicker({
            dateFormat: "yy-mm-dd"
            });
        } );
    </script>
    
</body>

</html>