<?php
    session_start();
    include_once("config/config.php");
    if(empty($_SESSION['student_nis'])){
    header('location:main.php');
    }
    if((time() - $_SESSION['auto_logout_time'])>900){
    header('location:logout1.php');
    }
?>

<?php
$student_full_name=$_SESSION["student_full_name"];
$class_name=$_SESSION["class_name"];
$majors_name=$_SESSION["majors_name"];
$foldersekolah=$_SESSION["folder"];
// $s_db=$_SESSION["db"];
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=0">
    <title>Tahfidz</title>

	<!-- Google font file. If you want you can change. -->
	<link href="https://fonts.googleapis.com/css?family=Nunito:400,600,700,900" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.2/css/bootstrap.css">

	<!-- Fontawesome font file css -->
    <link rel="stylesheet" type="text/css" href="css/font-awesome.min.css">
    
	<!-- Template global css file. Requared all pages -->
    <link rel="stylesheet" type="text/css" href="css/global.style.css">
    <!-- <link rel="stylesheet" type="text/css" href="cssbootstrap/css/bootstrap.min.css"> -->
   

</head>

<body>
	
	<div class="wrapper">
        <?php 
		    include("menu/nav-menu.php");
		?>
		<div class="wrapper-inline">
			<!-- Header area start -->
			<header> <!-- extra class no-background -->
                <a class="go-back-link" href="main.php"><i class="fa fa-arrow-left"></i></a>
                <h1 class="page-title">Laporan Tahfidz</h1>
				<div class="navi-menu-button">
					<em></em>
					<em></em>
					<em></em>
				</div>
			</header>
			<!-- Header area end -->
			<!-- Page content start -->
			<main class="fix-top-menu">

					<!-- TAB CONTROL START -->
                    <div class="container">
                        
                        <form action="<?php echo $_SERVER["PHP_SELF"];?>" method="get">
                            <div class="form-row no-padding">
                                <label for="sel1">Tahun Ajaran:</label>
                                <select class="form-element" name="period">
                                <?php
                                                               
                                        $sql="select * from period";

                                        $hasil=mysqli_query($mysqli,$sql);
                                        $no=0;
                                        while ($data = mysqli_fetch_array($hasil)) {
                                        $no++;

                                        $ket="";
                                            if (isset($_GET['period'])) {
                                            $period = trim($_GET['period']);

                                            if ($period==$data['period_id'])
                                            {
                                            $ket="selected";
                                            }
                                        }
                                ?>
                                <option <?php echo $ket; ?> value="<?php echo $data['period_id'];?>"><?php echo $data['period_start'];?>/<?php echo $data['period_end'];?></option>
                                <?php
                                    }
                                ?>
                                    
                                </select>
                                <div class="form-divider"></div>

                                <div class="col-sm-4">
                                    <button type="submit" class="button circle green"><i class="fa fa-search"> </i> Cari Data</button>
                                </div>
                                
                            </div>
                        </form>

                        <div class="form-divider"></div>
                        <div class="form-label-divider"><span>Laporan Tahfidz</span></div>
                        <div class="form-divider"></div>

                        <?php if (isset($_GET['period'])) {

                            $tahun_tahfidz   = $_GET['period'];
                            $siswaID = $_SESSION["student_id"];

                            $tahfidz = "SELECT `tahfidz`.`tahfidz_date`, `tahfidz_new`, `tahfidz_new_note`,`tahfidz_murojaah_note`, 
                            `tahfidz_murojaah`, `tahfidz_input_date`, `student_full_name`, `student_nis`, `student_name_of_mother`, 
                            `student_parent_phone`, `period_start`, `period_end`, `majors_name`, `majors_short_name`,  `madin_id`, 
                            `madin_name`, `class_name`
                            FROM `tahfidz` 
                            JOIN `period` ON `tahfidz`.tahfidz_period_id = `period`.period_id 
                            LEFT JOIN `student` ON `student`.student_id = `tahfidz`.tahfidz_student_id
                            LEFT JOIN `class` ON `class`.class_id = `student`.class_class_id
                            LEFT JOIN `majors` ON `student`.majors_majors_id = `majors`.majors_id
                            LEFT JOIN `madin` ON `madin`.`madin_id` = `student`.`student_madin`
                            WHERE tahfidz_student_id = '$siswaID' AND tahfidz_period_id = '$tahun_tahfidz'
                            ORDER BY `tahfidz_id` ASC";
                        ?>
                        
                        <div class="table-responsive">
                            <table id="example" class="table table-striped table-bordered">
                                <thead class="thead-dark">
                                    <tr>
                                        <th>No</th>
                                        <th>Tanggal</th>
                                        <th>Jumlah Hafalan</th>
                                        <th>Ket. Hafalan</th>
                                        <th>Murojaah</th>
                                        <th>Mur. Hafalan</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                        $mutasi = mysqli_query($mysqli, $tahfidz);

                                        $hafalan = mysqli_query($mysqli,"SELECT `tahfidz`.`tahfidz_date`, `tahfidz_new`, `tahfidz_new_note`,`tahfidz_murojaah_note`, `tahfidz_murojaah`, `tahfidz_input_date`, 
                                        `student_full_name`, `student_nis`, `student_name_of_mother`, `student_parent_phone`, 
                                        `period_start`, `period_end`, `majors_name`, `majors_short_name`,  `madin_id`, `madin_name`, `class_name`
                                        FROM `tahfidz` 
                                        JOIN `period` ON `tahfidz`.tahfidz_period_id = `period`.period_id 
                                        LEFT JOIN `student` ON `student`.student_id = `tahfidz`.tahfidz_student_id
                                        LEFT JOIN `class` ON `class`.class_id = `student`.class_class_id
                                        LEFT JOIN `majors` ON `student`.majors_majors_id = `majors`.majors_id
                                        LEFT JOIN `madin` ON `madin`.`madin_id` = `student`.`student_madin`
                                        WHERE tahfidz_student_id = '$siswaID' AND tahfidz_period_id = '$tahun_tahfidz'
                                        ORDER BY `tahfidz_id` DESC");
                                        $no = 1;
                                        while($row = $hafalan->fetch_assoc()){
                                        echo '
                                        <tr>
                                            <td>'.$no.'</td>
                                            <td>'.date ("d-F-Y", strtotime ($row['tahfidz_date'])).'</td>
                                            <td>'.$row['tahfidz_new'].'</td>
                                            <td>'.$row['tahfidz_new_note'].'</td>
                                            <td>'.$row['tahfidz_murojaah'].'</td>
                                            <td>'.$row['tahfidz_murojaah_note'].'</td>
                                        </tr>
                                        ';
                                        $no++;
                                    }
                                    ?>
                                </tbody>
                                <?php 
                                } 
                                ?>
                            </table>
                        </div>

                        <div class="form-divider"></div>

                        <?php
                            $hasil=mysqli_query($mysqli,$tahfidz);
                            $sumTahfidz = 0;
                            while($row = mysqli_fetch_array($hasil))
                            $sumTahfidz += $row['tahfidz_new'];{
                        ?>

                            <label for="sel1">Rekap Total Halaman:</label>

                            <div class="form-row-group with-icons">
                                <div class="form-row no-padding">
                                    <i class="fa fa-envelope"></i>
                                    <input type="text" name="aaa" class="form-element" placeholder="Username" value=" <?php echo $sumTahfidz ?>" readonly>
                                </div>
                            </div>
                            <div class="form-divider"></div>
                            <div class="form-row">
                                <a href="<?php echo 'https://' . $foldersekolah . '.epesantren.com/manage/tahfidz/printBook/?n=' . base64_encode($tahun) . '&r=' . base64_encode($siswaNIS) ?>" class="button circle block blue">Cetak Buku Tahfidz</a>
                            </div>

                        <?php 
                        }
                        ?>

                    </div>
                    
                    <?php 
                        include("footer/footer.php");
                    ?>
			</main>
			<!-- Page content end -->
            
		</div>
	</div>


	<!--Page loader DOM Elements. Requared all pages-->
	<div class="sweet-loader">
		<div class="box">
		  	<div class="circle1"></div>
		  	<div class="circle2"></div>
		  	<div class="circle3"></div>
		</div>
	</div>

	<!-- JQuery library file. requared all pages -->
	<script src="js/jquery-3.2.1.min.js"></script>

	<!-- Template global script file. requared all pages -->
    <script src="js/global.script.js"></script>

    <!-- JQuery DataTable -->
    <script src="js/jquery-datatable.js"></script>
    <script src="js/jquery-3.5.1.js"></script>
    <script src="js/jquery-datatable-bootstrap4.js"></script>

    <script>
        $(document).ready(function() {
        $('#example').DataTable();
    } );
    </script>

    <script >
        $(window).on("load resize ", function() {
        var scrollWidth = $('.tbl-content').width() - $('.tbl-content table').width();
        $('.tbl-header').css({'padding-right':scrollWidth});
        }).resize();
    </script>

	
</body>

</html>