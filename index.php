<?php

include_once("config/config.php");

// Fetch all data from database
$result = mysqli_query($mysqli, "SELECT * FROM `sekolahs` ORDER BY id ASC");
?>
<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>Epesantren Mobile</title>

	<!-- Fonts -->
	<link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

	<!-- Fontawesome font file css -->
	<link rel="stylesheet" type="text/css" href="css/font-awesome.min.css">
	<!-- <link rel="stylesheet" type="text/css" href="{{asset('/jquery-ui/jquery-ui.min.css')}}"> -->
	<link rel="stylesheet" type="text/css" href="jquery-ui/jquery-ui.min.css">

	<!--turbo slider plugin css file -->
	<link rel="stylesheet" type="text/css" href="plugins/turbo-slider/turbo.css">

	<!-- Template global css file. Requared all pages -->
	<link rel="stylesheet" type="text/css" href="css/global.style.css">
	<link rel="stylesheet" href="cssbootsrap/css/bootstrap.min.css">
    <script src="js/jquery-3.2.1.min.js" charset="utf-8"></script>
    
  
</head>

<body>
	<form action="login-pesantren.php" method="post">
		<div class="login-wrap">
			<div class="login-html">
			<div class="text-center  mt-10">
				<img src="img/ePesantren-1.png" class="logo-img" alt="" style=width:70% >
			</div>
				<input id="tab-1" type="radio" name="tab" class="sign-in" checked><label for="tab-1" class="tab"></label>
				<input id="tab-2" type="radio" name="tab" class="sign-up"><label for="tab-2" class="tab"></label>
                
                <div class="login-form" action="">
					<div class="sign-in-htm">
						<div class="group">
							<label for="user" class="label">Kode Pesantren</label>
							<input type="text" maxlength="10" class="input" style="width:100%;" id="kodesearch" name="kodesearch" placeholder="Masukkan ID Pesantren" onkeyup="load_data()">
						</div>
						<div class="group">
                            <input type="submit" class="button" value="Lanjut">
						</div>
						<div class="hr"></div>
						<div class="foot-lnk">
							<a href="logout.php">Hapus History</a>
						</div>
						<!-- <div class="form-divider"></div>
						<div class="foot-lnk">
							<a href="logout.php">Coba Lagi</a>
						</div>	 -->
					</div>
				</div>
			</div>
		</div>
	</form>


	<!--Page loader DOM Elements. Requared all pages-->
	<div class="sweet-loader">
		<div class="box">
		  	<div class="circle1"></div>
		  	<div class="circle2"></div>
		  	<div class="circle3"></div>
		</div>
	</div>

	<!-- JQuery library file. requared all pages -->
	<script src="js/jquery-3.2.1.min.js"></script>
    <script src="jquery-ui/jquery-ui.js" type="text/javascript"></script>
    <script src="jquery-ui/jquery-ui.min.js" type="text/javascript"></script>

    <!-- Turbo slider plugin file. requared only wizard pages -->
    <script src="plugins/turbo-slider/turbo.min.js"></script>
    <script src="js/turbo-ini.js"></script>

	<!-- Template global script file. requared all pages -->
	<script src="js/global.script.js"></script>

	<!-- <script>
		function load_data()
		{
			var kode = $('#kodesearch').val();
			// alert(kode);
			$.ajax({
				url:"ajaxlivesearch.php",
				method:"POST",
				data:{
					'query':kode
					},
				success:function(data)
				{
			$('#pesantren').html(data);
				}
			});
			
		}
		$(document).ready(function(){
		//load_data();
		});
	</script> -->

	<!-- <script>
		$(document).ready(function(){
			load_data();
			function load_data(keyword)
			{
				$.ajax({
					method:"POST",
					url:"data-pesantren.php",
					data: {keyword:keyword},
					success:function(hasil)
					{
						$('.result').html(hasil);
					}
				});
			}
			$('#kodesearch').keyup(function(){
				// var jurusan = $("#s_jurusan").val();
				var keyword = $("#kodesearch").val();
				load_data(keyword);
			});
			$('#s_jurusan').change(function(){
				// var jurusan = $("#s_jurusan").val();
				var keyword = $("#kodesearch").val();
				load_data(keyword);
			});
		});
	</script> -->

	<!-- <script>
		$(document).ready(function(){
		$("input").keydown(function(){
			$("input").css("background-color", "lightblue");
		});
		$("input").keyup(function(){
			$("input").css("background-color", "lavender");
		});
		$("#btn1").click(function(){
			$("input").keydown();
		});  
		$("#btn2").click(function(){
			$("input").keyup();
		});  
		});
	</script> -->

</body>

</html>