<?php

include_once("config/config.php");

// Fetch all data from database
$result = mysqli_query($mysqli, "SELECT * FROM `sekolahs` ORDER BY id ASC");
?>
<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>Epesantren Mobile</title>

	<!-- Fonts -->
	<link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

	<!-- Fontawesome font file css -->
	<link rel="stylesheet" type="text/css" href="css/font-awesome.min.css">
	<!-- <link rel="stylesheet" type="text/css" href="{{asset('/jquery-ui/jquery-ui.min.css')}}"> -->
	<link rel="stylesheet" type="text/css" href="jquery-ui/jquery-ui.min.css">

	<!--turbo slider plugin css file -->
	<link rel="stylesheet" type="text/css" href="plugins/turbo-slider/turbo.css">

	<!-- Template global css file. Requared all pages -->
	<link rel="stylesheet" type="text/css" href="css/global.style.css">
	<link rel="stylesheet" href="cssbootsrap/css/bootstrap.min.css">
    <script src="js/jquery-3.2.1.min.js" charset="utf-8"></script>
    
  
</head>

<body>
	<form action="login-pesantren.php" method="post">
		<div class="login-wrap">
			<div class="login-html">
                <div class="login-form" action="">
						<div class="group">
							<label for="pass" class="label">Nama Pesantren</label>
							<input  type="text" class="input"  id='sekolah' name='sekolah' value = "<?php echo $row->nama_sekolah; ?>"placeholder="Masukkan Nama Pesantren">
						</div>
						<div class="group">
							<label for="pass" class="label">Alamat</label>
							<input  type="text" class="input"  id='sekolahaddress' name='sekolahaddress' value = "<?php echo $row->alamat_sekolah; ?>"placeholder="Masukkan Alamat Pesantren">
						</div>
						<!-- <div class="group">
                            <input type="submit" class="button" value="Lanjut">
						</div>
						<div class="hr"></div>-->
					</div>
				</div>
			</div>
		</div>
	</form>


	<!--Page loader DOM Elements. Requared all pages-->
	<div class="sweet-loader">
		<div class="box">
		  	<div class="circle1"></div>
		  	<div class="circle2"></div>
		  	<div class="circle3"></div>
		</div>
	</div>

	<!-- JQuery library file. requared all pages -->
	<script src="js/jquery-3.2.1.min.js"></script>
    <script src="jquery-ui/jquery-ui.js" type="text/javascript"></script>
    <script src="jquery-ui/jquery-ui.min.js" type="text/javascript"></script>

    <!-- Turbo slider plugin file. requared only wizard pages -->
    <script src="plugins/turbo-slider/turbo.min.js"></script>
    <script src="js/turbo-ini.js"></script>

	<!-- Template global script file. requared all pages -->
	<script src="js/global.script.js"></script>

	<!-- <script>
	$(document).ready(function(){
		load_data();
		function load_data(jurusan, keyword)
		{
			$.ajax({
				method:"POST",
				url:"data.php",
				data: {jurusan: jurusan, keyword:keyword},
				success:function(hasil)
				{
					$('.group').html(hasil);
				}
			});
	 	}
		$('#s_keyword').keyup(function(){
			var jurusan = $("#s_jurusan").val();
    		var keyword = $("#s_keyword").val();
			load_data(jurusan, keyword);
		});
		$('#s_jurusan').change(function(){
			var jurusan = $("#s_jurusan").val();
    		var keyword = $("#s_keyword").val();
			load_data(jurusan, keyword);
		});
	});
</script> -->

<!-- <script type="text/javascript">
    $(document).ready( function() {
      $('#kodesearch').on('keyup', function() {
        $.ajax({
          type: 'POST',
          url: 'file-search.php',
          data: {
            search: $(this).val()
          },
          cache: false,
          success: function(data) {
            $('#sekolah').html(data);
			$('#sekolahaddress').html(data);
          }
        });
      });
    });
  </script> -->

</body>

</html>